# Industrial Domain

Library focused on application layer for industrial enviroments. It solves tipical problems of traceability, part flow or quality inspection control

It mix two architectures:
1. *Domain Driven  Design*: It's used ubiquous language of industrial field. And there are contexts, aggregates, value objects, services and other tipical concepts developed by Eric Evants.
2. *Event Sourcing*: I've tryed to decouple both concepts but the structure of aggregates shall follow some event sourcing principles, as any change on the aggregate shall fire an aggregate event.

Due to the fact that my practical experience is on quality control, it has the context of quality more developed. But anyway there are other contexts, such as product or logistic.
