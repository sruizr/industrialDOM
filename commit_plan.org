 #+title: Commit plans


* Active tasks
* Backlog
** TODO Review manufacturing service
- [ ] feat: access to toolbox from inspection service
- [ ] test: accept full successful test
- [ ] test: change of part model after manufacturing
- [ ] feat: autoload subrequirements
- [ ] feat: wait test until response
- [ ] feat: persist test aggregate
- [ ] feat: prepare test with main information
- [ ] bug: búsqueda de grupos en plan de control
* Archive
** DONE Test test aggregate
- State "DONE"       from "NEXT"       [2022-12-18 dom 23:40]
- [X] test: review tests package
- [X] test: review plans module
** DONE Close inspection service
- State "DONE"       from "NEXT"       [2022-12-18 Sun 14:33]
- [X] feat: collect part by station aggregate
- [X] feat: load equipments at station
- [X] feat: load epi in equipment if validated
- [X] feat: prepare test to start
- [X] feat: follow control plan checks
- [X] feat: close test to finish
** DONE Cleaning structure
- State "DONE"       from "NEXT"       [2022-12-15 Thu 20:15]
- [X] chore: add pre-commit and black
** DONE Change Inspection Service
- State "DONE"       from "NEXT"       [2022-12-17 sáb 13:20]
- [X] feat: take responsability of inspection
