from inddom.infrastructures.onmem.eventsourcing import OnMemBroker, OnMemEventData
from tests.inddom.build.units.infrastructure.core import _FeatureEventData


class FeatureEventDataManagesEventDTOs(_FeatureEventData):
    def setup_method(self, method):
        self.event_data = OnMemEventData()


# class FeatureEventBrokerPublishEventDTOs(_FeatureEventBroker):
#     def setup_method(self, method):
#         self.event_data = Mock()
#         self.broker = OnMemEventBroker(self.event_data, timeout=0)
