import inddom.infrastructures.onmem.projections as proj
import tests.inddom.build.units.infrastructure.projections as _


class FeaturePersonDataPersistPersons(_.FeaturePersonDataPersistPersons):
    def setup_method(self, method):
        self.data = proj.OnMemPersonData()


class FeatureRequirementsDataPersistProjections(
    _.FeatureRequirementsDataPersistProjections
):
    def setup_method(self, method):
        self.data = proj.OnMemRequirementData()


class FeaturePartModelDataPersistProjections(_.FeaturePartModelDataPersistProjections):
    def setup_method(self, method):
        self.data = proj.OnMemPartModelData()


class FeatureControlPlanDataProjectsEvents(_.FeatureControlPlanDataProjectsEvents):
    def setup_method(self, method):
        self.data = proj.OnMemControlPlanData()
