import datetime
import json

import pytest

import inddom.infrastructures.encoding as encoding
from tests.inddom.build.builders import Given

JSON_SEPARATORS = (",", ":")


class FeatureDatetimeIsoDecoderDecodesStringToDatetime:
    def ucase_decode_datetime_from_isoformat_string(self):
        now = datetime.datetime.now()
        decoder = encoding.DatetimeStringDecoder()
        assert now == decoder.decode(now.isoformat())

    def ucase_isoformat_is_validated(self):
        decoder = encoding.DatetimeStringDecoder()

        value = datetime.datetime.now().isoformat()
        assert decoder.is_isoformat(value)

        value = value.replace("T", "a")
        assert not decoder.is_isoformat(value)
        assert not decoder.is_isoformat(1)

        assert not decoder.is_isoformat("10101010")
        assert decoder.is_isoformat("2010-10-22")

        assert decoder.is_isoformat("2020-11-18 00:05:23.283+00:00")


class FeatureValueDictEncoderConvertValuesToDict:
    def ucase_encoding_plain_value_objects(self):
        encoder = encoding.ValueDictEncoder()
        value = Given().a_value.with_pars(par_int=1, part_str="str").build()
        Value = value.__class__

        dict_value = value.pars
        dict_value["__value__"] = "{}:{}".format(Value.__module__, Value.__name__)
        assert dict_value == encoder.encode(value)

    def ucase_encoding_nested_values(self):
        encoder = encoding.ValueDictEncoder()
        given = Given()
        nested_value = given.a_value.with_pars(par_int=1).build()
        value = given.a_value.with_pars(par_str="str", value=nested_value).build()

        encoded = encoder.encode(value)
        assert encoded["value"] == encoder.encode(nested_value)

    def ucase_encoding_id_values(self):
        encoder = encoding.ValueDictEncoder()
        aggregate_id = Given().an_aggregate_id.build()

        expected = {
            "__value__": "inddom._build.units.builders:FakeAggregateId",
            "uuid": aggregate_id.uuid,
        }

        encoded = encoder.encode(aggregate_id)
        assert encoded == expected

    def ucase_value_is_validated(self):
        value = Given().a_value.build()
        encoder = encoding.ValueDictEncoder()

        assert encoder.is_value(value)

        value = object()
        assert not encoder.is_value(value)


class FeatureValueDictDecoderConvertsDictToValues:
    def ucase_decoding_dict_to_values(self):
        decoder = encoding.ValueDictDecoder()
        value = Given().a_value.with_pars(par_int=1, par_str="str").build()
        value_dict = {
            "__value__": "inddom._build.units.builders:FakeValue",
            "par_int": 1,
            "par_str": "str",
        }

        assert value == decoder.decode(value_dict)

    def ucase_decoding_nested_values(self):
        decoder = encoding.ValueDictDecoder()
        given = Given()
        nested_value = given.a_value.with_pars(par_int=2).build()
        value = given.a_value.with_pars(par_int=1, par_value=nested_value).build()

        topic = "inddom._build.units.builders:FakeValue"
        value_dict = {
            "__value__": topic,
            "par_int": 1,
            "par_value": {"__value__": topic, "par_int": 2},
        }

        assert value == decoder.decode(value_dict)

    def ucase_decoding_absent_values_raise_ex(self):
        decoder = encoding.ValueDictDecoder()

        topic = "no.exist.this.route:FakeValue"
        value_dict = {
            "__value__": topic,
            "par_int": 1,
            "par_value": {"__value__": topic, "par_int": 2},
        }
        try:
            decoder.decode(value_dict)
            pytest.fail("ValueClassNotFound should be raised")
        except encoding.ValueClassNotFound:
            pass


class FeatureParsJSONEncoderEncodesEventParsToJSON:
    def ucase_encoding_simple_pars(self):
        encoder = encoding.ParsJSONEncoder()
        event_dto = Given().an_event_dto.with_pars(par_int=1, par_str="str").build()

        expected = json.dumps(
            {"par_int": 1, "par_str": "str"}, sort_keys=True, separators=JSON_SEPARATORS
        )
        print(event_dto)
        assert encoder.encode(event_dto["pars"]) == expected

    def ucase_encoding_datetime_pars(self):
        encoder = encoding.ParsJSONEncoder()
        now = datetime.datetime.now()
        event_dto = Given().an_event_dto.with_pars(par_int=1, par_dt=now).build()

        expected = json.dumps(
            {"par_int": 1, "par_dt": now.isoformat()},
            sort_keys=True,
            separators=JSON_SEPARATORS,
        )
        print(event_dto)
        assert encoder.encode(event_dto["pars"]) == expected

    def ucase_encoding_value_objects(self):
        encoder = encoding.ParsJSONEncoder()
        given = Given()
        value = given.a_value.with_pars(par=1).build()
        event_dto = given.an_event_dto.with_pars(value=value).build()

        expected = json.dumps(
            {
                "value": {
                    "__value__": "inddom._build.units.builders:FakeValue",
                    "par": 1,
                }
            },
            sort_keys=True,
            separators=JSON_SEPARATORS,
        )

        assert encoder.encode(event_dto["pars"]) == expected


class FeatureParsJSONDecoderDecodesJSONtoEventPars:
    def ucase_decoding_datetime(self):
        decoder = encoding.ParsJSONDecoder()
        now = datetime.datetime.now()
        event_dto = (
            Given()
            .an_event_dto.with_pars(par_int=1, part_str="str", issue_date=now)
            .build()
        )

        encoded = encoding.ParsJSONEncoder().encode(event_dto["pars"])

        assert event_dto["pars"] == decoder.decode(encoded)

    def ucase_decoding_value_objects(self):
        decoder = encoding.ParsJSONDecoder()
        given = Given()
        now = datetime.datetime.now()
        value = given.a_value.with_pars(par_dt=now).build()
        event_dto = given.an_event_dto.with_pars(value=value).build()

        encoded = encoding.ParsJSONEncoder().encode(event_dto["pars"])
        assert type(encoded) is str

        assert event_dto["pars"] == decoder.decode(encoded)
        assert event_dto["pars"]["value"].par_dt == now

    def ucase_decoding_nested_value_objects(self):
        given = Given()
        now = datetime.datetime.now()
        nested_value = given.a_value.with_pars(par_dt=now).build()
        value = given.a_value.with_pars(value=nested_value).build()
        event_dto = given.an_event_dto.with_pars(value=value).build()

        decoder = encoding.ParsJSONDecoder()
        encoded = encoding.ParsJSONEncoder().encode(event_dto["pars"])

        assert event_dto["pars"] == decoder.decode(encoded)
        assert event_dto["pars"]["value"].value.par_dt == now
