import random
from unittest.mock import Mock

import pytest

from inddom.ddd import Aggregate
from inddom.eventsourcing import (
    AbsentSubscriber,
    AggregateAlreadyExist,
    AggregateNotFound,
    AggregateVersionConflict,
)
from tests.inddom.build.units.builders import Given


class _FeatureEventData:
    def given_a_list_of_event_dtos(self, number_of_events=5, aggregate=None):
        given = Given()
        if not aggregate:
            aggregate = given.an_aggregate.build()
            aggregate.create()

        event_builder = given.an_event_dto.of_created_event().with_aggregate(aggregate)

        event_dtos = [event_builder.build()]
        for _ in range(number_of_events - 1):
            event_dtos.append(
                event_builder.of_modified_event()
                .with_pars(par_int=random.randint(1, 1000))
                .build()
            )

        return event_dtos

    def ucase_event_dtos_are_persisted(self):
        event_dtos = self.given_a_list_of_event_dtos()

        aggregate_id = event_dtos[0]["aggregate"]["id"]
        self.event_data.insert_events(event_dtos)

        loaded_event_dtos = self.event_data.select_aggregate_events(aggregate_id)

        assert event_dtos == loaded_event_dtos

    def ucase_event_data_doesnt_create_aggregate_twice(self):
        event_dtos = self.given_a_list_of_event_dtos()

        self.event_data.insert_events(event_dtos)

        try:
            self.event_data.insert_events(event_dtos)
            pytest.fail("AggregateAlreadyExist should be raised ")
        except AggregateAlreadyExist:
            pass

        aggregate_id = event_dtos[0]["aggregate"]["id"]
        print(len(self.event_data.select_aggregate_events(aggregate_id)))
        print(self.event_data.index)

        assert self.event_data.index == 5

    def ucase_event_data_doesnt_insert_incompatible_version_events(self):
        event_dtos = self.given_a_list_of_event_dtos()
        self.event_data.insert_events(event_dtos)
        event_dtos = event_dtos[3:]

        try:
            self.event_data.insert_events(event_dtos)
            pytest.fail("AggregateVersionConflict should be raised")

        except AggregateVersionConflict:
            pass
        print(self.event_data.index)
        assert self.event_data.index == 5

    @pytest.mark.current
    def ucase_event_collects_events_by_aggregate_class(self):
        events_1 = self.given_a_list_of_event_dtos(number_of_events=10)
        other_aggregate = Given().another_aggregate.build()
        other_aggregate.create()
        events_2 = self.given_a_list_of_event_dtos(
            number_of_events=5, aggregate=other_aggregate
        )
        events_3 = self.given_a_list_of_event_dtos()

        Aggregate = Given().an_aggregate.build().__class__
        events = []
        events.extend(events_1[:5])
        events.extend(events_2)
        events.extend(events_3)
        events.extend(events_1[5:])

        self.event_data.insert_events(events)

        assert self.event_data.index == 20

        events = self.event_data.collect_events(Aggregate)

        expected_ids = [1, 2, 3, 4, 5, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        ids = [event["id"] for event in events]

        assert expected_ids == ids

        events = self.event_data.collect_events(Aggregate, 11)
        assert len(events) == 9


class FakeSubscriber:
    def __init__(self):
        self.events = []

    def event_raised(self, event_dto):
        self.events.append(event_dto)
