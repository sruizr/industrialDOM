import inddom.infrastructures.sqlite.hhrr as hhrr
import inddom.infrastructures.sqlite.product as proj
import inddom.infrastructures.sqlite.quality as qua
import tests.inddom.build.units.infrastructure.projections as _


class FeaturePersonDataPersistPersons(_.FeaturePersonDataPersistPersons):
    def setup_method(self, method):
        self.data = hhrr.SQLPersonsData(":memory:")


class FeatureRequirementsDataPersistProjections(
    _.FeatureRequirementsDataPersistProjections
):
    def setup_method(self, method):
        self.data = proj.SQLRequirementData(":memory:")


class FeaturePartModelDataPersistProjections(_.FeaturePartModelDataPersistProjections):
    def setup_method(self, method):
        self.data = proj.SQLPartModelData(":memory:")


class FeatureControlPlanDataProjectsEvents(_.FeatureControlPlanDataProjectsEvents):
    def setup_method(self, method):
        self.data = qua.SQLControlPlanData(":memory:")
