from inddom.infrastructures.sqlite.eventstore import EventData
from tests.inddom.build.units.infrastructure.core import _FeatureEventData


class FeatureEventData(_FeatureEventData):
    def setup_method(self, method):
        self.event_data = EventData(":memory:")
