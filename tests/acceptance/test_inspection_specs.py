import pytest

import inddom.quality.inspection as inspection
from inddom.logistics.parts import PartId
from tests.inddom.build.builders import FakeClient
from tests.inddom.build.hhrr.builders import Given as GivenFromHHRR
from tests.inddom.build.logistics.builders import Given as GivenFromLogistics
from tests.inddom.build.quality.builders import Given as GivenFromQuality

given = GivenFromQuality()


class _FeatureAutomaticTestExecution:
    def ucase_inspection_supply_person_when_prepare_test(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        part_info = environment.part_info
        responsible = environment.responsible
        part_model = environment.part_model
        service = environment.build_service()

        service.prepare_test(client, part_info)

        responsible_event = client.events[-4]
        assert responsible_event["event_name"] == "ResponsibleIsSet"
        assert (
            responsible_event["entity_name"]
            == "inddom.inspection:AutomaticInspectionService"
        )
        assert responsible_event["event_pars"] == {
            "full_name": f"{responsible.surname}, {responsible.firstname}"
        }

        part_set_event = client.events[-3]
        assert part_set_event["event_name"] == "PartIsSet"
        assert (
            part_set_event["entity_name"]
            == "inddom.inspection:AutomaticInspectionService"
        )
        assert part_set_event["event_pars"] == {
            "part_number": part_model.part_number,
            "part_name": "part-name",
            "part_description": part_model.description,
            "serial_number": part_info["serial_number"],
        }

    def ucase_inspection_raises_when_responsible_not_authorized(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        service = environment.build_service()

        responsible = (
            GivenFromHHRR().a_person.with_code("new-code").as_role("new-role").build()
        )
        environment.store.save(responsible)

        client.set_responsible_code(responsible.code)

        try:
            service.prepare_test(client, environment.part_info)

            pytest.fail("NotAuthorizedResponsible exception should be raised")
        except inspection.NotAuthorizedResponsible as e:
            assert (
                str(e) == f'Responsible "{responsible.firstname} {responsible.surname}"'
                ' is not authorized to run inspection for "part-number"'
            )

    def ucase_inspection_raises_when_responsible_not_is_found(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        service = environment.build_service()

        client.set_responsible_code("no-exist-responsible")

        try:
            service.prepare_test(client, environment.part_info)

            pytest.fail("ResponsibleNotFound exception should be raised")
        except inspection.ResponsibleNotFound as e:
            assert (
                str(e)
                == f'Responsible with code "{client.responsible_code}" is not found on system'
            )

    def ucase_inspection_raises_when_control_plan_not_found(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        environment.control_plans.get_by_location_and_affected.return_value = None
        service = environment.build_service()

        try:
            service.prepare_test(client, environment.part_info)

            pytest.fail("ResponsibleNotFound exception should be raised")
        except inspection.ControlPlanNotFound as e:
            assert str(e) == 'Not found control plan for part number "part-number"'

    def ucase_inspection_try_to_find_control_plans_from_groups(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        service = environment.build_service()
        control_plans = environment.control_plans
        control_plan = environment.control_plan
        part_model = environment.part_model
        control_plans.get_by_location_and_affected.side_effect = (
            lambda location, name: control_plan.id.uuid
            if name == part_model.groups[0]
            else None
        )

        service.prepare_test(client, environment.part_info)

        control_plans.get_by_location_and_affected.assert_called_with(
            client.location_code, part_model.groups[0]
        )

    def ucase_inspection_try_to_find_control_plans_from_part_number(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        service = environment.build_service()
        control_plans = environment.control_plans
        control_plan = environment.control_plan
        part_model = environment.part_model

        control_plans.get_by_location_and_affected.side_effect = (
            lambda location, name: control_plan.id.uuid
            if name == part_model.part_number
            else None
        )

        service.prepare_test(client, environment.part_info)
        control_plans.get_by_location_and_affected.assert_called_with(
            client.location_code, part_model.part_number
        )


class _FeatureFollowingChecklist:
    def ucase_inspection_raises_when_method_not_found(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        control = environment.control_plan.steps[0]
        environment.methods[control.method_name] = None
        service = environment.build_service()

        with pytest.raises(inspection.MethodNotFound) as ex:
            service.walk(client)

        assert (
            str(ex.value) == f'Method with name "{control.method_name}" '
            "not found in current catalog."
        )

    def ucase_inspection_run_check_properly(self):
        def method(check, requirement, **pars):
            assert requirement.id == control.requirement_id
            assert pars == control.method_pars.invariants
            assert check.part
            assert check.ate == client.ate
            assert check.test.client == client

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        control = environment.control_plan.steps[0]
        environment.methods[control.method_name] = method
        service = environment.build_service()

        service.walk(client)

        assert client.events[-1]["event_name"] == "CheckIsOK"
        assert client.events[-2]["event_name"] == "CheckIsStarted"

    def ucase_inspection_run_check_with_failure(self):
        def method_with_failure(check, requirement, **pars):
            check.add_part_defect(requirement, "failure")

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        control = environment.control_plan.steps[0]
        environment.methods[control.method_name] = method_with_failure
        service = environment.build_service()

        service.walk(client)

        assert service.fetch(client) == "failed"

        assert client.events[-1]["event_name"] == "CheckIsNotOK"
        assert (
            client.events[-2]["event_name"] == "DefectIsAdded"
        )  # Defect is added to test
        assert (
            client.events[-3]["event_name"] == "DefectAdded"
        )  # Defect is added to part
        assert client.events[-4]["event_name"] == "CheckIsStarted"

    def ucase_inspection_run_check_with_exception(self):
        def method_with_exception(check, requirement, **pars):
            raise Exception("Upps")

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        control = environment.control_plan.steps[0]
        environment.methods[control.method_name] = method_with_exception
        service = environment.build_service()

        with pytest.raises(Exception) as ex:
            service.walk(client)

        assert str(ex.value) == "Upps"
        assert client.events[-1]["event_name"] == "CheckIsCancelled"

    def ucase_inspection_run_action_properly(self):
        def method(action, **pars):
            assert pars == step.method_pars.invariants
            assert action.ate == client.ate

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        step = environment.control_plan.steps[1]

        environment.methods[step.method_name] = method
        service = environment.build_service()

        service.jump(client)  # skip previous control
        service.walk(client)

        assert client.events[-1]["event_name"] == "ActionHasFinished"
        assert client.events[-2]["event_name"] == "ActionIsStarted"

    def ucase_inspection_run_action_with_exception(self):
        def method_with_exception(action, **pars):
            raise Exception("Developer had a problem!")

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        step = environment.control_plan.steps[1]

        environment.methods[step.method_name] = method_with_exception
        service = environment.build_service()

        service.jump(client)  # skip previous control
        with pytest.raises(Exception) as ex:
            service.walk(client)

        assert str(ex.value) == "Developer had a problem!"
        assert client.events[-1]["event_name"] == "ActionIsCancelled"

        with pytest.raises(StopIteration):
            service.walk(client)

    def ucase_inspection_finish_when_all_controls_are_done(self):
        def check_method(check, req, **pars):
            pass

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        service.walk(client)
        assert service.fetch(client) is None
        service.walk(client)

        with pytest.raises(StopIteration):
            service.walk(client)

        assert client.events[-2]["event_name"] == "Success"
        assert client.events[-1]["event_name"] == "Moved"  # Part is moved to next step
        assert (
            client.events[-1]["event_pars"]["location_code"]
            == environment.control_plan.to_location_code
        )
        assert service.fetch(client) == "success"

    def ucase_inspection_finish_before_due_to_cancelling(self):
        def check_method(check, req, **pars):
            pass

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        service.walk(client)
        service.cancel(client)
        with pytest.raises(StopIteration):
            service.walk(client)

        # assert client.events[-2].event_name == 'Moved'
        # assert (client.events[-2].event_pars['location_code'] ==
        #         client.location_code)
        assert client.events[-2]["event_name"] == "Cancelled"
        assert service.fetch(client) == "cancelled"


class _FeaturePartManagement:
    def ucase_inspection_creates_part_in_sublocation_of_client(self):
        client = FakeClient()

        environment = given.an_inspection_environment.suitable_for_client(client)

        part_info = environment.part_info
        service = environment.build_service()

        service.prepare_test(client, part_info)

        created = client.get_event_by_name("Created")
        assert created
        serial_number = part_info.pop("serial_number")
        assert created["event_pars"] == {
            "part_number": part_info.pop("part_number"),
            "serial_number": serial_number,
            "location_code": f"{client.location_code}_{client.key}",
            "parameters": part_info,
        }

        assert environment.store.load(PartId(serial_number))

    def ucase_inspection_moves_part_to_client_loc_if_already_exist(self):
        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(client)
        part_info = environment.part_info
        part_model = environment.part_model

        part = (
            GivenFromLogistics()
            .a_part.with_part_number(part_model.part_number)
            .with_serial_number(part_info["serial_number"])
            .build()
        )
        environment.store.save(part)

        service = environment.build_service()

        service.prepare_test(client, part_info)

        moved = client.get_event_by_name("Moved")
        client_loc = f"{client.location_code}_{client.key}"
        assert moved
        assert moved["event_pars"] == {"location_code": client_loc}
        assert part.location_code == client_loc

    def given_a_successful_inspection(self):
        pass

    def ucase_inspection_moves_part_to_control_plan_destination_if_all_correct(self):
        def check_method(check, req, **pars):
            pass

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        service.walk(client)
        service.walk(client)
        try:
            service.walk(client)
        except StopIteration:
            pass

        moved = client.events[-1]
        assert (
            moved["event_pars"]["location_code"]
            == environment.control_plan.to_location_code
        )

    def ucase_inspection_moves_part_to_control_plan_origin_if_cancelled(self):
        def check_method(check, req, **pars):
            pass

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        service.walk(client)
        service.walk(client)

        service.cancel(client)

        moved = client.events[-1]
        assert (
            moved["event_pars"]["location_code"]
            == environment.control_plan.from_location_code
        )

    def ucase_inspection_moves_part_to_control_plan_origin_if_defective(self):
        def check_method(check, req, **pars):
            check.add_part_defect(req, "incorrect")

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        service.walk(client)
        service.walk(client)

        try:
            service.cancel(client)
        except StopIteration:
            pass

        moved = client.events[-1]
        assert (
            moved["event_pars"]["location_code"]
            == environment.control_plan.from_location_code
        )

    def ucase_inspection_moves_part_even_with_hidden_exception(self):
        def check_method(check, req, **pars):
            raise Exception("upps!")

        def action_method(action, **pars):
            pass

        client = FakeClient()
        environment = given.an_inspection_environment.suitable_for_client(
            client
        ).prepared_to_test_for_client(client)
        steps = environment.control_plan.steps
        environment.methods[steps[0].method_name] = check_method
        environment.methods[steps[1].method_name] = action_method
        service = environment.build_service()

        for _ in range(3):
            try:
                service.walk(client)
            except Exception as e:
                print(str(e))

        moved = client.events[-1]
        print(moved)
        assert (
            moved["event_pars"]["location_code"]
            == environment.control_plan.from_location_code
        )
