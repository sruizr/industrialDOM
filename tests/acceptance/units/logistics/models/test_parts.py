from unittest.mock import Mock

from inddom._build.units import Observer
from inddom.logistics.models.parts import Part


class FeaturePartsHasTangibleActions:
    def given_an_observed_created_part(
        self, part_number="partNumber", serial_number="123456789"
    ):
        self.observer = Observer()
        part = Part()
        part.create(
            part_number, serial_number, "locatin_code", parameters={"other info": 1}
        )
        part.add_observer(self.observer)
        return part

    def ucase_part_is_created(self):
        a_part = Part()
        a_part.create("partNumber", "1234567", "location_code", parameters={"par1": 1})
        other_part = Part()
        other_part.create(
            "partNumber", "1234567", "other_location", parameters={"par1": 1}
        )
        assert a_part.id == other_part.id

    def ucase_part_is_moved_to_diferent_location(self):
        part = self.given_an_observed_created_part()

        part.move_to("other_location")

        assert type(self.observer.get_event(-1)) is part.Moved
        assert len(self.observer._events) == 2

        part.move_to("other_location")
        assert len(self.observer._events) == 2

    def ucase_part_can_have_defects(self):
        part = self.given_an_observed_created_part()
        assert not part.is_defective()
        defect = Mock()

        part.add_defect(defect)
        assert part.defects[0] == defect
        assert part.is_defective()

        assert type(self.observer.get_event(-1)) is part.DefectAdded

    def ucase_someone_can_remove_defects(self):
        part = self.given_an_observed_created_part()
        defect = Mock()

        part.add_defect(defect)
        part.add_defect(Mock())

        assert len(part.defects) == 2

        part.remove_defect(defect, "testId")
        assert len(part.defects) == 1

        part.remove_defect(Mock(), "testId")
        assert len(part.defects) == 1

    def ucase_defects_can_be_removed_by_requirement(self):
        part = self.given_an_observed_created_part(self)
        defect = Mock(requirement_id="reqId")

        part.add_defect(defect)
        part.add_defect(Mock())
        part.add_defect(Mock())

        requi = Mock(id="reqId")
        part.clear_defects(requi, "testId")
        assert defect not in part.defects

    def ucase_defects_can_be_removed_by_subrequirements(self):
        part = self.given_an_observed_created_part(self)
        defect = Mock(requirement_id="reqId")

        part.add_defect(defect)
        part.add_defect(Mock())
        part.add_defect(Mock())

        requi = Mock()
        requi.requirement_ids = ["reqId", "otherId"]

        part.clear_defects(requi, "testId")
        assert defect not in part.defects
