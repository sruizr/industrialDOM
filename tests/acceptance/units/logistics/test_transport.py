from unittest.mock import Mock

import pytest

from inddom.logistics.models.parts import PartId
from inddom.logistics.transport import PartNotFound, PartSetter


class FeaturePartSetterPlacesParts:
    def _given_a_part_setter(self):
        self.event_source = Mock()
        self.projector = Mock()
        return PartSetter(event_source=self.event_source, projector=self.projector)

    def _given_a_part_dto(
        self, part_number=None, serial_number=None, location_code=None
    ):
        return {
            "part_number": part_number if part_number else "3131",
            "serial_number": serial_number if serial_number else "12345",
            "location_code": location_code if location_code else "location",
        }

    def ucase_part_is_placed_to_a_location(self):
        part_setter = self._given_a_part_setter()
        part_dto = self._given_a_part_dto()
        part = self.event_source.load.return_value

        part_setter.place_part(
            part_number=part_dto["part_number"],
            serial_number=part_dto["serial_number"],
            to_location_code=part_dto["location_code"],
        )

        part_id = PartId(part_dto["part_number"], part_dto["serial_number"])

        self.event_source.load.called_with(part_id)
        part.move.assert_called_with(part_dto["location_code"])

    def ucase_part_not_found_raised_when_placing_if_part_no_exists(self):
        part_setter = self._given_a_part_setter()
        part_dto = self._given_a_part_dto()
        self.event_source.load.side_effect = PartNotFound(
            part_dto["part_number"], part_dto["serial_number"]
        )

        try:
            part_setter.place_part(
                part_number=part_dto["part_number"],
                serial_number=part_dto["serial_number"],
                to_location_code=part_dto["location_code"],
            )
            pytest.fail("PartNotFound exception should be raised")
        except PartNotFound as e:
            assert e.part_number == part_dto["part_number"]
            assert e.serial_number == part_dto["serial_number"]
