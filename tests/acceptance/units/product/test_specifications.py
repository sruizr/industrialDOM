from unittest.mock import Mock

from inddom.product.specifications import RequirementLoader


class FeatureRequirementLoaderLoadsRequirement:
    def ucase_simple_requirement_is_loaded(self):
        event_store = Mock()
        event_store.load.return_value = Mock(requirement_ids=[])
        loader = RequirementLoader(event_store)

        requirement = loader("requirementId")
        event_store.load.assert_called_with("requirementId")
        assert requirement == event_store.load.return_value

    def ucase_requirement_and_sub_requirements_are_loaded(self):

        event_store = Mock()
        expected_requirement = Mock()
        expected_requirement.requirement_ids = [1, 2, 3]
        subrequirements = [Mock(requirement_ids=[]) for _ in range(3)]
        event_store.load.side_effect = [expected_requirement] + subrequirements
        loader = RequirementLoader(event_store)

        requirement = loader("requirementId")
        assert requirement.requirements == subrequirements

    def ucase_requirement_is_cached(self):
        event_store = Mock()
        event_store.load.return_value = Mock(requirement_ids=[])
        loader = RequirementLoader(event_store)

        requirement = loader("requirementId")
        requirement = loader("requirementId")

        assert len(event_store.load.mock_calls) == 1
