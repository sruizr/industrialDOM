from unittest.mock import Mock, call

import pytest

from inddom.product.definitions import (
    Characteristic,
    PartModel,
    Requirement,
    RequirementId,
    Specification,
)


class FeatureRequirementsAsProductSpecificator:
    def ucase_requirements_are_defined(self):
        observer = Observer()

        requi = Requirement()
        requi.add_observer(observer)

        characteristic = Characteristic("co", "prod")
        specs = Specification(limits=[1, 2])
        path = "path/subpath"
        requi.define(path, characteristic, "eid", specs)

        assert type(observer.get_event()) is Requirement.Defined

    def ucase_requirements_has_sub_requirements(self):
        observer = Observer()

        requi = Requirement()
        requi.add_observer(observer)
        sub_requi = Requirement()
        sub_requi._id = 1

        requi.add_requirement(sub_requi)

        assert 1 in requi.requirement_ids
        assert type(observer.get_event()) is requi.RequirementAdded

    def ucase_requirements_affects_to_part_models_or_groups(self):
        observer = Observer()

        requi = Requirement()
        requi.add_observer(observer)
        requi.add_affected("group")

        assert "group" in requi.affects_to
        assert type(observer.get_event()) is requi.AffectedAdded

        requi.add_affected("group")
        assert len(requi.affects_to) == 1

    def ucase_subrequirements_are_got_by_key(self):
        requi = Requirement()
        requi.define("path", Characteristic("co", "EL"), None)

        sub_requi_1 = Requirement()
        sub_requi_2 = Requirement()
        sub_requi_1.define("", Characteristic("att1", "elem1"), "eid")
        sub_requi_2.define("", Characteristic("att2", "elem2"), None)
        requi.requirements.append(sub_requi_1)
        requi.requirements.append(sub_requi_2)

        assert requi.get_requirement("att1@elem1>eid") == sub_requi_1
        assert requi.get_requirement("att2@elem2") == sub_requi_2
        assert requi.get_requirement("nada") is None

    def ucase_load_from_store_subrequirements(self):
        requi = Requirement()
        requi.define("path", Characteristic("co", "EL"), None)

        sub_requi_1 = Requirement()
        sub_requi_2 = Requirement()
        sub_requi_1.define("", Characteristic("att1", "elem1"), "eid")
        sub_requi_2.define("", Characteristic("att2", "elem2"), None)
        requi.add_requirement(sub_requi_1)
        requi.add_requirement(sub_requi_1)

        store = Mock()
        requi.load_subrequirements(store)

        assert (
            call(RequirementId("", Characteristic("att1", "elem1"), "eid"))
            in store.load.mock_calls
        )
        assert store.load.return_value in requi.requirements


class FeaturePartModelsDefineParts:
    def ucase_part_models_are_defined_by_part_number(self):
        observer = Observer()

        part_model = PartModel()
        part_model.add_observer(observer)

        part_model.define("part_number", "partName", "description")

        assert type(observer.get_event()) is PartModel.Defined
        assert part_model.part_number == "part_number"
        assert part_model.description == "description"
        assert part_model.part_name == "partName"

        other_part_model = PartModel()
        other_part_model.define("part_number", "part_name", "other description")

        assert part_model.id == other_part_model.id

    def ucase_part_models_can_have_a_group(self):
        observer = Observer()

        part_model = PartModel()
        part_model.add_observer(observer)
        part_model.add_group("group")

        assert "group" in part_model.groups

        part_model.add_group("group")
        assert len(part_model.groups) == 1
