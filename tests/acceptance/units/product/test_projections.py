from unittest.mock import Mock

from inddom.product.models.definitions import PartModel, Requirement
from inddom.product.projections import PartModels, Requirements


class FeaturePartModelsProjects:
    def given_a_part_model_projection(self):
        self.data = Mock()
        self.data.index = 0
        self.event_dto = {
            "id": 1,
            "name": None,
            "aggregate": {"class": PartModel, "id": "aggregateId"},
            "pars": {"whatever": "whatever"},
        }

        return PartModels(self.data)

    def ucase_part_model_projection_has_name(self):
        part_models = self.given_a_part_model_projection()
        assert part_models.name == "part_models"

    def ucase_part_model_projection_is_defined(self):
        part_models = self.given_a_part_model_projection()
        self.event_dto["name"] = "Defined"
        part_models.event_raised(self.event_dto)
        self.data.insert_part_model.assert_called_with(self.event_dto)

    # def ucase_part_model_projection_has_affecteds(self):
    #     part_models = self.given_a_part_model_projection()
    #     self.event_dto['name'] = 'GroupAdded'
    #     part_models.event_raised(self.event_dto)
    #     self.data.update_add_group.assert_called_with(
    #         self.event_dto)

    def ucase_projection_retrieves_part_info_by_part_number(self):
        part_models = self.given_a_part_model_projection()
        value = part_models.get_part_info("part_number")

        self.data.select_part_info_by_part_number.assert_called_with("part_number")
        assert value == self.data.select_part_info_by_part_number.return_value


class FeatureRequirementsAreProjected:
    def given_a_requirement_projection(self):
        self.data = Mock()
        self.data.index = 0
        self.event_dto = {
            "id": 1,
            "name": None,
            "aggregate": {"class": Requirement, "id": "aggregateId"},
            "pars": {"whatever": "whatever"},
        }

        return Requirements(self.data)

    def ucase_requirement_projection_has_name(self):
        requirements = self.given_a_requirement_projection()
        assert requirements.name == "requirements"

    def ucase_requirement_projection_is_defined(self):
        requirements = self.given_a_requirement_projection()
        self.event_dto["name"] = "Defined"

        requirements.event_raised(self.event_dto)
        self.data.insert_requirement.assert_called_with(self.event_dto)

    def ucase_projection_retrieves_requirement_info(self):
        requirements = self.given_a_requirement_projection()
        value = requirements.get_requirement_info("requirementId")

        self.data.select_requirement.assert_called_with("requirementId")
        assert value == self.data.select_requirement.return_value
