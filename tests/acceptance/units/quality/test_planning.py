import uuid
from unittest.mock import Mock, call, patch

import pytest

from inddom.quality.planning import ControlPlanFinder


class ControlPlanFinderBuilder:
    def __init__(self, refresh_time=None):
        projector = Mock()
        self.projection = projector.control_plans
        self.event_store = Mock()

        self.projection.get.return_value = uuid.uuid1().hex
        self._finder = ControlPlanFinder(projector, self.event_store, refresh_time)

    def given_control_plan_finder(self):
        return self._finder


class FeatureRetrievingControlPlans:
    def ucase_control_plan_finder_retrieves_control_plan(self):
        builder = ControlPlanFinderBuilder()
        finder = builder.given_control_plan_finder()
        builder.projection.get_id.return_value = cp_id = "uuid"

        control_plan = finder("part_number", "location")

        builder.projection.get_id.assert_called_with("location", "part_number")

        builder.event_store.load.assert_called_with(cp_id)
        assert control_plan == builder.event_store.load.return_value

    @patch("inddom.quality.planning.PartModelId")
    def ucase_control_plan_is_found_by_part_group(self, mockId):
        builder = ControlPlanFinderBuilder()
        finder = builder.given_control_plan_finder()
        part = Mock()
        part.groups = ["group1", "group2"]

        control_plan = Mock()
        cp_id = uuid.uuid1().hex
        builder.projection.get_id.side_effect = [None, cp_id]

        builder.event_store.load.side_effect = [part, control_plan]

        control_plan = finder("part_number", "location")

        #  assert call(mockId.return_value) in builder.event_store.load.mock_calls
        assert call(cp_id) in builder.event_store.load.mock_calls

        assert call("location", "part_number") in builder.projection.get_id.mock_calls
        assert call("location", "group1") in builder.projection.get_id.mock_calls


class FeatureControlPlanFinderCaches:
    def ucase_control_plan_finder_caches_previous_queries(self):
        builder = ControlPlanFinderBuilder()
        finder = builder.given_control_plan_finder()

        control_plan = finder("part_number", "location")
        other_control_plan = finder("part_number", "location")

        assert len(builder.event_store.load.mock_calls) == 1
        assert control_plan == other_control_plan

    @patch("inddom.quality.planning.time")
    def ucase_control_plan_finder_removes_cache_after_time(self, mock_time):
        mock_time.time.side_effect = [0, 3, 4, 6, 6]
        builder = ControlPlanFinderBuilder(refresh_time=5)
        finder = builder.given_control_plan_finder()

        control_plan = finder("part_number", "location")

        control_plan = finder("part_number", "location")
        assert len(builder.event_store.load.mock_calls) == 1

        control_plan = finder("part_number", "location")
        assert len(builder.event_store.load.mock_calls) == 2
