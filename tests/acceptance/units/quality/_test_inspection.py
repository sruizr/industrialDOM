from unittest.mock import Mock, call, patch

import pytest

from inddom.eventsourcing import AggregateAlreadyExist
from inddom.hhrr.models.persons import NotAuthorizedPerson
from inddom.quality.inspection import Inspector, InspectorFactory
from inddom.quality.models.planning import Control, MethodPars, Step
from tests.units import Observer
from tests.units.logistics.builders import Given as GivenFromLogistics
from tests.units.quality.builders import Given

given_from_logistics = GivenFromLogistics()
given = Given()


class InspectorBuilder:
    def __init__(self, responsible_code="responsible_code"):
        self.control_plan_finder = Mock()
        self.method_catalog = Mock()
        self.method_catalog.return_value = lambda check, requi, **pars: None
        self.requirement_loader = Mock()
        self.event_store = Mock()

        self.given_a_control_plan_with_controls()

        self.cavity = Mock()
        self._inspector = Inspector(
            self.cavity,
            responsible_code,
            self.control_plan_finder,
            self.method_catalog,
            self.requirement_loader,
            self.event_store,
        )

    def with_part_loaded(self):
        self.part_info = self.given_part_info_from_cavity()
        self._inspector.load_part(self.part_info)
        return self

    def given_part_info_from_cavity(self):
        return {
            "part_number": "part-number",
            "serial_number": "1234556",
            "parameters": {"pars": 1},
        }

    def given_a_control_plan_with_controls(self):
        control_plan = self.control_plan_finder.return_value
        control_plan.steps = [
            Control("reqId", "method_name", MethodPars(par2=2)),
            Step("method_name", None),
        ]

        return control_plan

    def __call__(self):
        return self._inspector


class FeatureInspectionCycle:
    @patch("inddom.quality.inspection.Test")
    def ucase_inspector_run_check_following_control(self, mockTest):
        builder = InspectorBuilder()
        control_plan = builder.given_a_control_plan_with_controls()
        inspector = builder.with_part_loaded()()

        inspector.run_control()

        control = control_plan.steps[0]

        method = builder.method_catalog.return_value
        requirement = builder.requirement_loader.return_value
        method_pars = control.method_pars
        test = mockTest.return_value

        test.run_check.assert_called_with(method, requirement, method_pars)

    @patch("inddom.quality.inspection.Test")
    def ucase_inspector_run_control_one_by_one(self, mockTest):
        builder = InspectorBuilder()
        control_plan = builder.given_a_control_plan_with_controls()
        inspector = builder.with_part_loaded()()

        inspector.run_control()

        test = mockTest.return_value
        test.run_check.assert_called()

        inspector.run_control()
        assert len(test.run_check.mock_calls) == 1
        assert len(test.run_action.mock_calls) == 1

        try:
            inspector.run_control()
            pytest.fail("StopIteration should be raised")
        except StopIteration:
            pass

    @patch("inddom.quality.inspection.Test")
    def ucase_inspector_cancels_test(self, mockTest):
        builder = InspectorBuilder()
        inspector = builder.with_part_loaded()()
        exception = Mock()
        inspector.cancel(exception)

        test = mockTest.return_value
        test.cancel.assert_called_with(exception)

    @patch("inddom.quality.inspection.Test")
    def ucase_inspector_finish_test(self, mockTest):
        builder = InspectorBuilder()
        inspector = builder.with_part_loaded()()
        inspector.finish()

        test = mockTest.return_value
        test.finish.assert_called_with()

    @patch("inddom.quality.inspection.Test")
    def ucase_inspector_send_part_to_location_code_when_ok(self, mockTest):
        builder = InspectorBuilder()
        inspector = builder.with_part_loaded()()

        test = mockTest.return_value
        test.resolution = "success"

        inspector.finish()

        assert inspector.part.location_code == inspector.control_plan.to_location_code


class FeatureInspectionCycleFromCollectedPart:
    def ucase_inspector_saves_part(self):
        part = given_from_logistics.a_part.loaded_from_store().build()
        inspector_builder = given.an_inspector
        inspector = inspector_builder.build()

        inspector_builder.store.save.side_effect = AggregateAlreadyExist
        inspector_builder.store.load.return_value = part

        inspector.load_part(
            {"part_number": part.part_number, "serial_number": part.serial_number}
        )

        while True:
            try:
                inspector.run_control()
            except StopIteration:
                break

        inspector.finish()

        assert call(inspector.test) in inspector.store.mock_calls
        assert call(inspector.part) in inspector.store.mock_calls


class FeatureInspectorTransferObservers:
    def ucase_inspector_assures_cavity_observes_part(self):
        builder = InspectorBuilder()
        inspector = builder.with_part_loaded()()

        event = builder.cavity.event_raised.call_args_list[-3][0][0]

        assert type(event) is inspector.part.Created

    def ucase_inspector_assures_cavity_observes_test(self):
        builder = InspectorBuilder()
        inspector = builder.with_part_loaded()()
        builder.method_catalog.return_value = lambda check, req, **pars: None
        inspector.run_control()

        event = builder.cavity.event_raised.call_args_list[-1][0][0]
        print(event)

        assert type(event) is inspector.test.CheckIsOK
