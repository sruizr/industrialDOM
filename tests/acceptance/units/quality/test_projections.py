from unittest.mock import Mock

from inddom.quality.models.planning import ControlPlan
from inddom.quality.projections import ControlPlans


class FeatureControlPlansProjects:
    def given_a_control_plan_projection(self):
        self.data = Mock()
        self.data.index = 0
        self.event_dto = {
            "id": 1,
            "name": None,
            "aggregate": {"class": ControlPlan, "id": "aggregateId"},
            "pars": {"whatever": "whatever"},
        }

        return ControlPlans(self.data)

    def ucase_control_plan_projection_has_name(self):
        control_plans = self.given_a_control_plan_projection()
        assert control_plans.name == "control_plans"

    def ucase_control_plan_projection_is_created(self):
        control_plans = self.given_a_control_plan_projection()
        self.event_dto["name"] = "Created"
        control_plans.event_raised(self.event_dto)
        self.data.insert_control_plan.assert_called_with(self.event_dto)

    def ucase_control_plan_projection_has_affecteds(self):
        control_plans = self.given_a_control_plan_projection()
        self.event_dto["name"] = "AffectedAdded"
        control_plans.event_raised(self.event_dto)
        self.data.update_add_affected.assert_called_with(self.event_dto)

    def ucase_projection_retrieves_ids_by_location_and_affected(self):
        control_plans = self.given_a_control_plan_projection()
        value = control_plans.get_id("location", "part")

        self.data.select_control_plan_id.assert_called_with("location", "part")
        assert value == self.data.select_control_plan_id.return_value
