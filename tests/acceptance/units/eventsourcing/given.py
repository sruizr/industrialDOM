from inddom.eventsourcing import Broker, EventStore


class SimpleBroker(Broker):
    pass


class OnMemoryEventStore(EventStore):
    pass
