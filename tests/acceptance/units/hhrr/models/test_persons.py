import datetime
from unittest.mock import Mock, patch

from inddom._build.units import Observer
from inddom.hhrr.models.persons import Person, PersonId


class PersonBuilder:
    def __init__(self):
        self._person = Person()

    def hired(self):

        self._person.hire("code", "name", "Surname, Firstname")
        return self

    def given_person(self):
        return self._person


class FeaturePersonImplementEmployee:
    def ucase_person_is_hired(self):
        observer = Observer()

        person = Person()
        person.add_observer(observer)

        person.hire("code", "name", "Surname, Firstname")

        event = observer.get_event()

        assert type(event) is person.Hired
        assert person.code == "code"
        assert person.username == "name"
        assert person.firstname == "Firstname"
        assert person.surname == "Surname"

    @patch("inddom.hhrr.models.persons.datetime")
    def ucase_person_is_authorized_for_role_deploy(self, mock_datetime):
        mock_datetime.datetime.now.return_value = when = datetime.datetime(2020, 1, 1)
        responsible = Mock()
        observer = Observer()
        builder = PersonBuilder()
        person = builder.hired().given_person()
        person.add_observer(observer)

        person.authorize("role_name", responsible)

        event = observer.get_event()
        assert type(event) is person.RoleAdded
        assert event.pars == {
            "role": "role_name",
            "responsible_id": responsible.id,
            "issue_date": when,
        }

        assert "role_name" in person.roles
