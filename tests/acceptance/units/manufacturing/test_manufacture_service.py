from unittest.mock import Mock, patch

import pytest

from inddom.eventsourcing import AggregateVersionConflict
from inddom.manufacturing.manufacture import (
    ManufacturingProcess,
    PartAlreadyManufactured,
)


class FeatureManufacturingCreatesParts:
    def _given_a_manufacturing_process(self):
        self.event_source = Mock()
        self.projector = Mock()
        return ManufacturingProcess(
            line_code="line", event_source=self.event_source, projector=self.projector
        )

    def _given_a_part_dto(
        self, part_number=None, serial_number=None, location_code=None
    ):
        return {
            "part_number": part_number if part_number else "3131",
            "serial_number": serial_number if serial_number else "12345",
            "location_code": location_code if location_code else "location",
        }

    @patch("inddom.manufacturing.manufacture.Part")
    def ucase_part_is_created_from_manufacturing_process(self, mock_Part):
        manufacturing_process = self._given_a_manufacturing_process()
        part_dto = self._given_a_part_dto()

        manufacturing_process.produce(**part_dto)

        mock_Part.assert_called_with(**part_dto)
        self.event_source.save.assert_called_with(mock_Part.return_value)

    @patch("inddom.manufacturing.manufacture.Part")
    def ucase_part_already_exist_exception_is_raised(self, mock_Part):
        manufacturing_process = self._given_a_manufacturing_process()
        part_dto = self._given_a_part_dto()
        self.event_source.save.side_effect = AggregateVersionConflict()

        try:
            manufacturing_process.produce(**part_dto)
            pytest.fail("PartAlreadyExist should be raised")
        except PartAlreadyManufactured as e:
            assert e.part == mock_Part()
