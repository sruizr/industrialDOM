class EventObserver:
    def __init__(self):
        self.events = []

    def event_is_triggered(self, event):
        self.events.append(event)

    def get_event(self, position=1):
        return self.events[-position]

    def get_last_event(self):
        return self.events[-1]
