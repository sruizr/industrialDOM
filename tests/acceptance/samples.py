cavity_is_empty = {
    "name": "empty",
    "aggregate_id": "cavityKey",
    "aggregate_name": "Cavity",
    "pars": {},
}


cavity_is_loaded = {
    "name": "loaded",
    "aggregate_id": "cavityKey",
    "aggregate_name": "Cavity",
    "pars": {},
}


cavity_is_ready = {
    "name": "ready",
    "aggregate_id": "cavityKey",
    "aggregate_name": "Cavity",
    "pars": {"responsible": {"full_name": "Ruiz Romero, Salvador"}},
}


cavity_is_busy = {
    "name": "busy",
    "aggregate_id": "cavityKey",
    "aggregate_name": "Cavity",
    "pars": {
        "part_info": {
            "part_model": {
                "part_number": "partNumber",
                "description": "partDescription",
            },
            "serial_number": "1234567890",
        }
    },
}

_cavity_is_solved = {
    "name": "solved",
    "aggregate_id": "cavityKey",
    "aggregate_name": "Cavity",
    "pars": {"resolution": None},
}


cavity_is_solved_success = _cavity_is_solved.copy()
cavity_is_solved_success["pars"]["resolution"] = "success"


cavity_is_solved_failed = _cavity_is_solved.copy()
cavity_is_solved_failed["pars"]["resolution"] = "failed"


cavity_is_solved_cancelled = _cavity_is_solved.copy()
cavity_is_solved_cancelled["pars"]["resolution"] = "cancelled"


_test_dto = {
    "name": None,
    "aggregate_id": "TestId",
    "aggregate_name": "Test",
    "pars": {},
}

test_prepared = _test_dto.copy()
test_prepared["name"] = "Prepared"
test_prepared["pars"] = {
    "responsible": {"full_name": "Ruiz Romero, Salvador"},
    "location_code": "loc_cavityKey",
}

check_started = {
    "name": "CheckStarted",
    "aggregate_id": "TestId",
    "aggregate_name": "Test",
    "requirement": {
        "characteristic": {
            "value_name": "Characteristic",
            "pars": {"element": "el", "attribute": "attr"},
        },
        "eid": "eid",
        "specs": {"limits": [1, 3]},
    },
}

part_info = {
    "part_model": {"part_number": "partNumber", "part_description": "partDescription"},
    "serial_number": "1234567890",
    "other_par": "otherPar",
}
