from inddom.manufacturing import equipments, stations


class StationBuilder:
    def __init__(self):
        self._code = "station-code"
        self._description = "station-description"
        self._equipments = []

    def with_code(self, value):
        self._code = value
        return self

    def with_description(self, value):
        self._description = value
        return self

    def with_equipment(self, value):
        self._equipments.append(value)
        return self

    def builder(self):
        station = stations.Station()
        station.identify(self._code, self._description)
        station.add_equipments(*self._equipments)
        return station


class EquipmentBuilder:
    def __init__(self):
        self._code = "eq-code"
        self._name = "eq-name"
        self._description = "eq-description"

        self._manufacturer = "manufacturer"
        self._epi_name = "epi_name"
        self._epi_pars = {"par1": 1, "par2": 2}

    def with_code(self, value):
        self._code = value
        return self

    def with_name(self, value):
        self._name = value
        return self

    def with_description(self, value):
        self._description = value
        return self

    def with_epi_configuration(self, manufacturer, epi_name, epi_pars):
        self._manufacturer = manufacturer
        self._epi_name = epi_name
        self._epi_pars = epi_pars
        return self

    def build(self):
        equipment = equipments.Equipment()
        equipment.identify(self._code, self._description)
        equipment.config_epi(self._manufacturer, self._epi_name, self._epi_pars)
        return equipment
