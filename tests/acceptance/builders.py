from unittest.mock import MagicMock

from inddom.hhrr.persons import Person, PersonId
from inddom.product.definitions import (
    Characteristic,
    PartModel,
    PartModelId,
    Requirement,
    RequirementId,
    Specification,
)
from inddom.quality.plans import Control, ControlPlan, ControlPlanId, Step


class PersonBuilder:
    def __init__(self):
        self._code = "000"
        self._name = "sruiz"
        self._full_name = "Ruiz, Salvador"

    def with_code(self, value):
        self._code = value

    def build(self):
        mock = MagicMock(spec=Person)
        mock.name = self._name
        mock.code = self._code
        mock.full_name = self._full_name
        mock.id = PersonId(self._code)
        return mock


class PartModelBuilder:
    def __init__(self):
        self._part_number = "part-number"
        self._part_name = "part-name"
        self._part_description = "part-description"

    def with_part_number(self, value):
        self._part_number = value
        return self

    def build(self):
        mock = MagicMock(spec=PartModel)
        mock.part_number = self._part_number
        mock.part_name = self._part_name
        mock.part_description = self._part_description
        mock.id = PartModelId(self._part_number)
        return mock


class ControlPlanBuilder:
    def __init__(self):
        self._responsible = None
        self._part_model = None
        self._is_authorized = lambda _: False
        self._steps = []

    def for_part_model(self, value):
        self._part_model = value
        return self

    def for_responsible(self, value):
        self._is_authorized = lambda responsible: responsible == value
        return self

    def with_step(self, step):
        self._steps.append(step)

    def build(self):
        control_plan = MagicMock(name="control_plan")
        control_plan.steps = self._steps
        control_plan.is_authorized.side_effect = self._is_authorized
        control_plan.id = ControlPlanId()

        return control_plan


class RequirementBuilder:
    def __init__(self):
        self._path = "path"
        self._char = Characteristic("attr", "element")
        self._eid = "eid"
        self._spec = Specification(value=1)

    def build(self):
        mock = MagicMock(
            spec=Requirement, path=self._path, characteristic=self._char, eid=self._eid
        )
        mock.id = RequirementId(self._path, self._char, self._eid)
        mock.spec = self._spec
        return mock
