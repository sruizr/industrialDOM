from inddom.eventsourcing import EventStore
from inddom.hhrr.models.persons import Person
from inddom.infrastructures.onmem.eventsourcing import OnMemEventData
from inddom.logistics.models.parts import Part, PartId
from inddom.product.models.definitions import PartModel
from inddom.quality.models.planning import ControlPlan


class PartBuilder:
    def __init__(self):
        self._part_number = "part_number{}"
        self._serial_number = "1234567890{}"
        self._location_code = "source"
        self._index = 0

    def with_part_number(self, value):
        self._part_number = value
        return self

    def build(self):
        part = Part()
        part.create(
            self._part_number.format(self._index),
            self._serial_number.format(self._index),
            self._location_code,
        )
        self._index += 1
        return part


class PartModelBuilder:
    def __init__(self):
        self._part_number = "part_number{}"
        self._partname = "part-name{}"
        self._description = "description{}"
        self._groups = []
        self._index = 0

    def inside_group(self, value):
        self._groups.append(value)
        return self

    def build(self):
        part_model = PartModel()
        part_model.define(
            self._part_number.format(self._index),
            self._partname.format(self._index),
            self._description.format(self._index),
        )
        for group in self._groups:
            part_model.add_group(group)

        self._index += 1
        return part_model


class PersonBuilder:
    def __init__(self):
        self._index = 0
        self._set_templates()

    def _set_templates(self):
        self._code = "000{}"
        self._username = "name{}"
        self._full_name = "Surname{}, Firstname"
        self._role = None

    def with_role(self, value, responsible):
        self._role = value
        self._responsible = responsible
        return self

    def build(self):
        person = Person()
        person.hire(
            self._code.format(self._index),
            self._username.format(self._index),
            self._full_name.format(self._index),
        )
        if self._role:
            person.authorize(self._role, self._responsible)

        self._set_templates()
        self._index += 1
        return person


class ControlPlanBuilder:
    def __init__(self):
        self._source = "source"
        self._destination = "destination"
        self._role = "verifier"
        self._affects_to = ["partmodel"]
        self._steps = []

    def with_step(self, responsible, method_name, pars=None):
        self._steps.append((method_name, pars, responsible, None))
        return self

    def with_control(self, responsible, method_name, pars, requirement):
        self._steps.append((method_name, pars, responsible, requirement))
        return self

    def build(self):
        control_plan = ControlPlan()
        return control_plan


class StoreBuilder:
    def build(self):
        return EventStore(OnMemEventData())


class Given:
    def __init__(self):
        self.a_person = PersonBuilder()
        self.a_part = PartBuilder()
        self.a_part_model = PartModelBuilder()
        self.a_control_plan = ControlPlanBuilder()
        self.a_store = StoreBuilder()
