import uuid

from inddom.containments import ContainmentService
from tests.inddom.build.hhrr import Given as GivenFromHHRR
from tests.inddom.build.infra import FakeClient, FakeStore
from tests.inddom.build.logistics import Given as GivenFromLogistics
from tests.inddom.build.product import Given as GivenFromProduct

LOCATION_CODE = "location-code"


class ContainmentServiceBuilder:
    def __init__(self):
        self._store = FakeStore()
        self._box_inventory = {}
        self._defect_catalogs = {}

        self._set_default_domain()

    @property
    def store(self):
        return self._store

    def _set_default_domain(self):
        self.person = GivenFromHHRR().a_person.build()
        self.part_model = GivenFromProduct().a_part_model.build()
        self.reject_box = (
            GivenFromLogistics().a_reject_box.of_part_model(self.part_model).build()
        )
        self.requirement = GivenFromProduct().a_requirement.build()

    def _save_domain(self):
        self._store.save(self.person)
        self._store.save(self.part_model)
        self._store.save(self.reject_box)
        self._store.save(self.requirement)

    def build(self):
        self._save_domain()
        return ContainmentService(
            self._store, self._box_inventory, self._defect_catalogs
        )

    def __call__(self):
        return self.build()


class Given:
    @property
    def a_containment_service(self):
        return ContainmentServiceBuilder()


given = Given()


class FeatureEventNotifications:
    def given_a_client(self, for_service, location_code="location-code") -> FakeClient:
        person = for_service.store.get_a("Person")
        return FakeClient(person.code, location_code)

    def ucase_contaiment_session_start(self):
        service = given.a_containment_service()
        client = self.given_a_client(service)
        person = service.store.get_a("Person")

        id_session = service.start_session(client)

        assert type(id_session) is uuid.UUID
        event = client.get_event_by_name("SessionIsStarted")

        expected = {
            "responsible_name": f"{person.surname}, {person.firstname}",
            "location_code": "location-code",
            "key": None,
        }
        assert event["event_pars"] == expected

    def ucase_operator_takes_a_reject_box(self):
        service = given.a_containment_service()
        client = self.given_a_client(service)
        reject_box = service.store.get_a("RejectBox")

        id_session = service.start_session(client)
        service.take_reject_box(id_session, reject_box.id.uuid)

        event = client.get_event_by_name("RejectBoxTaken")

        expected = {
            "uuid": reject_box.id.uuid,
            "part_number": reject_box.part_number,
            "tracking": reject_box.tracking,
            "qty": reject_box.qty,
        }

        assert event["event_pars"] == expected

    def ucase_operator_rework_a_part(self):
        service = given.a_containment_service()
        client = self.given_a_client(service)
        reject_box = service.store.get_a("RejectBox")
        requirement = service.store.get_a("Requirement")

        id_session = service.start_session(client)
        service.take_reject_box(id_session, reject_box.id.uuid)

        service.rework_part(
            id_session,
            defect_dto={"mode": "mode", "requirement_uuid": requirement.id.uuid},
            qty=2,
        )

        event = client.get_event_by_name("PartReworked")

        expected = {
            "defect": {
                "mode": "mode",
                "path": requirement.path,
                "attribute": requirement.characteristic.attr,
                "element": requirement.characteristic.element,
                "eid": requirement.eid,
            },
            "qty": 2,
        }

        assert event["event_pars"] == expected
