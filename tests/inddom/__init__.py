"""Unit tests assures that modules works as expected by its client.

Rules:

1. Don't mock internal classes in the module.
2. Mock dependency injection or modules imported
3. Try to avoid import modules and use it to create entities inside classes.
"""

from inddom.ddd import Aggregate


class EventObserver:
    def __init__(self):
        self.events = []

    def record_event(self, event_dto):
        self.events.append(event_dto)


def reload_aggregate(aggregate: Aggregate, new_aggregate: Aggregate):
    """Construct a new aggregate from the inner events of aggregate"""
    observer = EventObserver()
    aggregate.event_handler.add_callback(observer.record_event)
    new_aggregate.event_handler.build_entity(observer.events)

    return new_aggregate
