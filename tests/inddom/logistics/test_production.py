from unittest.mock import MagicMock

import pytest

from inddom.hhrr.models.persons import Person
from inddom.infrastructures.factories import EventStoreFactory
from inddom.logistics.models.parts import Part, PartId
from inddom.manufacturing.production import (
    FlowNotFound,
    NoBatchStarted,
    PartNoExist,
    PartNotFound,
    TransformationProcess,
    WrongPartModel,
    WrongResponsible,
)


def given_a_store():
    store = EventStoreFactory("onmem").create_store()
    return store


def given_a_transformation_process(part_change, from_location, to_location):
    process = TransformationProcess(
        given_a_store(),
        part_change,
        from_location=from_location,
        to_location=to_location,
    )
    return process


class TransformationTest:
    def setup_method(self, method):
        self.store = EventStoreFactory("onmem").create_store()
        responsible = Person()
        responsible.hire("responsible-code", "user-name", "first, surname")
        self.store.save(responsible)

        part = Part()
        part.create("in-part-number", "123456", "source")
        self.store.save(part)

        part = Part()
        part.create("wrong_part_number", "0123456", "source")
        self.store.save(part)

    def teardown_method(self, method):
        self.store = None

    def given_a_transformation_process(self):
        transformation_process = TransformationProcess(self.store)
        transformation_process.add_flow(
            ["in-part-number", "out-part-number"], "source", "destination"
        )
        return transformation_process


class FeaturePartTransformation(TransformationTest):
    def ucase_part_is_transformed(self):

        process = self.given_a_transformation_process()
        process.start_batch("responsible-code", "batch-number", "out-part-number")
        process.transform("123456")

        part = self.store.load(PartId("123456"))
        assert part.location_code == "destination"
        assert part.part_number == "out-part-number"

    def ucase_transformation_dont_works_if_not_starts(self):
        process = self.given_a_transformation_process()
        with pytest.raises(NoBatchStarted):
            process.transform("123456")

    def ucase_starting_fails_if_not_responsible(self):
        process = self.given_a_transformation_process()

        with pytest.raises(WrongResponsible):
            process.start_batch(
                "no-exist-responsible", "batch-number", "out-part-number"
            )

    def ucase_part_shall_exist(self):
        process = self.given_a_transformation_process()

        with pytest.raises(PartNoExist):
            process.start_batch("responsible-code", "batch-number", "out-part-number")
            process.transform("1234567")

    def ucase_part_shall_be_on_source(self):
        process = self.given_a_transformation_process()
        part = self.store.load(PartId("123456"))
        part.move_to("other-location")
        self.store.save(part)

        with pytest.raises(PartNotFound):
            process.start_batch("responsible-code", "batch-number", "out-part-number")
            process.transform("123456")

    def ucase_part_shall_have_input_part_number(self):
        process = self.given_a_transformation_process()

        with pytest.raises(WrongPartModel):
            process.start_batch("responsible-code", "batch-number", "out-part-number")
            process.transform("0123456")


class FeatureBatchManagement(TransformationTest):
    def ucase_batch_can_be_closed(self):
        process = self.given_a_transformation_process()

        process.start_batch("responsible-code", "batch-number", "out-part-number")
        assert process.batch_number == "batch-number"

        process.end_batch()
        assert process.batch_number is None

    def ucase_batch_is_closed_when_replaced(self):
        process = self.given_a_transformation_process()
        process.end_batch = MagicMock()

        process.start_batch("responsible-code", "batch-number", "out-part-number")

        process.start_batch("responsible-code", "other-batch-number", "out-part-number")

        process.end_batch.assert_called_with()

    def ucase_batch_can_not_start_with_wrong_outcome(self):
        process = self.given_a_transformation_process()

        with pytest.raises(FlowNotFound):
            process.start_batch("responsible-code", "batch-number", "wrong-part-number")
