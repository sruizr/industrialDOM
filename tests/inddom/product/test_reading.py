from unittest.mock import MagicMock

from inddom.product.reading import RequirementReading
from tests.inddom.build.builders import FakeStore
from tests.inddom.build.product.builders import Given


class FeatureRequirementReading:
    def ucase_reading_simple_requirement(self):
        requirement = (
            Given()
            .a_requirement.with_characteristic("attr", "element")
            .with_eid("eid")
            .with_spec(nominal=1, tolerance=1)
            .with_path("path")
            .build()
        )

        store = FakeStore()
        store.save(requirement)

        service = RequirementReading(store)

        jsonable_dict = service(requirement.id.uuid)

        expected = {
            "id": requirement.id.uuid,
            "attribute": "attr",
            "element": "element",
            "path": "path",
            "eid": "eid",
            "specs": {"nominal": 1, "tolerance": 1},
        }
        assert jsonable_dict == expected

    def ucase_reading_requirements_with_subs(self):
        sub_req = Given().a_requirement.build()
        requirement = (
            Given()
            .a_requirement.with_characteristic("attr", "element")
            .with_eid("eid")
            .with_spec(nominal=1, tolerance=1)
            .with_path("path")
            .with_requirements([sub_req])
            .build()
        )

        store = FakeStore()
        store.save(sub_req)
        store.save(requirement)

        service = RequirementReading(store)

        jsonable_dict = service(requirement.id.uuid)

        expected = {
            "id": sub_req.id.uuid,
            "attribute": sub_req.characteristic.attr,
            "element": sub_req.characteristic.element,
            "path": sub_req.path,
            "eid": sub_req.eid,
            "specs": sub_req.specs.invariants,
        }
        assert jsonable_dict["requirements"] == [expected]

    def ucase_reading_requirement_without_specs(self):
        requirement = (
            Given()
            .a_requirement.with_characteristic("attr", "element")
            .with_eid("eid")
            .with_path("path")
            .build()
        )

        store = FakeStore()
        store.save(requirement)

        service = RequirementReading(store)

        assert "specs" not in service(requirement.id.uuid)

    def ucase_reading_requirements_is_cached(self):
        requirement = (
            Given()
            .a_requirement.with_characteristic("attr", "element")
            .with_eid("eid")
            .with_spec(nominal=1, tolerance=1)
            .with_path("path")
            .build()
        )

        store = MagicMock()
        store.load.return_value = requirement

        service = RequirementReading(store)

        service(requirement.id.uuid)
        service(requirement.id.uuid)

        assert len(store.load.mock_calls) == 1
