import inddom.quality.ports as ports
from inddom.quality.inspection import InspectionService


class StubATE(ports.ATE):
    @property
    def name(self):
        return "stub-ate"


class EventObserver:
    def __init__(self, inspection: InspectionService):
        inspection.register_event_observer(self._tester_has_triggered_a_event)
        self.events = []

    def _tester_has_triggered_a_event(self, event):
        self.events.append(event)


class StubStore(ports.Store):
    def __init__(self):
        self._entities = {}

    def load(self, entity_id):
        return self._entities.get(entity_id)

    def save(self, *entities):
        for entity in entities:
            self._entities[entity.id] = entity


class StubMethods(ports.Methods):
    def __init__(self):
        self._methods = {
            "success_check_method": self._success_check_method,
            "check_method_with_error": self._error_check_method,
            "setup_method": self._setup_method,
            "setup_method_with_error": self._error_setup_method,
        }

    def __getitem__(self, key):
        return self._methods[key]

    def get(self, key, default=None):
        return self._methods.get(key, default)

    def _success_check_method(self, check, requirement, par1, par2):
        pass

    def _error_check_method(self, check, requirement, par1, par2):
        raise Exception()

    def _setup_method(self, operation, par1):
        pass

    def _error_setup_method(self, operation, par1):
        raise Exception()


class StubControlPlanFinder(ports.Query):
    def __init__(self):
        self._cps = {}

    def record_control_plan(self, cp_id, location, part_model):
        self._cps[(location, part_model)] = cp_id

    def __call__(self, location_code, part_model):
        return self._cps.get((location_code, part_model))
