import datetime

from eventence.core.commands import EventDataTransfer

from inddom.hhrr.persons import Person, PersonId
from inddom.product.definitions import (
    Characteristic,
    PartModel,
    PartModelId,
    Requirement,
    RequirementId,
    Specification,
)
from inddom.quality.plans import ControlPlan, ControlPlanId

person_id = PersonId("438")
person_dto = EventDataTransfer(
    f"{Person.__module__}:{Person.__name__}",
    person_id.uuid,
    0,
    "Hired",
    {"code": "438", "username": "sruiz", "full_name": "Ruiz Romero, Salvador"},
    datetime.datetime.now(),
    9,
)


class FeaturePersonDataPersistPersons:
    def ucase_person_data_is_inserted(self):
        data = self.data

        data.insert_person(person_dto)
        expected = {
            "id": person_id.uuid.hex,
            "full_name": "Ruiz Romero, Salvador",
            "username": "sruiz",
        }
        assert data.select_person(person_id) == expected
        assert data.last_processed_event_id == 9


requirement_id = RequirementId("path", "attr@elem", "eid")
requirement_dto = EventDataTransfer(
    f"{Requirement.__module__}:{Requirement.__name__}",
    requirement_id.uuid,
    0,
    "Defined",
    {
        "path": "path",
        "characteristic": Characteristic("attr", "elem"),
        "eid": "eid",
        "specs": Specification(limits=[1, 2]),
    },
    datetime.datetime.now(),
    5,
)

part_model_id = PartModelId("part_number")
part_model_dto = EventDataTransfer(
    f"{PartModel.__module__}:{PartModel.__name__}",
    part_model_id.uuid,
    "Defined",
    0,
    {
        "part_number": "part_number",
        "part_name": "partName",
        "description": "description",
    },
    datetime.datetime.now(),
    6,
)


class FeatureRequirementsDataPersistProjections:
    def ucase_requirement_is_persisted(self):
        requirement_data = self.data
        requirement_data.insert_requirement(requirement_dto)

        expected = {
            "id": requirement_id.uuid.hex,
            "path": "path",
            "characteristic": "attr@elem",
            "eid": "eid",
            "specs": {"limits": [1, 2]},
        }
        assert requirement_data.select_requirement(requirement_id) == expected
        assert requirement_data.last_processed_event_id == 5


class FeaturePartModelDataPersistProjections:
    def ucase_part_model_is_persisted(self):
        part_model_data = self.data
        part_model_data.insert_part_model(part_model_dto)

        expected = {
            "id": part_model_id.uuid.hex,
            "part_number": "part_number",
            "part_name": "partName",
            "description": "description",
        }
        assert part_model_data.select_part_model("part_number") == expected
        assert part_model_data.last_processed_event_id == 6


control_plan_id = ControlPlanId()
control_plan_dtos = [
    EventDataTransfer(
        f"{ControlPlan.__module__}:{ControlPlan.__name__}",
        control_plan_id.uuid,
        0,
        "Created",
        {"from_location_code": "loc1", "to_location_code": "loc2"},
        datetime.datetime.now(),
        9,
    ),
    EventDataTransfer(
        f"{ControlPlan.__module__}:{ControlPlan.__name__}",
        control_plan_id.uuid,
        1,
        "AffectedAdded",
        {"affected": "partgroup"},
        datetime.datetime.now(),
        10,
    ),
]


class FeatureControlPlanDataProjectsEvents:
    def ucase_control_plan_data_persists_control_plans(self):
        data = self.data
        data.insert_control_plan(control_plan_dtos[0])
        data.update_add_affected(control_plan_dtos[1])

        assert data.last_processed_event_id == 10
        assert data.select_control_plan_id("loc1", "partgroup") == control_plan_id
        assert not data.select_control_plan_id("loc3", "partgroup")
        assert not data.select_control_plan_id("loc1", "group")
