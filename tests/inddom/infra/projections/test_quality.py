from unittest.mock import MagicMock

from inddom.infra.projections.quality import ControlPlanFinder, ControlPlansData


class FeatureControlPlanAsProjection:
    def ucase_with_hooks_available_to_projector(self):
        projection_data = MagicMock(spec=ControlPlansData)
        projection = ControlPlanFinder(projection_data)

        assert projection.event_hooks

    def ucase_projection_have_last_event_id(self):
        projection_data = MagicMock(spec=ControlPlansData)
        projection = ControlPlanFinder(projection_data)

        assert projection.last_event_id == projection_data.last_processed_event_id


class FeatureControlPlanAsQuery:
    def ucase_query_delegate_result_from_data(self):
        projection_data = MagicMock(spec=ControlPlansData)
        query = ControlPlanFinder(projection_data)

        control_plan_id = query("location", "product_key")
        assert control_plan_id == projection_data.select_control_plan_id.return_value
        projection_data.select_control_plan_id.assert_called_with(
            "location", "product_key"
        )
