from inddom.infra.projections.onmem import (
    OnmemControlPlansData,
    OnmemPartModelsData,
    OnmemPersonsData,
    OnmemRequirementsData,
)

from . import templates as _


class FeatureControlPlanDataProjectsEvents(_.FeatureControlPlanDataProjectsEvents):
    def setup_method(self):
        self.data = OnmemControlPlansData()


class FeaturePartModelDataPersistProjections(_.FeaturePartModelDataPersistProjections):
    def setup_method(self):
        self.data = OnmemPartModelsData()


class FeatureRequirementsDataPersistProjections(
    _.FeatureRequirementsDataPersistProjections
):
    def setup_method(self):
        self.data = OnmemRequirementsData()


class FeaturePersonsDataPersistence(_.FeaturePersonDataPersistPersons):
    def setup_method(self):
        self.data = OnmemPersonsData()
