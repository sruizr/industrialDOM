from unittest.mock import MagicMock

from inddom.infra.projections.hhrr import PersonId, PersonInfoGetter, PersonsData


class FeaturePersonInfoGetterAsProjection:
    def ucase_with_hooks_available_to_projector(self):
        projection_data = MagicMock(spec=PersonsData)
        projection = PersonInfoGetter(projection_data)

        assert projection.event_hooks

    def ucase_projection_have_last_event_id(self):
        projection_data = MagicMock(spec=PersonsData)
        projection = PersonInfoGetter(projection_data)

        assert projection.last_event_id == projection_data.last_processed_event_id


class FeaturePersonInfoGetterAsQuery:
    def ucase_query_delegate_result_from_data(self):
        projection_data = MagicMock(spec=PersonsData)
        query = PersonInfoGetter(projection_data)

        person_info = query("person-code")
        assert person_info == projection_data.select_person.return_value
        projection_data.select_person.assert_called_with(PersonId("person-code"))
