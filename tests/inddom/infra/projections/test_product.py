from unittest.mock import MagicMock

from inddom.infra.projections.product import (
    PartModelId,
    PartModelInfoGetter,
    PartModelsData,
    RequirementId,
    RequirementInfoGetter,
    RequirementsData,
)


class FeatureRequirementInfoGetterAsProjection:
    def ucase_with_hooks_available_to_projector(self):
        projection_data = MagicMock(spec=RequirementsData)
        projection = RequirementInfoGetter(projection_data)

        assert projection.event_hooks

    def ucase_projection_have_last_event_id(self):
        projection_data = MagicMock(spec=RequirementsData)
        projection = RequirementInfoGetter(projection_data)

        assert projection.last_event_id == projection_data.last_processed_event_id


class FeatureRequirementInfoGetterAsQuery:
    def ucase_query_delegate_result_from_data(self):
        projection_data = MagicMock(spec=RequirementsData)
        query = RequirementInfoGetter(projection_data)

        requirement_info = query("requirement-id")
        assert requirement_info == projection_data.select_requirement.return_value
        projection_data.select_requirement.assert_called_with("requirement-id")


class FeaturePartModelInfoGetterAsProjection:
    def ucase_with_hooks_available_to_projector(self):
        projection_data = MagicMock(spec=PartModelsData)
        projection = PartModelInfoGetter(projection_data)

        assert projection.event_hooks

    def ucase_projection_have_last_event_id(self):
        projection_data = MagicMock(spec=PartModelsData)
        projection = PartModelInfoGetter(projection_data)

        assert projection.last_event_id == projection_data.last_processed_event_id


class FeaturePartModelInfoGetterAsQuery:
    def ucase_query_delegate_result_from_data(self):
        projection_data = MagicMock(spec=PartModelsData)
        query = PartModelInfoGetter(projection_data)

        part_model_info = query("part-number")
        assert part_model_info == projection_data.select_part_model.return_value
        projection_data.select_part_model.assert_called_with(PartModelId("part-number"))
