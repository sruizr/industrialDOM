from inddom.infra.projections import sqlite

from . import templates as _


class FeatureControlPlanDataProjectsEvents(_.FeatureControlPlanDataProjectsEvents):
    def setup_method(self):
        self.data = sqlite.SqliteControlPlansData(filename=":memory:")


class FeaturePartModelDataPersistProjections(_.FeaturePartModelDataPersistProjections):
    def setup_method(self):
        self.data = sqlite.SqlitePartModelsData(filename=":memory:")


class FeatureRequirementsDataPersistProjections(
    _.FeatureRequirementsDataPersistProjections
):
    def setup_method(self):
        self.data = sqlite.SqliteRequirementsData(filename=":memory:")


class FeaturePersonsDataPersistence(_.FeaturePersonDataPersistPersons):
    def setup_method(self):
        self.data = sqlite.SqlitePersonsData(filename=":memory:")
