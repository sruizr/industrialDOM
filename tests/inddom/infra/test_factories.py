from unittest.mock import patch

import eventence as ev

from inddom.infra.factories import PersistenceFactory
from inddom.infra.projections.quality import ControlPlanFinder


class FeaturePersistenceCreation:
    def setup_method(self):
        self._importlib_patch = patch("inddom.infra.factories.importlib")
        self._eventence_patch = patch("inddom.infra.factories.ev")
        self._control_plan_patch = patch(
            "inddom.infra.factories.ControlPlanFinder", spec=ControlPlanFinder
        )

        self.importlib = self._importlib_patch.start()
        self.ControlPlanFinder = self._control_plan_patch.start()
        self.eventence = self._eventence_patch.start()
        self.ev_persistence = self.eventence.Persistence.return_value

    def tear_down(self):
        self._importlib_patch.stop()
        self._control_plan_patch.stop()
        self._eventence_patch.stop()

    def ucase_create_store(self):
        engine = ev.DbEngine("sqlite", {"filename": ":memory:"})
        factory = PersistenceFactory(engine)

        store = factory.create_store()

        self.eventence.Persistence.assert_called_with(engine)
        assert store == self.ev_persistence.create_store.return_value

    def ucase_create_control_plan_finder_query(self):
        engine = ev.DbEngine("sqlite", {"filename": ":memory:"})
        factory = PersistenceFactory(engine)

        query = factory.create_control_plan_finder(
            ev.DbEngine("sqlite", {"filename": ":memory:"})
        )

        module = self.importlib.import_module.return_value
        self.importlib.import_module.assert_called_with(
            "inddom.infra.projections.sqlite"
        )
        module.SqliteControlPlansData.assert_called_with(filename=":memory:")
        self.ControlPlanFinder.assert_called_with(
            module.SqliteControlPlansData.return_value
        )

        assert query == self.ControlPlanFinder.return_value
