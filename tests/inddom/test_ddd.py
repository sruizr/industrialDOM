import datetime

import pytest

from inddom.ddd import Aggregate, Event, KeyId, RandomId, Value


def reload_aggregate(aggregate: Aggregate, new_aggregate: Aggregate):
    """Construct a new aggregate from the inner events of aggregate"""
    observer = EventObserver()
    aggregate.event_handler.add_callback(observer.event_is_triggered)
    new_aggregate.event_handler.build_entity(observer.events)

    return new_aggregate


class EventObserver:
    def __init__(self):
        self.events = []

    def event_is_triggered(self, event):
        self.events.append(event)


class FeatureKeyIdcreateUUId:
    def ucase_uuid_is_replicated(self):
        id_1 = KeyId(FakeAggregate, "2", "3")
        id_2 = KeyId(FakeAggregate, "2", "3")

        assert id_1 == id_2

    def ucase_uuid_admits_none(self):
        id = KeyId(FakeAggregate, None)
        assert id


class FeatureRandomIdcreatesUUId:
    def ucase_uuid_creates_one_if_none_arg(self):
        id_1 = RandomId()
        id_2 = RandomId(None)

        assert id_1 != id_2

    def ucase_uuid_creates_same_uuid(self):
        id_1 = RandomId()
        id_2 = RandomId(uuid=id_1.uuid)

        assert id_1 == id_2


class FakeAggregate(Aggregate):
    class FakeEvent(Event):
        pass

    class Modified(Event):
        def apply_on(self, aggregate):
            aggregate.foo = self.foo

    class Created(Event):
        def apply_on(self, aggregate):
            aggregate._id = self.id
            aggregate.par_1 = self.par_1

    def create(self, par_1):
        id = RandomId()
        self._trigger_event(self.Created(id=id, par_1=par_1))

    def modify(self, foo):
        self._trigger_event(self.Modified(foo=foo))


class FeatureAggregateKeepsHistory:
    def ucase_history_is_recorded_with_event_dtos(self):
        aggregate = FakeAggregate()
        aggregate.create(4)
        aggregate.modify("buzz")
        aggregate.modify("foo")

        history = aggregate.event_handler.history
        now = datetime.datetime.now()
        assert len(history) == 3
        assert history[-3].event_name == "Created"
        assert history[-2].event_pars == {"foo": "buzz"}
        assert history[-1].version == 2
        assert (now - history[-1].created_on).seconds < 1
        assert history[-1].__class__.__name__ == "EventDataTransfer"

    def ucase_aggregate_reconstructed_from_history(self):
        aggregate = FakeAggregate()
        aggregate.create(4)
        aggregate.modify("buzz")
        aggregate.modify("foo")

        history = aggregate.event_handler.history
        other_aggregate = FakeAggregate()
        other_aggregate.event_handler.build_entity(history)

        assert aggregate == other_aggregate
        assert aggregate.foo == other_aggregate.foo
        assert aggregate.par_1 == other_aggregate.par_1


class FeatureEventFlow:
    def ucase_event_parameters_can_not_be_changed(self):
        event = Event(par1=1, par2="a")

        assert event.par1 == 1
        assert event.par2 == "a"

        try:
            event.part = 2
            pytest.fail("Attribute error should be raised")
        except AttributeError:
            pass


class FeatureDomainRepresentation:
    def ucase_event_has_representation(self):
        event = FakeAggregate.FakeEvent(par1=1, par2=3)

        assert repr(event) == "FakeEvent(par1=1, par2=3)"


class FeatureAggregateEventsAreObservable:
    def ucase_test_aggregate_notify_to_observers_event_dtos(self):
        aggregate = FakeAggregate()
        observer = EventObserver()
        aggregate.event_handler.add_observer(observer)

        aggregate.create(1)
        aggregate.modify("foo")

        now = datetime.datetime.now()
        created = observer.events[-2]
        assert created.event_name == "Created"
        expected_pars = {"id": aggregate.id, "par_1": 1}
        assert created.event_pars == expected_pars
        assert created.version == 0
        assert (now - created.created_on).seconds < 1
        assert created.entity_name == "tests.test_ddd:FakeAggregate"

        modified = observer.events[-1]
        assert modified.event_name == "Modified"
        assert modified.event_pars == {"foo": "foo"}


class FakeValue(Value):
    pass


class FeatureValueIsInvariant:
    def ucase_value_has_no_new_attributes(self):
        value = Value(par1=1, par2="par2")

        assert value.par1 == 1
        assert value.par2 == "par2"

        try:
            value.par3 = 3
            pytest.fail("AttributeError should be raissed")
        except AttributeError:
            pass

    def ucase_value_attributes_can_not_change(self):
        value = Value(par1=1, par2="par2")

        assert value.par1 == 1
        assert value.par2 == "par2"

        try:
            value.par1 = 3
            pytest.fail("AttributeError should be raissed")
        except AttributeError:
            pass

    def ucase_two_values_with_same_parameters_are_equal(self):
        value = FakeValue(par1=1, par2="par2")
        other_value = FakeValue(par1=1, par2="par2")

        assert value == other_value

    def ucase_two_values_with_diferent_parameters_are_not_equal(self):
        value = FakeValue(par1=1, par2="par2")
        diferent_value = FakeValue(par1=1, par2="par22")

        assert value != diferent_value

    def ucase_two_same_values_has_same_hash(self):
        value = FakeValue(par1=1, par2="par2")
        same_value = FakeValue(par1=1, par2="par2")
        diferent_value = FakeValue(par1=1, par2="par22")

        assert hash(value) == hash(same_value)


class FeatureAggregateComparison:
    def ucase_two_aggregates_with_same_id_has_same_hash(self):
        aggregate_1 = FakeAggregate()

        aggregate_2 = FakeAggregate()
        aggregate_2._id = aggregate_1.id

        assert aggregate_1.id == aggregate_2.id

        assert hash(aggregate_1) == hash(aggregate_2)
