from unittest.mock import MagicMock, patch

import pytest

from inddom import values
from inddom.logistics.parts import Part, PartId
from inddom.manufacturing.equipments import EquipmentId
from inddom.manufacturing.stations import PartNotInLocation, Station, StationId
from tests.inddom.test_ddd import reload_aggregate


class StationContext:
    def given_a_station(self):
        station = Station()
        station.identify("station-code", "station-description")
        station.set_origin("origin")

        return station


class FeatureStationDefinition(StationContext):
    def ucase_station_is_identified(self):
        station = self.given_a_station()

        assert station.id == StationId("station-code")
        assert station.description == "station-description"
        assert station.code == "station-code"

    def ucase_station_has_origin(self):
        station = self.given_a_station()

        station.set_origin("origin")

        assert "origin" in station.origin
        assert station.force_part_origin

        station.set_origin("*origin")
        assert "origin" in station.origin
        assert not station.force_part_origin

        station.set_origin("origin", force_part_origin=False)
        assert "origin" in station.origin
        assert not station.force_part_origin


class FeatureStationAsEquipmentContainer(StationContext):
    def ucase_station_has_equipments(self):
        station = self.given_a_station()

        equipments = [MagicMock(id=EquipmentId(f"Equipment{i}")) for i in range(2)]
        station.add_equipments(*equipments)

        assert station.equipments == equipments
        assert station.equipment_ids == tuple([eq.id for eq in equipments])

    def ucase_load_station_with_equipment_codes(self):
        station = self.given_a_station()

        station.add_equipments_from_codes("eq-code1", "eq-code2")

        expected = (EquipmentId("eq-code1"), EquipmentId("eq-code2"))
        assert station.equipment_ids == expected
        assert not station.equipments


class FeatureStationPersistence(StationContext):
    def ucase_station_can_reconstruct(self):
        station = self.given_a_station()
        station.set_origin("*origin")
        station.add_equipments(MagicMock(id=EquipmentId("eq")))

        reloaded_station = Station()
        reload_aggregate(station, reloaded_station)

        assert station == reloaded_station
        assert station.description == reloaded_station.description
        assert station.equipment_ids == reloaded_station.equipment_ids
        assert station.origin == reloaded_station.origin
        assert station.force_part_origin == reloaded_station.force_part_origin


class FeaturePartCollecting(StationContext):
    def given_part_info(self):
        return values.PartInfo("part-number", "serial_number")

    def ucase_station_collect_part_from_store(self):
        station = self.given_a_station()

        part_info = self.given_part_info()
        store = MagicMock()
        store.load.return_value.location_code = "origin"
        part = station.collect_part(part_info, store)

        assert part == store.load.return_value
        store.load.assert_called_with(PartId(part_info.serial_number))

    def ucase_station_create_part_if_no_exist(self):
        station = self.given_a_station()

        part_info = self.given_part_info()
        store = MagicMock()
        store.load.return_value = None

        with patch("inddom.manufacturing.stations.Part", spec=Part) as MockPart:
            expected_part = MockPart.return_value
            expected_part.location_code = station.origin
            part = station.collect_part(part_info, store)
            assert part == expected_part
            part.create.assert_called_with(
                part_info.part_number,
                part_info.serial_number,
                station.origin,
                part_info.extra_info,
            )

    def ucase_station_raises_part_not_in_place(self):
        station = self.given_a_station()

        part_info = self.given_part_info()
        store = MagicMock()
        store.load.return_value.location_code = "not-origin"
        with pytest.raises(PartNotInLocation):
            station.collect_part(part_info, store)

    def ucase_station_moves_part_to_origin(self):
        station = self.given_a_station()
        station.set_origin("*origin")

        part_info = self.given_part_info()
        store = MagicMock()
        store.load.return_value.location_code = "not-origin"
        part = station.collect_part(part_info, store)

        part.move_to.assert_called_with(station.origin)


class FeatureDeviceLoading(StationContext):
    def given_a_station_with_equipments(self, *equipments):
        station = self.given_a_station()
        station.add_equipments(*equipments)

        return station

    def ucase_load_equipment_from_store(self):
        equipment = MagicMock(id=EquipmentId("eq1"))
        store = MagicMock()
        store.load.return_value = equipment
        station = self.given_a_station_with_equipments(equipment)
        station.load_equipments(store)

        store.load.assert_called_with(EquipmentId("eq1"))
        assert station.equipments == [equipment]

    def ucase_load_epi_from_equipment(self):
        equipment = MagicMock(id=EquipmentId("eq1"))
        store = MagicMock()
        store.load.return_value = equipment
        station = self.given_a_station_with_equipments(equipment)
        create_equipment = MagicMock()

        station.load_equipments(store, create_equipment)

        equipment.load_equipment_programming_interface.assert_called_with(
            create_equipment, store
        )
