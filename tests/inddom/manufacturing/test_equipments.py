from unittest.mock import MagicMock

import pytest

from inddom.manufacturing.equipments import Equipment, EquipmentId, EquipmentNotValid
from tests.inddom.test_ddd import reload_aggregate


class EquipmentContext:
    def given_a_equipment(self, epi_pars=None):
        equipment = Equipment()
        equipment.identify("eq-code", "eq-name", "eq-description", "epi-name")
        epi_pars = (
            epi_pars if epi_pars else {"par1": 1, "equipment": "EquipmentId(eq-2)"}
        )
        equipment.config_epi(epi_pars)

        return equipment


class FeatureEquipmentDefinition(EquipmentContext):
    def ucase_equipment_has_main_attributes_accessibles(self):
        equipment = self.given_a_equipment()

        assert equipment.id == EquipmentId("eq-code")
        assert equipment.code == "eq-code"
        assert equipment.name == "eq-name"
        assert equipment.description == "eq-description"
        assert equipment.epi_name == "epi-name"
        assert equipment.epi is None

    def ucase_equipment_has_epi_parameters(self):
        equipment = self.given_a_equipment()
        epi_pars = {"par1": 1, "equipment": "EquipmentId(eq-2)"}
        equipment.config_epi(epi_pars)

        assert equipment.epi_pars == epi_pars


class FeatureEquipmentPersistence(EquipmentContext):
    def ucase_equipment_is_reloable(self):
        equipment = self.given_a_equipment()

        reloaded = Equipment()

        reload_aggregate(equipment, reloaded)

        assert reloaded.id == EquipmentId("eq-code")
        assert reloaded.code == "eq-code"
        assert reloaded.name == "eq-name"
        assert reloaded.description == "eq-description"
        assert reloaded.epi_name == "epi-name"
        assert reloaded.epi_pars == equipment.epi_pars
        assert reloaded.epi is None


class FeatureProgrammingInterfaceLoading(EquipmentContext):
    def ucase_equipment_programming_interface_is_loaded(self):
        store = MagicMock()
        create_equipment = MagicMock()

        equipment = self.given_a_equipment()
        equipment.config_epi({"par1": 1})

        equipment.load_equipment_programming_interface(create_equipment, store)

        assert equipment.epi == create_equipment.return_value
        create_equipment.assert_called_with(equipment.epi_name, equipment.epi_pars)

    def ucase_equipment_loads_its_dependencies(self):
        store = MagicMock()
        create_equipment = MagicMock()

        equipment = self.given_a_equipment()
        equipment.config_epi({"par1": 1, "eq": "EquipmentId(eq2)"})

        equipment.load_equipment_programming_interface(create_equipment, store)

        create_equipment.assert_called_with(
            equipment.epi_name,
            {"par1": 1, "eq": store.load.return_value.epi},
        )

        store.load.assert_called_with(EquipmentId("eq2"))

    def ucase_equipment_has_a_caducated_validation(self):
        store = MagicMock()
        create_equipment = MagicMock()

        equipment = self.given_a_equipment()
        equipment.config_epi({"par1": 1, "eq": "EquipmentId(eq2)"})

        equipment.load_equipment_programming_interface(create_equipment, store)

        create_equipment.assert_called_with(
            equipment.epi_name,
            {"par1": 1, "eq": store.load.return_value.epi},
        )

        store.load.assert_called_with(EquipmentId("eq2"))

    def ucase_equipment_raises_equipment_not_valid_if_has_a_caducated_validation(self):
        store = MagicMock()
        create_equipment = MagicMock()
        person = MagicMock()
        equipment = self.given_a_equipment()
        equipment.validate(-2, person)

        with pytest.raises(EquipmentNotValid) as e:
            equipment.load_equipment_programming_interface(create_equipment, store)
