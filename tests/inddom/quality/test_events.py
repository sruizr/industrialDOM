import json

import inddom.quality.events as ev
from inddom.product.definitions import Characteristic
from inddom.quality.tests import Measurement, TestId
from inddom.values import RequirementDataTransfer, TestResolution


class A_ActionHasFinishedEvent:
    def should_be_jsonable(self):
        event = ev.ActionHasFinishedEvent(
            "loc", "result", Exception("something is wrong")
        )

        value = event.as_plain_dict()
        assert value
        assert json.dumps(value)


class A_ActionHasStartedEvent:
    def should_be_jsonable(self):
        event = ev.ActionHasStartedEvent("loc", ["what", "ever"])

        value = event.as_plain_dict()
        assert value
        assert json.dumps(value)


class A_CheckHasStartedEvent:
    def should_be_jsonable(self):
        req = RequirementDataTransfer(
            "path", {"attr": "attr", "element": "element"}, "eid", {"limits": [1, 2]}
        )

        event = ev.CheckHasStartedEvent(
            "loc",
            RequirementDataTransfer(
                "path",
                {"attr": "attr", "element": "element"},
                "eid",
                {"limits": [1, 2]},
                [req],
            ),
        )

        value = event.as_plain_dict()
        assert value
        assert json.dumps(event.as_plain_dict())


class A_CheckHasFinishedEvent:
    def should_be_jsonable(self):
        measurements = [
            Measurement(
                Characteristic("attr", "element"),
                "eid",
                TestId(),
                122.0,
                TestId(),
                "tracking",
                1,
            )
        ]
        defects = []
        event = ev.CheckHasFinishedEvent(
            "loc", "result", measurements, defects, Exception("somethin is wrong")
        )
        value = event.as_plain_dict()
        assert value
        assert json.dumps(event.as_plain_dict())


class A_TestHasFinishedEvent:
    def should_be_jsonable(self):
        event = ev.TestHasFinishedEvent("loc", TestResolution.CANCELLED)
        value = event.as_plain_dict()
        assert value
        assert json.dumps(event.as_plain_dict())
