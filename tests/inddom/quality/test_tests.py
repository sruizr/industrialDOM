from threading import Thread
from unittest.mock import MagicMock

import pytest

from inddom.hhrr.persons import NotAuthorizedPerson
from inddom.product.definitions import Specification
from inddom.quality.plans import MethodPars
from inddom.quality.tests import (
    Action,
    Check,
    Defect,
    Measurement,
    Test,
    TestId,
    WrongPartForTesting,
)
from inddom.values import Request, Response
from tests.inddom import EventObserver

# given = Given()                 #


class FeatureTestIdIsIdentification:
    def ucase_test_id_is_generated_randomly(self):
        a_test_id = TestId()
        other_test_id = TestId()
        assert a_test_id != other_test_id

    def ucase_test_id_can_be_reproduced(self):
        test_id = TestId()

        other_test_id = TestId(uuid=test_id.uuid)
        assert test_id == other_test_id


class TestContext:
    def given_a_test_prepared(self):
        test = Test()
        self.observer = EventObserver()
        test.event_handler.add_to_history = self.observer.record_event
        self.person = MagicMock(name="person")
        test.prepare("location-code", self.person, {"par1": 1})
        return test

    def given_a_test_with_part(self):
        test = self.given_a_test_prepared()
        self.part = MagicMock(name="part")
        test.take_part(self.part)
        return test


class FeatureTestFlow(TestContext):
    def ucase_test_preparation_creates_id(self):
        test = self.given_a_test_prepared()
        assert test.id is not None

    def ucase_test_preparation_raise_prepared_event(self):
        test = self.given_a_test_prepared()

        assert (
            test.Prepared(
                responsible_id=self.person.id,
                location_code="location-code",
                test_id=test.id,
            )
            == self.observer.events[-1]
        )

    def ucase_test_part_is_taken(self):
        test = self.given_a_test_with_part()

        assert test.PartIsTaken(part_id=self.part.id) == self.observer.events[-1]

    def should_raise_check_has_started(self):
        def method(check, req, **pars):
            pass

        test = self.given_a_test_with_part()

        requirement = MagicMock(name="requirement", id=1)
        method_pars = MagicMock(methods="name")
        method_pars.invariants = {"par1": 1}
        test.run_check(method, requirement, method_pars)

        expected_event = test.CheckIsStarted(
            method_name="tests.inddom.quality.test_tests:method",
            requirement_id=requirement.id,
        )
        assert expected_event == self.observer.events[-2]

    def should_trigger_check_has_finished_event(self):
        def method(check, req, **pars):
            pass

        test = self.given_a_test_with_part()

        requirement = MagicMock(name="requirement", id=1)
        method_pars = MagicMock(methods="name")
        method_pars.invariants = {"par1": 1}
        assert "ok" == test.run_check(method, requirement, method_pars)

        expected_event = test.CheckIsOK()
        assert expected_event == self.observer.events[-1]

    def should_trigger_test_is_success_event(self):
        test = self.given_a_test_with_part()
        test.finish()

        assert test.resolution == "success"
        expected = test.Success()
        assert expected == self.observer.events[-1]

    def should_be_cancelled(self):
        test = self.given_a_test_with_part()
        test.cancel()
        test.finish()

        assert test.resolution == "cancelled"
        assert test.Cancelled(message=None) == self.observer.events[-1]

    def ucase_test_resolution_is_none_while_check_is_ok(self):
        def method(check, req, **pars):
            pass

        test = self.given_a_test_with_part()

        test.run_check(method, MagicMock(name="requirement", id=1), {})

        assert test.resolution is None

    def ucase_test_has_failed(self):
        def method(check, req, **pars):
            check.add_part_defect(req, "incorrect")

        test = self.given_a_test_with_part()

        assert "nok" == test.run_check(method, MagicMock(name="requirement", id=1), {})

        test.finish()

        assert test.resolution == "failed"
        assert test.Failed() == self.observer.events[-1]


class CheckContext(TestContext):
    def given_a_check(self):
        self.test = self.given_a_test_with_part()
        self.requirement = MagicMock(name="requirement")
        return Check(self.test, self.requirement)


class FeatureChecksRunMethods(CheckContext):
    def ucase_check_runs_with_ok_results(self):
        check = self.given_a_check()
        check.run(MagicMock(), MethodPars(par1=1))

        assert check.result == "ok"

    def ucase_check_runs_with_nok_results(self):
        check = self.given_a_check()

        def method(check: Check, requirement, par1):
            check.add_part_defect(requirement, "incorrect")

        check.run(method, MethodPars(par1=1))

        assert check.result == "nok"

    def ucase_check_runs_cancelled_by_exception(self):
        check = self.given_a_check()

        def method(check: Check, requirement, par1):
            raise Exception()

        with pytest.raises(Exception):
            check.run(method, MethodPars(par1=1))

        assert check.result == "error"
        check.test.finish()
        assert check.test.resolution == "cancelled"

    def _ucase_check_can_wait_for_responses(self):
        def method(check, requirement, **pars):
            method.response = check.wait("request", loop_time=0)

        a_check = given.a_check
        test = a_check.test
        client = a_check.client
        client.get_response.return_value = None

        thread = Thread(
            target=test.run_check,
            args=(method, a_check.requirement, a_check.method_pars),
        )
        thread.start()

        assert thread.is_alive()
        client.get_response.return_value = "response"
        thread.join()

        wait_event = a_check.observer.get_last_event(3)
        assert wait_event.name == "CheckIsWaiting"
        assert wait_event.pars["request"] == "request"
        continue_event = a_check.observer.get_last_event(2)
        assert continue_event.name == "CheckContinues"
        assert continue_event.pars["response"] == "response"

        client.get_response.assert_called_with("request")

        assert method.response == "response"


class FeatureCheckManageDefects(CheckContext):
    def ucase_check_add_part_defects(self):
        check = self.given_a_check()

        check.add_part_defect(self.requirement, "fm", ms_tracking="tracking")

        defect = Defect(
            self.requirement.id,
            "fm",
            self.part.id,
            self.test.id,
            ms_tracking="tracking",
        )

        assert check.defects[0] == defect
        self.part.add_defect.assert_called_with(defect)


class FeatureCheckManageMeasurements(CheckContext):
    def ucase_check_add_part_measurements_and_eval_it(self):
        check = self.given_a_check()
        requi = self.requirement
        requi.specs.invariants = {"limits": [1, 2]}

        check.add_part_measurement_and_eval(self.requirement, 3, "tracking")

        measurement = Measurement(
            requi.characteristic,
            requi.eid,
            self.part.id,
            3,
            self.test.id,
            "tracking",
        )

        assert check.measurements[0] == measurement

        defect = Defect(
            requi.id,
            "high",
            self.part.id,
            self.test.id,
            ms_tracking="tracking",
        )
        assert check.defects[0] == defect


class FeatureMeasurementsEvaluatesRequirement:
    def given_a_requirement(self, specs):
        requirement = MagicMock()
        requirement.id = "requirement_id"
        requirement.specs = specs
        return requirement

    def given_a_measurement(self, value):
        measurement = Measurement(MagicMock(), "eid", "subject_id", value, "test_id")
        return measurement

    def ucase_measurement_evals_correct_interval_values(self):
        requirement = self.given_a_requirement(Specification(limits=[1, 3]))
        measurement = self.given_a_measurement(1.5)

        assert not measurement.evaluate(requirement)

    def ucase_measurement_evals_tolerances(self):
        requirement = self.given_a_requirement(Specification(tolerance=1, nominal=1))

        measurement = self.given_a_measurement(1.5)
        assert not measurement.evaluate(requirement)

        measurement = self.given_a_measurement(2.5)
        defect = measurement.evaluate(requirement)
        assert defect.failure_mode == "high"

    def ucase_measurement_evals_circular_interval_values(self):
        requirement = self.given_a_requirement(Specification(limits=[3, 1]))
        measurement = self.given_a_measurement(3.6)

        assert not measurement.evaluate(requirement)

        measurement = self.given_a_measurement(0.2)
        assert not measurement.evaluate(requirement)

        measurement = self.given_a_measurement(2)
        defect = measurement.evaluate(requirement)
        assert defect.failure_mode == "incorrect"

    def ucase_measurement_evals_low_values(self):
        requirement = self.given_a_requirement(Specification(limits=[1, 3]))
        measurement = self.given_a_measurement(0.5)

        assert measurement.evaluate(requirement).failure_mode == "low"

    def ucase_measurement_evals_high_values(self):
        requirement = self.given_a_requirement(Specification(limits=[1, 3]))
        measurement = self.given_a_measurement(4.5)

        assert measurement.evaluate(requirement).failure_mode == "high"

    def ucase_measurement_evals_suspicios_low_values(self):
        requirement = self.given_a_requirement(Specification(limits=[1, 3]))
        measurement = self.given_a_measurement(1.5)

        assert measurement.evaluate(requirement, 1).failure_mode == "suspicious-low"

    def ucase_measurement_evals_suspicious_high_values(self):
        requirement = self.given_a_requirement(Specification(limits=[1, 3]))
        measurement = self.given_a_measurement(2.5)

        assert measurement.evaluate(requirement, 1).failure_mode == "suspicious-high"

    def ucase_measurement_evals_no_limits(self):
        requirement = self.given_a_requirement(Specification(limits=[None, None]))
        measurement = self.given_a_measurement(0.5)

        assert not measurement.evaluate(requirement)

    def ucase_measurement_evals_correct_value_requirements(self):
        requirement = self.given_a_requirement(Specification(value=1))
        measurement = self.given_a_measurement(1)

        assert not measurement.evaluate(requirement)

    def ucase_measurement_evals_wrong_value_requirements(self):
        requirement = self.given_a_requirement(Specification(value=1))
        measurement = self.given_a_measurement(0)

        assert measurement.evaluate(requirement).failure_mode == "wrong"

    def ucase_measurement_evas_wrong_values_requirement(self):
        requirement = self.given_a_requirement(Specification(values=[1, 2]))
        measurement = self.given_a_measurement(0)

        assert measurement.evaluate(requirement).failure_mode == "wrong"

    def ucase_measurement_evals_correct_values_requirements(self):
        requirement = self.given_a_requirement(Specification(values=[1, 2]))
        measurement = self.given_a_measurement(1)

        assert not measurement.evaluate(requirement)

    def ucase_measurement_create_linked_defects(self):
        requirement = self.given_a_requirement(Specification(value=1))
        measurement = self.given_a_measurement(0)

        defect = measurement.evaluate(requirement)

        assert defect.requirement_id == requirement.id
        assert defect.subject_id == measurement.subject_id
        assert defect.test_id == measurement.test_id
        assert defect.index == measurement.index


class FeatureActionRuns(TestContext):
    def given_an_action(self):
        self.test = self.given_a_test_with_part()
        action = Action(self.test)
        return action

    def ucase_action_starts_and_finish_properly(self):
        action = self.given_an_action()

        def method(_, par1):
            pass

        action.run(method, MethodPars(par1=1))

        assert action.result == "done"

    def ucase_action_finish_with_cancellation(self):
        action = self.given_an_action()

        def method(_, par1):
            raise Exception()

        with pytest.raises(Exception) as ex:
            action.run(method, MethodPars(par1=1))

        assert action.result == "error"


def eval_generator(check, requirement):
    requirement.status = "before request"
    yield "I need something"
    requirement.status = "after request"


class FeatureWaitingTests(TestContext):
    def should_run_generators(self):
        test = self.given_a_test_with_part()
        requirement = MagicMock()

        request = test.run_check(eval_generator, requirement)
        assert request == Request("I need something")
        assert requirement.status == "before request"

    def should_be_answered(self):
        test = self.given_a_test_with_part()
        requirement = MagicMock()

        request = test.run_check(eval_generator, requirement)

        response = Response("here you are", request)
        test.answer(response)

        assert test.last_action.response == response
        assert requirement.status == "after request"

    def should_trigger_events(self):
        test = self.given_a_test_with_part()
        requirement = MagicMock()

        request = test.run_check(eval_generator, requirement)
        assert test.CheckIsWaiting(request=request) in self.observer.events

        response = Response("here you are", request)
        test.answer(response)
        assert test.CheckContinues() in self.observer.events
