from unittest.mock import MagicMock, call, patch

import pytest

import inddom.quality.events as events
import tests.inddom.adapters as adp
from inddom import values
from inddom.hhrr.persons import PersonId
from inddom.infra.factories import ControlPlanFinder
from inddom.logistics.parts import PartId
from inddom.manufacturing.stations import Station, StationId
from inddom.product.definitions import PartModelId, Requirement, RequirementId
from inddom.quality import inspection as insp
from inddom.quality.plans import Control, ControlPlanId, Step


class InspectionContext:
    def given_a_inspection_service(self):
        self.store = MagicMock()
        self.station = MagicMock(name="station")
        self.location_code = "station-code_1"
        self.station.equipments = [MagicMock(name=f"eq-{index}") for index in range(2)]
        self.part = self.station.collect_part.return_value
        self.person = MagicMock(name="person")
        self.store.load.side_effect = [self.station, self.person]
        self.methods = MagicMock()
        self.control_plan_finder = MagicMock(spec=ControlPlanFinder)
        self.create_equipment = MagicMock()
        inspection = insp.InspectionService(
            self.methods, self.store, self.control_plan_finder, self.create_equipment
        )
        self.observer = adp.EventObserver(inspection)

        return inspection

    def given_a_inspection_service_with_responsible(self, controls=None):
        inspection = self.given_a_inspection_service()
        inspection.take_responsability("station-code", "person-code")
        self.part_model = MagicMock(name="part-model")
        self.control_plan = MagicMock(name="control-plan")
        self.controls = controls if controls else []
        self.control_plan.steps = self.controls
        return inspection

    def setup_method(self):
        self._tests_patch = patch("inddom.quality.inspection.tests")
        self._tests = self._tests_patch.start()
        self.test = self._tests.Test.return_value
        self.test.run_check.return_value = None
        self.check = self.action = self.test.last_action

    def tear_down(self):
        self._tests_patch.stop()

    def given_a_inspection_service_prepared_for(
        self, controls=None, till_first_failure=True
    ):
        inspection = self.given_a_inspection_service_with_responsible(controls)
        self.location = "station-code_1"
        self.part_info = values.PartInfo("part-number", "serial-nubmer")
        self.requirement = MagicMock(name="requirement")
        self.requirement.requirements = []
        self.test.last_action.defects = []
        self.store.load.side_effect = [
            self.part_model,
            self.control_plan,
            self.requirement,
        ]
        inspection.start_test(self.location, self.part_info, till_first_failure)
        return inspection


class FeatureResponsability(InspectionContext):
    def ucase_inspection_not_find_responsible(self):
        inspection = self.given_a_inspection_service()
        self.store.load.side_effect = [MagicMock(name="station"), None]

        with pytest.raises(insp.ResponsibleNotFound):
            inspection.take_responsability("station-code", "wrong-person-code")

        assert call(PersonId("wrong-person-code")) in self.store.load.mock_calls

    def ucase_station_not_found(self):
        inspection = self.given_a_inspection_service()
        self.store.load.side_effect = [None]

        with pytest.raises(insp.StationNotFound):
            inspection.take_responsability("station-code", "responsible-code")

        assert call(StationId("station-code")) in self.store.load.mock_calls

    def ucase_station_load_its_equipments(self):
        inspection = self.given_a_inspection_service()

        inspection.take_responsability("station-code", "responsible-code")

        self.station.load_equipments.assert_called_with(
            self.store, self.create_equipment
        )

    def ucase_inspection_has_responsible(self):
        inspection = self.given_a_inspection_service()
        responsible = MagicMock(name="person")
        responsible.username = "sruiz"
        responsible.surname = "Ruiz"
        responsible.firstname = "Salvador"
        self.store.load.side_effect = [MagicMock(name="station"), responsible]

        inspection.take_responsability("station-code", "person-code")

        assert (
            events.ResponsabilityIsTakenEvent(
                "station-code",
                values.PersonDataTransfer(responsible.code, "sruiz", "Ruiz, Salvador"),
            )
            == self.observer.events[-1]
        )

    def ucase_inspection_loose_responsible(self):
        inspection = self.given_a_inspection_service()

        inspection.take_responsability("station-code", None)

        assert (
            events.ResponsabilityIsLeftEvent("station-code") == self.observer.events[-1]
        )


class FeatureTestPreparation(InspectionContext):
    def ucase_no_start_test_without_responsible(self):
        inspection = self.given_a_inspection_service()
        inspection.take_responsability("station-code", None)

        with pytest.raises(insp.ResponsibleIsAbsent):
            inspection.start_test(
                "station-code_1", values.PartInfo("part-number", "serial-number")
            )

    def ucase_no_start_test_without_station(self):
        inspection = self.given_a_inspection_service()

        with pytest.raises(insp.LocationNotAsignable):
            inspection.start_test(
                "station-code_1", values.PartInfo("part-number", "serial-number")
            )

    def ucase_no_start_if_wrong_part_number(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [None]

        with pytest.raises(insp.PartModelNotFound):
            inspection.start_test(
                "station-code_1", values.PartInfo("part-number", "serial-number")
            )
        self.store.load.assert_called_with(PartModelId("part-number"))

    def ucase_no_start_if_no_control_plan(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model]
        self.control_plan_finder.return_value = None

        with pytest.raises(insp.ControlPlanNotFound):
            inspection.start_test(
                "station-code_1", values.PartInfo("part-number", "serial-number")
            )

    def ucase_control_plan_with_part_numbers(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        inspection.start_test(
            "station-code_1", values.PartInfo("part-number", "serial-number")
        )

        assert (
            call(
                location=self.station.origin,
                affected_part_key=self.part_model.part_number,
            )
            in self.control_plan_finder.mock_calls
        )

    def ucase_control_plan_found_with_part_groups(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        self.control_plan_finder.side_effect = [None, 1]
        self.part_model.groups = ["part-group"]

        inspection.start_test(
            "station-code_1", values.PartInfo("part-number", "serial-number")
        )

        assert (
            call(location=self.station.origin, affected_part_key="part-group")
            in self.control_plan_finder.mock_calls
        )

    def ucase_no_start_if_responsible_is_not_authorized(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        self.control_plan_finder.return_value = self.control_plan.id
        self.control_plan.is_authorized.return_value = False

        with pytest.raises(insp.ResponsibleNotAuthorized):
            inspection.start_test(
                "station-code_1", values.PartInfo("part-number", "serial-number")
            )

        assert call(self.control_plan.id) in self.store.load.mock_calls
        self.control_plan.is_authorized.assert_called_with(self.person)

    def ucase_start_with_part(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        part_info = values.PartInfo("part-number", "serial-number")
        toolbox = {eq.name: eq for eq in self.station.equipments}

        with patch("inddom.quality.inspection.tests") as tests:
            test = tests.Test.return_value
            inspection.start_test("station-code_1", part_info)
            test.prepare.assert_called_with(
                "station-code_1", self.person, {"toolbox": toolbox}
            )
            test.take_part.assert_called_with(self.station.collect_part.return_value)
            self.station.collect_part.assert_called_with(part_info, self.store)

    def ucase_start_with_custom_check_attributes(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        part_info = values.PartInfo("part-number", "serial-number")
        toolbox = {eq.name: eq for eq in self.station.equipments}

        check_attrs = {"par1": 1}

        with patch("inddom.quality.inspection.tests") as tests:
            test = tests.Test.return_value
            inspection.start_test(
                "station-code_1",
                part_info,
                till_first_failure=False,
                check_attrs=check_attrs,
            )
            test.prepare.assert_called_with(
                "station-code_1", self.person, {"toolbox": toolbox, "par1": 1}
            )

    def ucase_start_test_notify_test_starting_event(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        part_info = values.PartInfo("part-number", "serial-number")

        with patch("inddom.quality.inspection.tests") as tests:
            inspection.start_test("station-code_1", part_info)

        assert (
            events.TestHasStartedEvent(
                "station-code_1",
                part_info,
                values.PartModelDataTransfer(
                    self.part_model.part_number,
                    self.part_model.name,
                    self.part_model.description,
                ),
            )
            == self.observer.events[-1]
        )

    def should_move_part_to_location(self):
        inspection = self.given_a_inspection_service_with_responsible()
        self.store.load.side_effect = [self.part_model, self.control_plan]
        part_info = values.PartInfo("part-number", "serial-number")

        with patch("inddom.quality.inspection.tests") as tests:
            inspection.start_test("station-code_1", part_info)

        self.part.move_to.assert_called_with(self.location_code)


class FeatureFollowigCheckList(InspectionContext):
    def ucase_following_unique_control(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})]
        )

        inspection.walk("station-code_1")
        with pytest.raises(StopIteration):
            inspection.walk("station-code_1")

    def ucase_notify_event_of_start_check(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})]
        )

        inspection.walk("station-code_1")

        expected_event = events.CheckHasStartedEvent(
            "station-code_1",
            values.RequirementDataTransfer(
                self.requirement.path,
                self.requirement.characteristic.as_plain_dict(),
                self.requirement.eid,
                self.requirement.specs.as_plain_dict(),
                self.requirement.requirements,
            ),
        )
        assert expected_event in self.observer.events

    def ucase_notify_ok_check_finish_event(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})]
        )
        self.test.last_action.defects = []
        self.test.last_action.measurements = []

        inspection.walk("station-code_1")

        expected_event = events.CheckHasFinishedEvent(
            self.location_code, "ok", [], [], None
        )
        assert expected_event in self.observer.events

    def ucase_notify_nok_check_finish_event(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})]
        )
        self.test.last_action.defects = ["a defect"]
        self.test.last_action.measurements = []

        with pytest.raises(StopIteration):
            inspection.walk("station-code_1")

        expected_event = events.CheckHasFinishedEvent(
            self.location_code, "nok", [], ["a defect"], None
        )
        assert expected_event in self.observer.events

    def ucase_notify_nok_check_finish_event_continues_with_test(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})],
            till_first_failure=False,
        )
        self.test.last_action.defects = ["a defect"]
        self.test.last_action.measurements = []

        inspection.walk("station-code_1")

        expected_event = events.CheckHasFinishedEvent(
            self.location_code, "nok", [], ["a defect"], None
        )
        assert expected_event in self.observer.events

    def ucase_notify_cancelled_check_finish_event(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {})]
        )
        self.test.last_action.defects = ["a defect"]
        self.test.last_action.measurements = []
        self.test.run_check.side_effect = ex = Exception("something failed!!")

        with pytest.raises(Exception):
            inspection.walk("station-code_1")

        expected_event = events.CheckHasFinishedEvent(
            self.location_code, "cancelled", [], ["a defect"], ex
        )
        assert expected_event == self.observer.events[-1]

    def ucase_notify_event_of_start_action(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Step("module:method_name", {})]
        )

        inspection.walk("station-code_1")

        expected_event = events.ActionHasStartedEvent(
            "station-code_1", ["method", "name"]
        )
        assert expected_event in self.observer.events

    def ucase_notify_action_has_finished(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Step("module:method_name", {})]
        )

        inspection.walk("station-code_1")

        expected_event = events.ActionHasFinishedEvent(self.location_code, "done")
        assert expected_event in self.observer.events

    def ucase_notify_action_has_finished_with_exception(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Step("module:method_name", {})]
        )

        self.test.run_action.side_effect = ex = Exception("something failed!!")

        with pytest.raises(Exception):
            inspection.walk("station-code_1")

        expected_event = events.ActionHasFinishedEvent(
            self.location_code, "cancelled", ex
        )
        assert expected_event in self.observer.events

    def ucase_test_has_proper_run_check_call(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Control(RequirementId("path", "char", "eid"), "check_method", {"par1": 1})]
        )

        inspection.walk("station-code_1")

        self.test.run_check.assert_called_with(
            self.methods.__getitem__.return_value, self.requirement, {"par1": 1}
        )

    def ucase_test_has_proper_run_action_call(self):
        inspection = self.given_a_inspection_service_prepared_for(
            [Step("module:method_name", {"par1": 1})]
        )

        inspection.walk("station-code_1")

        self.test.run_action.assert_called_with(
            self.methods.__getitem__.return_value, {"par1": 1}
        )

    def _ucase_check_waits_if_(self):
        ...


class FeatureTestClosing(InspectionContext):
    def should_report_resolution_at_finish_of_test(self):
        inspection = self.given_a_inspection_service_prepared_for()

        self.test.resolution = values.TestResolution.SUCCESS

        assert self.test.resolution == inspection.close_test(self.location_code)
        assert events.TestHasFinishedEvent(
            self.location_code, values.TestResolution.SUCCESS
        )

    def should_persist_test(self):
        inspection = self.given_a_inspection_service_prepared_for()

        inspection.close_test(self.location_code)

        assert call(self.test) in self.store.save.mock_calls
        assert call(self.part) in self.store.save.mock_calls

    def should_move_part_to_origin_if_failed(self):
        inspection = self.given_a_inspection_service_prepared_for()

        self.test.resolution = values.TestResolution.FAILED

        inspection.close_test(self.location_code)
        self.part.move_to.assert_called_with(self.station.origin)

    def should_move_part_to_origin_if_cancelled(self):
        inspection = self.given_a_inspection_service_prepared_for()

        self.test.resolution = values.TestResolution.CANCELLED

        inspection.close_test(self.location_code)
        self.part.move_to.assert_called_with(self.station.origin)

    def should_move_part_to_control_plan_destination_if_success(self):
        inspection = self.given_a_inspection_service_prepared_for()

        self.test.resolution = values.TestResolution.SUCCESS

        inspection.close_test(self.location_code)
        self.test.finish.assert_called_with()

        self.part.move_to.assert_called_with(self.control_plan.to_location_code)

    def should_cancel_test_externally(self):
        inspection = self.given_a_inspection_service_prepared_for()

        inspection.cancel_test(self.location_code)

        self.test.cancel.assert_called_with(
            f"External cancel from {self.location_code}"
        )
        self.test.finish.asssert_called_with()


def eval_with_wait(check, requirement):
    check.status = "first"
    yield 1
    check.status = "second"


class FeatureTestWaiting(InspectionContext):
    def ucase_run_check_answer_a_request(self):
        requirement = MagicMock(specs=Requirement)
        service = self.given_a_inspection_service_prepared_for(
            [Control(requirement, "eval_with_wait")]
        )
        self.test.run_check.return_value = response = MagicMock()

        assert service.walk(self.location_code) == response

    def ucase_walk_not_allowed_if_request(self):
        requirement = MagicMock(specs=Requirement)
        service = self.given_a_inspection_service_prepared_for(
            [Control(requirement, "eval_with_wait")]
        )
        self.test.run_check.return_value = response = MagicMock()
        service.walk(self.location_code)

        with pytest.raises(insp.WalkingNotAllowed):
            service.walk(self.location_code)

    def ucase_answer_to_inspection(self):
        requirement = MagicMock(specs=Requirement)
        service = self.given_a_inspection_service_prepared_for(
            [Control(requirement, "eval_with_wait")]
        )
        self.test.run_check.return_value = MagicMock()
        request = service.walk(self.location_code)
        response = values.Response("answered", request)
        service.answer(self.location_code, response)

        self.test.answer.assert_called_with(response)

        with pytest.raises(StopIteration):
            service.walk(self.location_code)

    def ucase_events_are_triguered(self):
        requirement = MagicMock(specs=Requirement)
        service = self.given_a_inspection_service_prepared_for(
            [Control(requirement, "eval_with_wait")]
        )
        self.test.run_check.return_value = request = values.Request("I need help")
        assert request == service.walk(self.location_code)

        expected = events.CheckIsWaitingEvent("station-code_1", request)
        assert expected in self.observer.events

        response = values.Response("answered", request)
        service.answer(self.location_code, response)

        check = self.test.last_action
        expected = events.CheckHasFinishedEvent(
            "station-code_1", check.result, check.measurements, check.defects
        )
        assert expected in self.observer.events
