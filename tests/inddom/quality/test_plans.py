from unittest.mock import MagicMock, patch

from inddom.quality.plans import Control, ControlPlan, ControlPlanId, MethodPars, Step
from tests.inddom import EventObserver, reload_aggregate


class ControlPlanContext:
    def given_a_control_plan(self):
        control_plan = ControlPlan()
        self.observer = EventObserver()

        def catch_history(add_to_history):
            def func(event):
                self.observer.record_event(event)
                add_to_history(event)

            return func

        add_to_history = control_plan.event_handler.add_to_history
        control_plan.event_handler.add_to_history = catch_history(add_to_history)
        return control_plan

    def given_a_created_control_plan(self):
        control_plan = self.given_a_control_plan()
        self.from_location = "from"
        self.to_location = "to"
        control_plan.create(self.from_location, self.to_location)
        return control_plan

    def given_a_control_plan_with_steps(self, *steps):
        self.responsible = MagicMock()
        control_plan = self.given_a_created_control_plan()
        with patch("inddom.quality.plans.datetime") as mock_datetime:
            mock_datetime.datetime.now.return_value = "now"
            for step in steps:
                if type(step) is Step:
                    control_plan.add_step(
                        step.method_name, step.method_pars, self.responsible
                    )
                else:
                    control_plan.add_step(
                        step.method_name,
                        step.method_pars,
                        self.responsible,
                        self.requirement,
                    )

        return control_plan

    def given_a_sequence_of_steps(self):
        self.requirement = MagicMock()
        return [
            Control(self.requirement.id, "method1", MethodPars()),
            Control(self.requirement.id, "method2", MethodPars()),
            Step("method3", MethodPars()),
            Step("method4", MethodPars()),
            Control(self.requirement.id, "method5", MethodPars()),
        ]


class A_ControlPlan_as_planing_record(ControlPlanContext):
    def should_be_created_from_locations(self):
        control_plan = self.given_a_control_plan()

        control_plan.create("from", "to")

        assert (
            control_plan.Created(
                control_plan_id=control_plan.id,
                from_location_code="from",
                to_location_code="to",
            )
            == self.observer.events[-1]
        )
        assert control_plan.from_location_code == "from"
        assert control_plan.to_location_code == "to"

    def should_collect_a_sequence_of_controls(self):
        control_plan = self.given_a_created_control_plan()
        requirement = MagicMock(name="requirement")
        responsible = MagicMock(name="responsible")

        control = Control(requirement.id, "module:method_name", MethodPars(par1=1))
        with patch("inddom.quality.plans.datetime") as mock_datetime:
            control_plan.add_step(
                "module:method_name", MethodPars(par1=1), responsible, requirement
            )

            expected = control_plan.StepAdded(
                issued_on=mock_datetime.datetime.now.return_value,
                requirement_id=requirement.id,
                method_pars=MethodPars(par1=1),
                method_name="module:method_name",
                responsible_id=responsible.id,
            )
            assert expected == self.observer.events[-1]

        assert control == control_plan.steps[-1]

    def should_collect_a_sequence_of_steps(self):
        control_plan = self.given_a_created_control_plan()
        responsible = MagicMock(name="responsible")

        step = Step("module:method_name", MethodPars(par1=1))
        with patch("inddom.quality.plans.datetime") as mock_datetime:
            control_plan.add_step("module:method_name", MethodPars(par1=1), responsible)

            expected = control_plan.StepAdded(
                issued_on=mock_datetime.datetime.now.return_value,
                method_pars=MethodPars(par1=1),
                method_name="module:method_name",
                responsible_id=responsible.id,
                requirement_id=None,
            )
            assert expected == self.observer.events[-1]

        assert step == control_plan.steps[-1]

    def ucase_control_plan_is_autorized_for_roles(self):
        control_plan = self.given_a_created_control_plan()
        person = MagicMock()
        person.roles = ["role"]

        control_plan.authorize_to("role", "other_role")

        assert control_plan.is_authorized(person)

        other_person = MagicMock()
        other_person.roles = ["other"]
        assert not control_plan.is_authorized(other_person)

        other_person.roles = ["other_role"]
        assert control_plan.is_authorized(other_person)

    def should_affect_to_part_numbers(self):
        control_plan = self.given_a_created_control_plan()
        affected = "affected"

        control_plan.affects_to(affected)

        part = MagicMock()
        part.groups = []
        part.part_number = "affected"

        assert control_plan.is_affected(part)
        assert not control_plan.is_affected(MagicMock())

    def should_affect_to_part_groups(self):
        control_plan = self.given_a_created_control_plan()
        affected = "group"

        control_plan.affects_to(affected)

        part = MagicMock()
        part.groups = ["group"]

        assert control_plan.is_affected(part)


class A_ControlPlan_as_editable_planning(ControlPlanContext):
    def should_new_step_is_inserted_on_control_plan(self):
        steps = self.given_a_sequence_of_steps()
        control_plan = self.given_a_control_plan_with_steps(*steps)

        step = Step("inserted:name", MethodPars(par1="par1"))
        control_plan.insert_step(step, 2, self.responsible)

        assert control_plan.steps[2] == step
        assert len(control_plan.steps) == len(steps) + 1
        assert (
            control_plan.StepInserted(
                step=step, position=2, responsible_id=self.responsible.id
            )
            == self.observer.events[-1]
        )

    def ucase_new_step_is_replaced_on_control_plan(self):
        steps = self.given_a_sequence_of_steps()
        control_plan = self.given_a_control_plan_with_steps(*steps)

        step = Step("inserted:name", MethodPars(par1="par1"))
        control_plan.replace_step(step, 3, self.responsible)

        assert control_plan.steps[3] == step
        assert len(control_plan.steps) == len(steps)
        assert (
            control_plan.StepReplaced(
                step=step, position=3, responsible_id=self.responsible.id
            )
            == self.observer.events[-1]
        )

    def ucase_step_is_removed_from_control_plan(self):
        steps = self.given_a_sequence_of_steps()
        control_plan = self.given_a_control_plan_with_steps(*steps)

        control_plan.remove_step(3, self.responsible)

        assert len(control_plan.steps) == len(steps) - 1
        assert control_plan.steps[3] == steps[4]
        assert (
            control_plan.StepRemoved(position=3, responsible_id=self.responsible.id)
            == self.observer.events[-1]
        )


class A_ControPlan_as_aggregate(ControlPlanContext):
    def should_be_reloaded(self):
        control_plan = self.given_a_control_plan_with_steps(
            *self.given_a_sequence_of_steps()
        )
        control_plan.authorize_to("role")
        control_plan.affects_to("part-number")

        reloaded = ControlPlan()
        reload_aggregate(control_plan, reloaded)

        assert reloaded.from_location_code == control_plan.from_location_code
        assert reloaded.to_location_code == control_plan.to_location_code
        assert reloaded.steps == control_plan.steps

        assert reloaded.is_authorized(MagicMock(roles=["role"]))
        assert reloaded.is_affected(MagicMock(part_number="part-number"))
