# from inddom.quality.models.projections import ControlPlans
from inddom.eventsourcing import EventBroker, EventStore
from inddom.hhrr.models.persons import Person
from inddom.infrastructures.onmem.eventsourcing import OnMemBroker, OnMemEventData
from inddom.product.models.definitions import Characteristic, Requirement
from inddom.quality.models.planning import Control, ControlPlan, MethodPars, Step


class FeatureControlPlanIsPersisted:
    def ucase_control_plan_persists_with_onmem_event_data(self):
        event_data = OnMemEventData()
        broker = OnMemBroker()
        event_broker = EventBroker(broker, event_data)
        event_store = EventStore(event_data, event_broker)

        responsible = Person()
        responsible.hire("438", "sruiz", "Ruiz Romero, Salvador")
        event_store.save(responsible)

        requirement = Requirement()
        requirement.define("path", Characteristic("color", "element"), "eid")

        event_store.save(requirement)

        control_plan = ControlPlan()
        control_plan.create("fromLocationCode", "toLocationCode")
        control_plan.add_role("role")
        control_plan.add_step(
            "methodName", MethodPars(par1=1), responsible, requirement
        )
        control_plan.add_step("stepName", MethodPars(par2=2), responsible)
        event_store.save(control_plan)

        new_control_plan = event_store.load(control_plan.id)

        control = Control(requirement.id, "methodName", MethodPars(par1=1))
        step = Step("stepName", MethodPars(par2=2))
        assert new_control_plan.from_location_code == "fromLocationCode"
        assert new_control_plan.to_location_code == "toLocationCode"
        assert "role" in new_control_plan.roles
        assert control in new_control_plan.controls
        assert step in new_control_plan.steps
        assert control_plan.id == new_control_plan.id


class FeatureControlPlanIsProjected:
    def _ucase_control_plan_is_projected_when_persisted(self):
        pass
