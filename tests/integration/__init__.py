"""Integration tests are for assuring that environment very close to production
is valid. Run in a stage enviroment these tests.

In a hexagonal architecture we need to check that all external port adapters (inputs and outputs)
works in stage environment.

Assure no errors of configuration and correct function in port use cases."""
