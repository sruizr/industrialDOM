#!/usr/bin/env sh

poetry run pytest  \
    tests/inddom/infra \
    tests/inddom/manufacturing \
    tests/inddom/quality
