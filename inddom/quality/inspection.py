import datetime
import threading
from dataclasses import dataclass
from types import GeneratorType
from typing import Any, Callable, Iterable

import inddom.product.definitions as defs
import inddom.quality.tests as tests
from inddom import values as val
from inddom.hhrr.persons import Person
from inddom.logistics.parts import Part, PartId
from inddom.manufacturing.stations import Station, StationId
from inddom.quality import events as ev
from inddom.quality import ports
from inddom.quality.plans import Control, ControlPlan, Step


@dataclass
class _LocationScope:
    station_code: str
    test: tests.Test | None = None
    part_model: defs.PartModel | None = None
    part: Part | None = None
    checklist: Iterable | None = None
    control_plan: ControlPlan | None = None
    till_first_failure: bool = True
    request: val.Request | None = None


@dataclass
class _StationSession:
    station: Station
    responsible: Person | None = None


class StationNotFound(Exception):
    def __init__(self, station_code):
        super().__init__(f'Station with code "{station_code}" not found in domain.')


class WalkingNotAllowed(Exception):
    ...


class ResponsibleNotFound(Exception):
    def __init__(self, person_code):
        super().__init__(f'Not found responsible with code "{person_code}"')


class ResponsibleIsAbsent(Exception):
    def __init__(self):
        super().__init__("No responsibility is taken in ate")


class PartModelNotFound(Exception):
    def __init__(self, part_number):
        super().__init__(f'Not found part model for partnumber "{part_number}"')


class LocationNotAsignable(Exception):
    def __init__(self, location):
        super().__init__(f'Not possible to asign location "{location}" to any station')


class TestNotPrepared(Exception):
    def __init__(self, location):
        super().__init__(f'Test at location "{location}" is not prepared')


class ControlPlanNotFound(Exception):
    def __init__(self, part_model, location):
        super().__init__(
            f'Not found control plan for part number "{part_model.part_number}" at "{location}"'
        )


class ResponsibleNotAuthorized(Exception):
    """Raise when responsible's roles are not compatible with control plan."""

    def __init__(self, responsible, part_model):
        """Raise message of responsilbe and part model"""
        super().__init__(
            f'Responsible "{responsible.firstname} {responsible.surname}"'
            " is not authorized to run inspection for "
            f'"{part_model.part_number}"'
        )


class MethodNotFound(Exception):
    """Raise when method is not found in method catalog."""

    def __init__(self, method_name):
        self._method_name = method_name
        super().__init__(
            f'Method with name "{method_name}" not found in current catalog.'
        )


class InspectionService:
    """Inspection service to inspect parts following control plans."""

    def __init__(
        self,
        methods: ports.Methods,
        store: ports.Store,
        control_plan_finder: ports.Query,
        create_equipment: Callable,
    ):

        self._sessions: dict[str, _StationSession] = {}
        self._locations: dict[str, _LocationScope] = {}
        self._lock = threading.Lock()

        self._methods = methods
        self._store = store
        self._cp_finder = control_plan_finder
        self._create_equipment = create_equipment

        self._observers = []
        self._clients = {}
        self._till_first_failure = True

        self._part_models = {}
        self._requirements = {}
        self._control_plans = {}

    def _load_station(self, station_code: str):
        station: Station = self._store.load(StationId(station_code))
        if not station:
            raise StationNotFound(station_code)

        station.load_equipments(self._store, self._create_equipment)
        self._toolbox = {equipment.name: equipment for equipment in station.equipments}
        return station

    @property
    def toolbox(self) -> dict:
        return self._toolbox.copy()

    def take_responsability(self, station_code: str, person_code: str | None) -> None:
        if station_code not in self._sessions:
            self._sessions[station_code] = _StationSession(
                self._load_station(station_code)
            )

        if person_code is None:
            self._sessions[station_code].responsible = None
            self._notify_event(ev.ResponsabilityIsLeftEvent(station_code))
            return

        responsible: Person = self._store.load(val.PersonId(person_code))
        if not responsible:
            raise ResponsibleNotFound(person_code)

        self._sessions[station_code].responsible = responsible
        self._notify_event(
            ev.ResponsabilityIsTakenEvent(
                station_code,
                val.PersonDataTransfer(
                    responsible.code,
                    responsible.username,
                    f"{responsible.surname}, {responsible.firstname}",
                ),
            )
        )

    def _load_location_scope(self, location: str):
        for station_code in self._sessions.keys():
            if location[: len(station_code)] == station_code:
                return _LocationScope(station_code)

        raise LocationNotAsignable(location)

    def _get_part_model(self, part_number: str):
        if part_number not in self._part_models:
            part_model = self._store.load(defs.PartModelId(part_number))
            if not part_model:
                raise PartModelNotFound(part_number)
            self._part_models[part_number] = part_model

        return self._part_models[part_number]

    def _get_control_plan(self, origin: str, part_model: defs.PartModel):
        if (origin, part_model) not in self._control_plans:
            control_plan_id = self._cp_finder(
                location=origin, affected_part_key=part_model.part_number
            )
            if not control_plan_id:
                for group in part_model.groups:
                    control_plan_id = self._cp_finder(
                        location=origin, affected_part_key=group
                    )
                    if control_plan_id:
                        break

            if not control_plan_id:
                raise ControlPlanNotFound(part_model, origin)

            self._control_plans[(origin, part_model)] = self._store.load(
                control_plan_id
            )

        return self._control_plans[(origin, part_model)]

    def _get_requirement(self, requirement_id):
        if requirement_id not in self._requirements:
            requi = self._store.load(requirement_id)
            requi.load_subrequirements(self._store)
            self._requirements[requirement_id] = requi

        return self._requirements[requirement_id]

    def start_test(
        self,
        location: str,
        part_info: val.PartInfo,
        till_first_failure: bool = True,
        check_attrs: dict[str, Any] | None = None,
    ) -> None:

        if location not in self._locations:
            self._locations[location] = self._load_location_scope(location)

        scope = self._locations[location]
        station_session = self._sessions[scope.station_code]
        if not station_session.responsible:
            raise ResponsibleIsAbsent()

        scope.part_model = part_model = self._get_part_model(part_info.part_number)
        scope.control_plan = control_plan = self._get_control_plan(
            station_session.station.origin, part_model
        )
        if not control_plan.is_authorized(station_session.responsible):
            raise ResponsibleNotAuthorized(station_session.responsible, part_model)

        scope.checklist = iter(control_plan.steps)

        scope.till_first_failure = till_first_failure
        scope.test = test = tests.Test()
        if not check_attrs:
            check_attrs = {}

        check_attrs.update({"toolbox": self._toolbox})
        test.prepare(location, station_session.responsible, check_attrs)
        scope.part = station_session.station.collect_part(part_info, self._store)
        scope.part.move_to(location)
        test.take_part(scope.part)

        self._notify_event(
            ev.TestHasStartedEvent(
                location,
                part_info,
                val.PartModelDataTransfer.from_part_model(part_model),
            )
        )

    def walk(self, location: str) -> val.OperationResult | val.Request:
        scope = self._locations[location]
        if scope.request:
            raise WalkingNotAllowed()

        if not scope.checklist:
            raise TestNotPrepared(location)

        step = next(scope.checklist)
        test: tests.Test = scope.test
        method = self._methods[step.method_name]
        if type(step) is Control:
            requirement = self._get_requirement(step.requirement_id)
            self._notify_event(
                ev.CheckHasStartedEvent(
                    location, val.RequirementDataTransfer.from_requirement(requirement)
                )
            )
            try:
                result = test.run_check(method, requirement, step.method_pars)
                if type(result) is val.Request:
                    scope.request = request = result
                    self._notify_event(ev.CheckIsWaitingEvent(location, request))
                    return request

                result = "ok"
                if test.last_action.defects:
                    result = "nok"
            except Exception as e:
                result = "cancelled"
                self._notify_event(
                    ev.CheckHasFinishedEvent(
                        location,
                        result,
                        test.last_action.measurements,
                        test.last_action.defects,
                        e,
                    )
                )
                raise e

            self._notify_event(
                ev.CheckHasFinishedEvent(
                    location,
                    result,
                    test.last_action.measurements,
                    test.last_action.defects,
                    None,
                )
            )
            if result == "nok" and scope.till_first_failure:
                raise StopIteration()

        elif type(step) is Step:
            description = step.method_name.split(":")[-1].split("_")
            self._notify_event(ev.ActionHasStartedEvent(location, description))
            try:
                test.run_action(method, step.method_pars)
            except Exception as e:
                result = "cancelled"
                self._notify_event(ev.ActionHasFinishedEvent(location, "cancelled", e))
                raise e

            self._notify_event(ev.ActionHasFinishedEvent(location, "done"))

    def close_test(self, location: str) -> str:
        """Return test results and close all."""
        scope = self._locations[location]
        scope.test.finish()

        destination = scope.control_plan.to_location_code
        if scope.test.resolution != 'success':
            station = self._sessions[scope.station_code].station
            destination = station.origin

        scope.part.move_to(destination)

        self._store.save(scope.test)
        self._store.save(scope.part)

        self._notify_event(ev.TestHasFinishedEvent(location, scope.test.resolution))

        return scope.test.resolution

    def cancel_test(self, location: str) -> None:
        """Cancel test and persist it."""
        scope = self._locations[location]
        scope.test.cancel(f"External cancel from {location}")
        self.close_test(location)

    def answer(self, location: str, response: val.Response):
        """Answer a test waiting for a result."""
        scope = self._locations[location]
        result = scope.test.answer(response)
        result = scope.test.last_action.result
        scope.request = None
        self._notify_event(
            ev.CheckHasFinishedEvent(
                location,
                result,
                scope.test.last_action.measurements,
                scope.test.last_action.defects,
            )
        )

    def register_event_observer(self, notify_func: Callable):
        """Register observer function to notify a ew event."""
        self._observers.append(notify_func)

    def _notify_event(self, event):
        for func in self._observers:
            func(event)
