import abc
from typing import Any, Callable

from inddom import values as val
from inddom.quality import ports

from ..ddd import Entity, _Id


class Store:
    @abc.abstractmethod
    def load(self, id: _Id) -> Any:
        ...

    @abc.abstractmethod
    def save(self, entity):
        ...


class Equipment(abc.ABC):
    @abc.abstractproperty
    def tracking(self) -> str:
        ...

    @abc.abstractproperty
    def name(self) -> str:
        ...


class ATE(Equipment):
    pass


class Query(abc.ABC):
    @abc.abstractmethod
    def __call__(self, **kwargs):
        ...


class Methods(abc.ABC):
    @abc.abstractmethod
    def __getitem__(self, key: str):
        ...

    @abc.abstractmethod
    def get(self, key: str, default: Callable | None = None):
        ...
