import logging
import time
from types import GeneratorType
from typing import Any

from inddom.ddd import Aggregate, Attribute, Entity, Event, RandomId, Value
from inddom.hhrr.persons import NotAuthorizedPerson, Person
from inddom.values import Request, Response, TestResolution

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class WrongPartForTesting(Exception):
    pass


class TestId(RandomId):
    pass


class Test(Aggregate):
    part = Attribute()
    client = Attribute()
    control_plan = Attribute()
    resolution = Attribute()
    responsible = Attribute()

    class Prepared(Event):
        def apply_on(self, test):
            test._id = self.test_id
            test.responsible_id = self.responsible_id
            test.location_code = self.location_code

    class PartIsTaken(Event):
        def apply_on(self, test):
            test.part_id = self.part_id

    class ActionIsStarted(Event):
        def apply_on(self, test):
            action = Action(test)
            test._actions.append(action)

    class ActionHasFinished(Event):
        def apply_on(self, test):
            ...

    class ActionIsCancelled(Event):
        def apply_on(self, test):
            test._resolution = "cancelled"

    class CheckIsStarted(Event):
        def apply_on(self, test):
            ...

    class CheckIsOK(Event):
        def apply_on(self, test):
            ...

    class CheckIsNotOK(Event):
        def apply_on(self, test):
            test._resolution = "failed"

    class CheckIsWaiting(Event):
        def apply_on(self, test):
            ...

    class CheckContinues(Event):
        def apply_on(self, test):
            ...

    class CheckIsCancelled(Event):
        def apply_on(self, test):
            test._resolution = "cancelled"
            ...

    class MeasurementIsAdded(Event):
        def apply_on(self, test):
            ...

    class DefectIsAdded(Event):
        def apply_on(self, test):
            test._resolution = "failed"

    class Success(Event):
        def apply_on(self, test):
            ...

    class Cancelled(Event):
        def apply_on(self, test):
            ...

    class Failed(Event):
        def apply_on(self, test):
            ...

    def __init__(self):
        self._responsible = None
        self._control_plan = None
        self._part = None
        self._client = None
        self._checklist = None
        self._actions = []
        self._resolution = None
        self._check_attrs = None
        self._tff = True

        super().__init__()

    @property
    def actions(self):
        return self._actions.copy()

    def prepare(
        self,
        location: str,
        responsible: Person,
        check_attrs: dict[str, Any] | None = None,
    ):
        """Prepare test with responsible and client."""
        self._responsible = responsible
        self._check_attrs = check_attrs

        self._trigger_event(
            self.Prepared(
                responsible_id=responsible.id,
                location_code=location,
                test_id=TestId(),
            )
        )

    def take_part(self, part):
        """Take part to be tested."""
        self._part = part
        self._trigger_event(self.PartIsTaken(part_id=part.id))

    @property
    def last_action(self):
        return self._actions[-1]

    def run_check(self, method, requirement, method_pars=None):
        check = Check(self, requirement)

        method_name = "{}:{}".format(method.__module__, method.__name__)
        self._trigger_event(
            self.CheckIsStarted(requirement_id=requirement.id, method_name=method_name)
        )
        self._actions.append(check)
        self._inject_attrs(check)
        request = check.run(method, method_pars)
        if request:
            return request
        return check.result

    def run_action(self, method, method_pars=None):
        action = Action(self)
        method_name = "{}:{}".format(method.__module__, method.__name__)
        self._trigger_event(self.ActionIsStarted(method_name=method_name))
        action = self._actions[-1]
        self._inject_attrs(action)
        action.run(method, method_pars)
        return action.result

    def answer(self, response: Response) -> Request | None:
        return self.last_action.continue_(response)

    def _inject_attrs(self, action):
        if not self._check_attrs:
            return

        for name, value in self._check_attrs.items():
            setattr(action, name, value)

    def cancel(self, message=None):
        self._resolution = "cancelled"
        self._trigger_event(self.Cancelled(message=message))

    def finish(self):
        if self._resolution == "cancelled":
            return

        for action in self._actions:
            if type(action) is Check and action.result == "nok":
                self._resolution = "failed"
                self._trigger_event(self.Failed())
                return

        for action in self._actions:
            if action.result == "error":
                self._resolution = "cancelled"
                self._trigger_event(self.Cancelled())
                return

        self._resolution = "success"
        self._trigger_event(self.Success())


class Check(Entity):
    result = Attribute()
    requirement_id = Attribute()

    def __init__(self, test, requirement):
        self.test = test
        self._requirement = requirement
        self._result = None
        self._defects = []
        self._measurements = []
        self._generator = None

    def run(self, method, method_pars):
        self.part = self.test.part
        try:

            pars = method_pars.invariants if method_pars else {}
            result = method(self, self._requirement, **pars)
            if type(result) is GeneratorType:
                self._generator = result
                request_message = next(result)
                request = Request(str(request_message))
                self.test._trigger_event(self.test.CheckIsWaiting(request=request_message))
                return request

            self._set_result()
        except Exception as e:
            self._result = "error"
            self.test._trigger_event(
                self.test.CheckIsCancelled(
                    ex_name=e.__class__.__name__, ex_message=str(e)
                )
            )
            # self.test._trigger_event(self.test.Cancelled(message='Check had an exception'))
            logger.exception(e)
            raise e

    def _set_result(self):
            if self._defects:
                self._result = "nok"
                self.test._trigger_event(self.test.CheckIsNotOK())
                return
            self._result = "ok"
            self.test._trigger_event(self.test.CheckIsOK())

    def continue_(self, response):
        """Continue check if has yields."""
        if self._generator:
            self.response = response
            try:
                request = next(self._generator)
                if request:
                    self.test._trigger_event(self.test.CheckIsWaiting(request=request))
                    return request
            except StopIteration:
                self._generator = None
                self.test._trigger_event(self.test.CheckContinues())
                self._set_result()
            except Exception as e:
                self._result = "error"
                self.test._trigger_event(
                    self.test.CheckIsCancelled(
                        ex_name=e.__class__.__name__, ex_message=str(e)
                    )
                )
                raise e

    @property
    def defects(self):
        return self._defects.copy()

    @property
    def measurements(self):
        return self._measurements.copy()

    def add_defect(self, defect):
        """Add defect to test."""
        logger.info(f"Defect is added to check: {defect}")
        if defect.subject_id == self.test.part.id:
            self.test.part.add_defect(defect)

        self._defects.append(defect)
        self.test._trigger_event(self.test.DefectIsAdded(defect=defect))

    def add_part_defect(self, requirement, failure_mode, ms_tracking=None, index=None):
        """Add defect to testing part."""
        defect = Defect(
            requirement.id,
            failure_mode,
            self.test.part.id,
            self.test.id,
            ms_tracking=ms_tracking,
            index=index,
        )
        self.add_defect(defect)

    def add_part_measurement_and_eval(
        self, requirement, value, ms_tracking=None, index=None, uncertainty=0.0
    ):
        """Add a measurement to part and eval against a requirement."""
        measurement = Measurement(
            requirement.characteristic,
            requirement.eid,
            self.test.part.id,
            value,
            self.test.id,
            ms_tracking,
            index,
        )
        self.add_measurement(measurement)

        defect = measurement.evaluate(requirement, uncertainty)
        if defect:
            self.add_defect(defect)

    def add_measurement(self, measurement):
        """Add measurement to test."""
        logger.info(f"Measurement is added to check: {measurement}")
        self._measurements.append(measurement)
        self.test._trigger_event(self.test.MeasurementIsAdded(measurement=measurement))

    def clear_previous_requests(self):
        pass


class Measurement(Value):
    def __init__(
        self,
        characteristic,
        eid,
        subject_id,
        value,
        test_id,
        ms_tracking=None,
        index=None,
    ):
        super().__init__(
            characteristic=characteristic,
            eid=eid,
            subject_id=subject_id,
            value=value,
            test_id=test_id,
            ms_tracking=ms_tracking,
            index=index,
        )

    def evaluate(self, requirement, uncertainty=0):
        if hasattr(requirement.specs, "limits") or hasattr(
            requirement.specs, "tolerance"
        ):
            return self._evaluate_continuous(requirement, uncertainty=uncertainty)
        elif hasattr(requirement.specs, "value"):
            return self._evaluate_discrete(requirement)

        elif hasattr(requirement.specs, "values"):
            return self._evaluate_discretes(requirement)

        elif hasattr(requirement.specs, "max_abs"):
            return self._evaluate_max_abs(requirement, uncertainty=uncertainty)

        else:
            raise Exception("No compatible specs to evaluate!!!")

    def _evaluate_max_abs(self, requirement, uncertainty):
        max_abs = requirement.specs.max_abs
        if abs(self.value) > max_abs:
            return self._create_defect(requirement, "high")

        if abs(self.value) > max_abs - uncertainty:
            return self._create_defect(requirement, "suspicious high")

    def _evaluate_continuous(self, requirement, uncertainty):
        specs = requirement.specs.invariants
        if "tolerance" in specs:
            limits = [
                specs.get("nominal", 0) - specs["tolerance"],
                specs.get("nominal", 0) + specs["tolerance"],
            ]
        else:
            limits = specs["limits"]

        if limits[0] is not None and limits[1] is not None:
            if limits[0] > limits[1]:
                return self._evaluate_inverse(requirement, uncertainty)

        failure_mode = self._evaluate_interval(limits, uncertainty)
        if failure_mode:
            return self._create_defect(requirement, failure_mode)

    def _evaluate_inverse(self, requirement, uncertainty):
        limits = requirement.specs.limits
        limits = [limits[1], limits[0]]
        failure_mode = self._evaluate_interval(limits, uncertainty)

        if not failure_mode:
            return self._create_defect(requirement, "incorrect")

    def _evaluate_interval(self, limits, uncertainty):
        if limits[0] is not None:
            if self.value < limits[0]:
                return "low"
            if self.value < limits[0] + uncertainty:
                return "suspicious-low"
        if limits[1] is not None:
            if self.value > limits[1]:
                return "high"
            if self.value > limits[1] - uncertainty:
                return "suspicious-high"

    def _evaluate_discrete(self, requirement):
        if requirement.specs.value != self.value:
            return self._create_defect(requirement, "wrong")

    def _evaluate_discretes(self, requirement):
        if self.value not in requirement.specs.values:
            return self._create_defect(requirement, "wrong")

    def _create_defect(self, requirement, failure_mode):
        return Defect(
            requirement.id,
            failure_mode,
            self.subject_id,
            self.test_id,
            self.index,
            self.ms_tracking,
        )


class Defect(Value):
    def __init__(
        self,
        requirement_id,
        failure_mode,
        subject_id,
        test_id,
        index=None,
        ms_tracking=None,
    ):
        "docstring"
        super().__init__(
            requirement_id=requirement_id,
            failure_mode=failure_mode,
            subject_id=subject_id,
            test_id=test_id,
            index=index,
            ms_tracking=ms_tracking,
        )


class Action(Entity):
    result = Attribute()

    def __init__(self, operation):
        self.operation = operation
        self._result = None

    def run(self, method, method_pars):
        self.part = self.operation.part
        try:
            pars = method_pars.invariants if method_pars else {}
            method(self, **pars)
            self._result = "done"
            self.operation._trigger_event(self.operation.ActionHasFinished())
        except Exception as e:
            self._result = "error"
            self.operation._trigger_event(
                self.operation.ActionIsCancelled(
                    ex_name=e.__class__.__name__, ex_message=str(e)
                )
            )
            logger.exception(e)
            raise e
