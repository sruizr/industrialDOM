"""As a user, I want centralized all external domain events, so it is
easier to get them."""

import abc
from dataclasses import asdict, dataclass

from inddom import values as val


@dataclass(frozen=True)
class Event(abc.ABC):
    location: str

    def as_plain_dict(self):
        return asdict(self)


@dataclass(frozen=True)
class ResponsabilityIsTakenEvent(Event):
    person: val.PersonDataTransfer


@dataclass(frozen=True)
class ResponsabilityIsLeftEvent(Event):
    ...


@dataclass(frozen=True)
class ResponsibleNotFound:
    person_code: str


@dataclass(frozen=True)
class TestHasStartedEvent(Event):
    part_info: val.PartInfo
    part_model: val.PartModelDataTransfer


@dataclass(frozen=True)
class CheckHasStartedEvent(Event):
    requirement: val.RequirementDataTransfer


@dataclass(frozen=True)
class CheckIsWaitingEvent(Event):
    request: val.Request


@dataclass(frozen=True)
class CheckHasFinishedEvent(Event):
    result: str
    measurements: list
    defects: list
    exception: Exception | None = None

    def as_plain_dict(self):
        plain = super().as_plain_dict()
        defects = []
        for defect in self.defects:
            defects.append(defect.as_plain_dict())
        plain["defects"] = defects
        measurements = []
        for measurement in self.measurements:
            measurements.append(measurement.as_plain_dict())
        plain["measurements"] = measurements
        if self.exception:
            plain["exception"] = {"name": self.exception.__class__.__name__}
        return plain


@dataclass(frozen=True)
class ActionHasStartedEvent(Event):
    description: list


@dataclass(frozen=True)
class ActionHasFinishedEvent(Event):
    result: str
    exception: Exception | None = None

    def as_plain_dict(self):
        plain = super().as_plain_dict()
        if self.exception:
            plain["exception"] = {"name": self.exception.__class__.__name__}
        return plain


@dataclass(frozen=True)
class TestHasFinishedEvent(Event):
    result: str | val.TestResolution

    def as_plain_dict(self):
        plain = super().as_plain_dict()
        result = plain["result"]
        plain["result"] = result.value if type(result) is val.TestResolution else result
        return plain


@dataclass(frozen=True)
class ExceptionEvent(Event):
    ex_name: str
    ex_message: str
    tracking: str | None = None
