import datetime

from inddom.ddd import Aggregate, Attribute, Event, RandomId, Value


class ControlPlanId(RandomId):
    pass


class ControlPlan(Aggregate):
    from_location_code = Attribute()
    to_location_code = Attribute()

    class Created(Event):
        def apply_on(self, control_plan):
            control_plan._id = self.control_plan_id
            control_plan._from_location_code = self.from_location_code
            control_plan._to_location_code = self.to_location_code

    class StepAdded(Event):
        def apply_on(self, control_plan):
            if self.requirement_id:
                control_plan._steps.append(
                    Control(self.requirement_id, self.method_name, self.method_pars)
                )
            else:
                control_plan._steps.append(Step(self.method_name, self.method_pars))

    class StepInserted(Event):
        def apply_on(self, control_plan):
            control_plan._steps.insert(self.position, self.step)

    class StepReplaced(Event):
        def apply_on(self, control_plan):
            control_plan._steps.pop(self.position)
            control_plan._steps.insert(self.position, self.step)

    class StepRemoved(Event):
        def apply_on(self, control_plan):
            control_plan._steps.pop(self.position)

    class RoleAdded(Event):
        def apply_on(self, control_plan):
            control_plan._roles.append(self.role)

    class AffectedAdded(Event):
        def apply_on(self, control_plan):
            control_plan._affects_to.append(self.affected)

    def __init__(self):
        self._steps = []
        self._roles = []
        self._affects_to = []
        super().__init__()

    @property
    def controls(self):
        return [step for step in self._steps if type(step) is Control]

    @property
    def steps(self):
        return self._steps.copy()

    @property
    def roles(self):
        return self._roles.copy()

    def create(self, from_location_code, to_location_code):
        id = ControlPlanId()
        self._trigger_event(
            self.Created(
                from_location_code=from_location_code,
                to_location_code=to_location_code,
                control_plan_id=id,
            )
        )

    def add_step(self, method, method_pars, responsible, requirement=None):
        requirement_id = requirement.id if requirement else None

        self._trigger_event(
            self.StepAdded(
                requirement_id=requirement_id,
                method_name=method,
                method_pars=method_pars,
                responsible_id=responsible.id,
                issued_on=datetime.datetime.now(),
            )
        )

    def insert_step(self, step, position, responsible):
        self._trigger_event(
            self.StepInserted(
                step=step, position=position, responsible_id=responsible.id
            )
        )

    def replace_step(self, step, position, responsible):
        self._trigger_event(
            self.StepReplaced(
                step=step, position=position, responsible_id=responsible.id
            )
        )

    def remove_step(self, position, responsible):
        self._trigger_event(
            self.StepRemoved(position=position, responsible_id=responsible.id)
        )

    def authorize_to(self, *roles):
        for role in roles:
            self._trigger_event(self.RoleAdded(role=role))

    def is_authorized(self, person):
        return set(self._roles).intersection(set(person.roles)) != set()

    def affects_to(self, *affecteds):
        for affected in affecteds:
            self._trigger_event(self.AffectedAdded(affected=affected))

    def is_affected(self, part):
        affected = set(self._affects_to).intersection(set(part.groups)) != set()
        return part.part_number in self._affects_to or affected


class Control(Value):
    def __init__(
        self,
        requirement_id,
        method_name,
        method_pars=None,
        sampling=None,
        reaction=None,
    ):
        method_pars = method_pars if method_pars else {}
        super().__init__(
            requirement_id=requirement_id,
            method_name=method_name,
            method_pars=method_pars,
            sampling=sampling,
            reaction=reaction,
        )


class Step(Value):
    def __init__(self, method_name, method_pars):
        super().__init__(method_name=method_name, method_pars=method_pars)


class MethodPars(Value):
    def get(self, key, default):
        return getattr(self, key, default)
