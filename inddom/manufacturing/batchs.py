from inddom.ddd import Aggregate, Event, RandomId, Value


class OpFlow(Value):
    def __init__(self, income, outcome, from_location, to_location, fun=None):
        if not fun:
            fun = lambda part: None
        super().__init__(
            income=income,
            outcome=outcome,
            from_location=from_location,
            to_location=to_location,
            fun=fun,
        )


class BatchId(RandomId):
    pass


class Batch(Aggregate):
    class Started(Event):
        def apply_on(self, batch):
            batch._id = self.batch_id
            batch._location_code = self.location_code
            batch._responsible_code = self.responsible_code
            batch.batch_number = self.batch_number
            super().apply_on(batch)

    class Stopped(Event):
        def apply_on(self, batch):
            super().apply_on(batch)

    def __init__(self):
        super().__init__()
        self._can_manufacture = False

    def start(self, location_code, batch_number, responsible_code):
        self._can_manufacture = True
        self._trigger_event(
            self.Started(
                batch_id=BatchId(),
                location_code=location_code,
                batch_number=batch_number,
                responsible_code=responsible_code,
            )
        )

    def stop(self):
        self._can_manufacture = False
        self._trigger_event(self.Stopped())
