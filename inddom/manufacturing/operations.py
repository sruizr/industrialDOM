from inddom.ddd import Aggregate, Attribute, Event, RandomId


class OperationId(RandomId):
    pass


class Operation(Aggregate):
    class Started(Event):
        def apply_on(self, operation):
            operation._id = self.op_id
            operation._location_code = self.location_code
            operation._responsible_code = self.responsible_code
            operation.batch_number = self.batch_number
            super().apply_on(operation)

    class PartTransformed(Event):
        def apply_on(self, operation):
            operation._part_ids.append(self.part_id)
            super().apply_on(operation)

    def __init__(self):
        self._part_ids = []
        super().__init__()

    def start(self, responsible_code, location_code, batch_number):
        self._trigger_event(
            self.Started(
                op_id=OperationId(),
                location_code=location_code,
                responsible_code=responsible_code,
                batch_number=batch_number,
            )
        )

    def transform_part(self, part, new_part_number):
        part.transform(new_part_number, self)
        self._trigger_event(
            self.PartTransformed(part_id=part.id, part_number=new_part_number)
        )
