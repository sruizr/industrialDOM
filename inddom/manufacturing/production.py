from contextlib import contextmanager

from inddom.eventsourcing import AggregateVersionConflict
from inddom.hhrr.models.persons import PersonId
from inddom.logistics.models.parts import Part, PartId
from inddom.manufacturing.models.batchs import Batch, OpFlow


class NoBatchStarted(Exception):
    pass


class WrongResponsible(Exception):
    pass


class PartNoExist(Exception):
    pass


class PartNotFound(Exception):
    pass


class WrongPartModel(Exception):
    pass


class FlowNotFound(Exception):
    pass


class PartAlreadyManufactured(Exception):
    def __init__(self, part):
        self.part = part
        super().__init__(
            f"Part {part.part_number} with serial number {part.serial_number} was manufactured before!"
        )


class TransformDto:
    def __init__(self, process, part):
        self.part_number = part.part_number
        self.part_pars = part.parameters.copy()
        self.batch_number = process.batch_number
        self.serial_number = part.part_number
        self.location_code = part.location_code


class ManufacturingProcess:
    def __init__(self, line_code, event_source, projector=None):
        self._line_code = line_code
        self._event_source = event_source
        self._projector = projector

    def set_responsible(self, operator_code):
        pass

    def produce(self, part_number, serial_number, location_code):
        part = Part(
            part_number=part_number,
            serial_number=serial_number,
            location_code=location_code,
        )

        try:
            self._event_source.save(part)
        except AggregateVersionConflict:
            raise PartAlreadyManufactured(part)


class TransformationProcess:
    def __init__(self, store):
        self._store = store
        self._allowed_flows = set()
        self._batch = None
        self._active_flow = None

    @property
    def batch_number(self):
        return getattr(self._batch, "batch_number", None)

    @property
    def outcome(self):
        return getattr(self._active_flow, "outcome", None)

    @property
    def responsible_code(self):
        return getattr(self._batch, "responsible_code", None)

    def add_flow(self, part_change, from_location, to_location):
        flow = OpFlow(
            income=part_change[0],
            outcome=part_change[1],
            from_location=from_location,
            to_location=to_location,
        )

        self._allowed_flows.add(flow)

    def _set_active_flow(self, outcome):
        if self._active_flow and self._active_flow.outcome == outcome:
            return

        for flow in self._allowed_flows:
            if flow.outcome == outcome:
                self._active_flow = flow
                return

        raise FlowNotFound(f"Not found flow for {outcome}")

    def start_batch(self, responsible_code, batch_number, part_number):
        responsible = self._store.load(PersonId(responsible_code))
        if not responsible:
            raise WrongResponsible(
                f"Not found responsible with code {responsible_code}"
            )

        self._set_active_flow(part_number)
        if self._batch:
            self.end_batch()

        self._batch = Batch()

        self._batch.start(
            self._active_flow.from_location, batch_number, responsible_code
        )
        self._store.save(self._batch)

    def transform(self, serial_number):
        part = self._check_part(serial_number)
        self._exec_transform(part)

    def _check_part(self, serial_number):
        if not self._batch:
            raise NoBatchStarted("Batch has not be started for running tranformation")
        part = self._store.load(PartId(serial_number))

        if not part:
            raise PartNoExist(f'Part with sn "{serial_number}" no exist on store')

        if part.part_number != self._active_flow.income:
            raise WrongPartModel(
                f'Part with part_number "{part.part_number}" can not tranform '
                f'to "{self._active_flow.income}"'
            )

        if part.location_code != self._active_flow.from_location:
            raise PartNotFound(
                f'Part with sn {serial_number} not found on "{self._active_flow.from_location}"'
            )
        return part

    def _exec_transform(self, part):
        part.transform(self._active_flow.outcome, self._batch)
        part.move_to(self._active_flow.to_location)

        self._store.save(part)

    @contextmanager
    def transform_context(self, serial_number):
        part = self._check_part(serial_number)
        yield TransformDto(self, part)
        self.exec_transform(part)

    def end_batch(self):
        self._batch.stop()
        self._store.save(self._batch)
        self._batch = None
