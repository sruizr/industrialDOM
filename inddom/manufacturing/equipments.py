from datetime import datetime, timedelta
from typing import Any, Callable
from uuid import UUID

from inddom.ddd import Aggregate, Attribute, Event, KeyId
from inddom.hhrr.persons import Person
from inddom.ports import Store
from inddom.values import Validation


class EquipmentId(KeyId):
    def __init__(self, code: str | None = None, uuid: UUID | None = None):
        super().__init__(Equipment, code, uuid=uuid)


class Equipment(Aggregate):
    code = Attribute()
    name = Attribute()
    description = Attribute()
    epi_name = Attribute()
    epi = Attribute()
    validation = Attribute()

    class EquipmentIdentified(Event):
        def apply_on(self, equipment):
            equipment._id = EquipmentId(self.code)
            equipment._code = self.code
            equipment._name = self.eq_name
            equipment._description = self.description
            equipment._epi_name = self.epi_name

    class EpiIsConfigured(Event):
        def apply_on(self, equipment):
            equipment._epi_pars = self.epi_pars

    class EquipmentIsValidated(Event):
        def apply_on(self, equipment):
            equipment._validation = Validation(
                equipment.id,
                validation_period=self.validation_period,
                validated_by=self.validated_by,
                today=self.issue_date,
            )

    def __init__(self):
        super().__init__()
        self._epi_pars = {}
        self._epi = None
        self._validation = None

    @property
    def epi_pars(self) -> dict:
        return self._epi_pars.copy()

    def identify(self, code: str, name: str, description: str, epi_name: str):
        self._trigger_event(
            self.EquipmentIdentified(
                code=code,
                eq_name=name,
                epi_name=epi_name,
                description=description,
            )
        )
        pass

    def config_epi(self, epi_pars: dict):
        self._trigger_event(self.EpiIsConfigured(epi_pars=epi_pars))

    @property
    def eq_dependency_ids(self):
        dependencies = []
        for value in self._epi_pars.values():
            if "EquipmentId" in value:
                code = value[11:-1]
                dependencies.append(EquipmentId(code))
        return dependencies

    def load_equipment_programming_interface(
        self, create_equipment: Callable, store: Store
    ):

        if not self.is_valid():
            raise EquipmentNotValid(self)

        epi_pars = {}
        for key, value in self.epi_pars.items():
            if type(value) is str:
                print(value[:12])
            if type(value) is str and "EquipmentId(" == value[:12]:
                value = store.load(EquipmentId(value[12:-1]))
                value.load_equipment_programming_interface(create_equipment, store)
                value = value.epi
            epi_pars[key] = value

        self._epi = create_equipment(self.epi_name, epi_pars)
        self._epi.identify(self.code)

    def validate(
        self,
        validation_period: int,
        validated_by: Person,
        today: datetime | None = None,
    ):
        """Validate equipment for a period of time."""
        today = today if today else datetime.today()
        self._trigger_event(
            self.EquipmentIsValidated(
                issue_date=today,
                validation_period=validation_period,
                validated_by=validated_by.id,
            )
        )

    def is_valid(self):
        return self._validation is None or not self._validation.has_caducated()


class EquipmentNotValid(Exception):
    def __init__(self, equipment):
        super().__init__(
            f"Equipment with code {equipment.code} has caducated validation since {equipment.validation.caducation_date}"
        )
