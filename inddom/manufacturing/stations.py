from typing import Any, Callable

from inddom.ddd import Aggregate, Attribute, Event, KeyId
from inddom.logistics.parts import Part, PartId
from inddom.manufacturing.equipments import EquipmentId
from inddom.ports import Store
from inddom.values import PartInfo


class StationId(KeyId):
    def __init__(self, code=None, uuid=None):
        if code:
            super().__init__(Station, code, uuid=uuid)


class Station(Aggregate):
    code = Attribute()
    description = Attribute()
    origin = Attribute()
    force_part_origin = Attribute()

    class StationIdentified(Event):
        def apply_on(self, station):
            station._id = StationId(self.code)
            station._code = self.code
            station._description = self.description

    class EquipmentsAdded(Event):
        def apply_on(self, station):
            station._equipment_ids.extend(self.equipment_ids)

    class OriginIsSet(Event):
        def apply_on(self, station):
            station._origin = self.origin
            station._force_part_origin = self.force_part_origin

    def __init__(self):
        super().__init__()
        self._equipments = []
        self._equipment_ids = []

    @property
    def equipment_ids(self) -> tuple:
        return tuple(self._equipment_ids)

    @property
    def equipments(self) -> list[Any]:
        return self._equipments.copy()

    def identify(self, code: str, description: str | None = None):
        self._trigger_event(self.StationIdentified(code=code, description=description))

    def set_origin(self, origin: str, force_part_origin: bool = True):
        if "*" == origin[0]:
            force_part_origin = False
            origin = origin[1:]

        self._trigger_event(
            self.OriginIsSet(origin=origin, force_part_origin=force_part_origin)
        )

    def add_equipments(self, *equipments):
        self._equipments.extend(equipments)
        self._trigger_event(
            self.EquipmentsAdded(equipment_ids=[value.id for value in equipments])
        )

    def add_equipments_from_codes(self, *codes):
        self._trigger_event(
            self.EquipmentsAdded(equipment_ids=[EquipmentId(code) for code in codes])
        )

    def collect_part(self, part_info: PartInfo, store: Store) -> Part:
        part = store.load(PartId(part_info.serial_number))
        if part is None:
            part = Part()
            part.create(
                part_info.part_number,
                part_info.serial_number,
                self.origin,
                part_info.extra_info,
            )

        if self.force_part_origin and part.location_code != self.origin:
            raise PartNotInLocation(part, self.origin)

        if part.location_code != self.origin:
            part.move_to(self.origin)

        return part

    def load_equipments(self, store: Store, create_equipment: Callable | None = None):
        self._equipments.clear()
        for eq_id in self._equipment_ids:
            equipment = store.load(eq_id)
            if create_equipment:
                equipment.load_equipment_programming_interface(create_equipment, store)
            self._equipments.append(equipment)


class PartNotInLocation(Exception):
    def __init__(self, part: Part, location_code: str):
        super().__init__(
            f"Part with serial number {part.serial_number} is in {part.location_code} instead of {location_code}"
        )
