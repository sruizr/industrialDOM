import uuid
from dataclasses import dataclass

from inddom.ddd import Attribute
from inddom.hhrr.persons import PersonId
from inddom.hhrr.sessions import Session
from inddom.infrastructures import Client
from inddom.logistics.containers import BoxId, RejectBox
from inddom.product.definitions import RequirementId
from inddom.quality.tests import Defect


@dataclass
class ServiceSession:
    session_uuid: uuid.UUID
    session: Session
    client: Client
    box: RejectBox


class _RequirementCache:
    def __init__(self, store):
        self._data = {}
        self._store = store

    def get(self, requirement_uuid):
        id = RequirementId(uuid=requirement_uuid)
        return self._store.load(id)


class ContainmentService:
    store = Attribute()

    def __init__(self, store, box_inventory, defect_catalogs):
        self._store = store
        self._inventory = box_inventory
        self._defect_catalogs = defect_catalogs

        self._sessions = {}
        self._requis = _RequirementCache(store)

    def start_session(self, client):
        person = self._store.load(PersonId(client.responsible_code))

        session = Session()
        session.start(person, client.location_code, client.key)
        self._sessions[session.id.uuid] = (client, session)

        self._store.save(session)

        client.event_is_triggered(
            {
                "event_name": "SessionIsStarted",
                "event_pars": {
                    "responsible_name": f"{person.surname}, {person.firstname}",
                    "location_code": client.location_code,
                    "key": client.key,
                },
            }
        )

        return session.id.uuid

    def take_reject_box(self, session_uuid, box_uuid):
        """Take a reject box from quarantine area."""
        box = self._store.load(BoxId(uuid=box_uuid))
        self._box = box

        client = self._sessions[session_uuid][0]
        client.event_is_triggered(
            {
                "event_name": "RejectBoxTaken",
                "event_pars": {
                    "part_number": box.part_number,
                    "tracking": box.tracking,
                    "uuid": box_uuid,
                    "qty": box.qty,
                },
            }
        )

    def rework_part(self, session_uuid, defect_dto, qty=1):
        """Rework part/parts for a concrete defect."""

        requi = self._requis.get(defect_dto["requirement_uuid"])
        mode = defect_dto["mode"]
        session = self._sessions[session_uuid][1]
        defect = Defect(requi.id, mode, self._box.id, session.id)
        self._box.add_defect(defect)
        self._box.remove_part(qty)
        self._store.save(self._box)

        self._notify(
            session_uuid,
            "PartReworked",
            {
                "defect": {
                    "path": requi.path,
                    "mode": mode,
                    "attribute": requi.characteristic.attr,
                    "element": requi.characteristic.element,
                    "eid": requi.eid,
                },
                "qty": qty,
            },
        )

    def _notify(self, session_uuid, event_name, event_pars):
        client = self._sessions[session_uuid][0]
        client.event_is_triggered({"event_name": event_name, "event_pars": event_pars})
