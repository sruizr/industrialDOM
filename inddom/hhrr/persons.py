import datetime

from inddom.ddd import Aggregate, Attribute, Event, KeyId


class NotAuthorizedPerson(Exception):
    def __init__(self, activity, person):
        super().__init__(f'{person} is not authorized to develop activity "{activity}"')


class PersonId(KeyId):
    def __init__(self, code=None, uuid=None):
        super().__init__(Person, code, uuid=uuid)


class Person(Aggregate):
    code = Attribute()
    username = Attribute()
    surname = Attribute()
    firstname = Attribute()

    class Hired(Event):
        def apply_on(self, person):
            person._id = PersonId(self.code)
            person._code = self.code
            person._username = self.username

            person._surname, person._firstname = self.full_name.split(", ")

    class RoleAdded(Event):
        def apply_on(self, person):
            person._roles.append(self.role)

    def __init__(self):
        self._roles = []
        super().__init__()

    @property
    def roles(self):
        return [role for role in self._roles]

    def hire(self, code, username, full_name):
        self._trigger_event(
            self.Hired(
                code=code, username=username, full_name=full_name, person_id=None
            )
        )

    def authorize(self, role_name, responsible):
        self._trigger_event(
            self.RoleAdded(
                role=role_name,
                responsible_id=responsible.id,
                issue_date=datetime.datetime.now(),
            )
        )


class Role(Aggregate):
    pass
