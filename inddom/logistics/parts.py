import datetime
import logging

from inddom.ddd import Aggregate, Attribute, Event, KeyId

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class OldPartId(KeyId):
    def __init__(self, part_number=None, serial_number=None, uuid=None):
        super().__init__(Part, part_number, serial_number, uuid=uuid)


class PartId(KeyId):
    def __init__(self, serial_number=None, uuid=None):
        super().__init__(Part, serial_number, uuid=uuid)


class Part(Aggregate):
    part_number = Attribute()
    serial_number = Attribute()
    location_code = Attribute()
    parameters = Attribute()

    def __init__(self):
        self._defects = []
        self._parameters = {}
        super().__init__()

    class Created(Event):
        def apply_on(self, aggregate):
            aggregate._id = PartId(self.serial_number)
            aggregate._part_number = self.part_number
            aggregate._serial_number = self.serial_number
            aggregate._location_code = self.location_code
            if self.parameters:
                aggregate._parameters.update(self.parameters)

    class ParametersAdded(Event):
        def apply_on(self, part):
            part._parameters.update(self.pars)

    class ParameterAdded(Event):
        def apply_on(self, part):
            part._parameters[self.key] = self.value

    class Moved(Event):
        def apply_on(self, aggregate):
            aggregate._location_code = self.location_code

    class DefectAdded(Event):
        def apply_on(self, part):
            part._defects.append(self.defect)

    class DefectRemoved(Event):
        def apply_on(self, part):
            index = part._defects.index(self.defect)
            part._defects.pop(index)

    class Transfomed(Event):
        def apply_on(self, part):
            part._part_number = self.part_number
            part._operation_id = self.operation_id

    class Validated(Event):
        def apply_on(self, part):
            pass

    def create(self, part_number, serial_number, location_code, parameters=None):

        event = self.Created(
            part_number=part_number,
            serial_number=serial_number,
            location_code=location_code,
            parameters=parameters,
        )
        self._trigger_event(event)

    def add_parameter(self, key, value):
        self._trigger_event(self.ParameterAdded(key=key, value=value))

    def move_to(self, location_code):
        logger.debug(
            f"Part has location {self.location_code} and requestted to move to {location_code}"
        )
        if self.location_code == location_code:
            return

        self._trigger_event(self.Moved(location_code=location_code))

    @property
    def defects(self):
        return self._defects.copy()

    def add_defect(self, defect):
        if defect in self._defects:
            return

        self._trigger_event(self.DefectAdded(defect=defect))

    def remove_defect(self, defect, test_id):
        if defect not in self._defects:
            return

        self._trigger_event(self.DefectRemoved(defect=defect, test_id=test_id))

    def clear_defects(self, requirement, test_id):
        for defect in self._defects:
            if defect.requirement_id == requirement.id:
                self.remove_defect(defect, test_id)
                return

        for sub_id in requirement.requirement_ids:
            for defect in self._defects:
                if defect.requirement_id == sub_id:
                    self.remove_defect(defect, test_id)

    def is_defective(self):
        return not not self._defects

    def validate(self, validator):
        self._trigger_event(
            self.Validated(
                validator_id=validator.id, issue_date=datetime.datetime.now()
            )
        )

    def transform(self, part_number, batch):
        if self.part_number == part_number:
            return

        self._trigger_event(
            self.Transfomed(part_number=part_number, operation_id=batch.id)
        )

    def add_parameters(self, **parameters):
        self._trigger_event(self.ParametersAdded(**parameters))
