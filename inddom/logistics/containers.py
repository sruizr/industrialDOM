from inddom.ddd import Aggregate, Attribute, Event, RandomId


class BoxId(RandomId):
    pass


class RejectBox(Aggregate):
    part_number = Attribute()
    tracking = Attribute()
    outcome = Attribute()
    qty = Attribute()

    class Created(Event):
        def apply_on(self, box):
            box._id = self.id
            box._part_number = self.part_number
            box._tracking = self.tracking
            box._qty = self.qty
            box._outcome = self.outcome

    def create(self, part_number, tracking, qty=1, outcome=None):
        id_ = BoxId()
        self._trigger_event(
            self.Created(
                id=id_,
                part_number=part_number,
                tracking=tracking,
                qty=qty,
                outcome=outcome,
            )
        )

    def add_defect(self, defect, qty=1):
        pass

    def correct_defect(self, defect, qty=1):
        pass

    def suspect_defect(self, defect):
        pass

    def remove_part(self, qty=1):
        pass

    def add_part(
        self,
        box,
        qty,
    ):
        pass

    def move_part_to_box(self, destination_box, qty=1, defect=None):
        pass

    def mark_as_suspicious(self, defect):
        pass

    def is_suspicious(self):
        pass

    def unmark_as_suspicious(self):
        pass
