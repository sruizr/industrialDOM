from inddom.eventsourcing import AggregateNotFound
from inddom.logistics.models.parts import PartId


class PartNotFound(Exception):
    def __init__(self, part_number, serial_number):
        self.part_number = part_number
        self.serial_number = serial_number


class PartSetter(Service):
    def place_part(self, part_number, serial_number, to_location_code):
        try:
            part = self._event_source.load(PartId(part_number, serial_number))
        except AggregateNotFound:
            raise PartNotFound(part_number, serial_number)

        part.move(to_location_code)
