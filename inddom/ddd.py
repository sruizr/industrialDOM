"""Base classes for Domain Driven Development."""
import abc
import logging
import uuid as _uuid

from eventence.core.commands import EventHandler

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Attribute:
    """Attribute interface for entities."""

    def __set_name__(self, owner, name):
        """Set attribute in class."""
        self.public_name = name
        self.private_name = f"_{name}"

    def __get__(self, object, name, owner=None):
        """Return private attribute."""
        return getattr(object, self.private_name)

    def __set__(self, instance, value):
        """Avoid to be set directly."""
        raise AttributeError(
            "Attribute {self.public_name} can not be modified directly"
        )


class Entity(abc.ABC):
    """Entity implementation following Doman Driven Design."""

    id = Attribute()

    def __init__(self):
        """Identify entity by its id."""
        self._id = None

    def __hash__(self):
        """Return unique hash value for entity."""
        return hash("{}:{}".format(self.__class__.__name__, self._id))

    def __eq__(self, other):
        return self._id == other.id

    def __repr__(self):
        """Return readable representation of aggregate."""
        name = self.__class__.__name__
        return f"{name}(id={self.id!r})"


class EntityFactory:
    """Returns an entity from its full name."""

    def create(self, qual_name):
        """Create class from its name."""
        pass


class Aggregate(Entity):
    """Aggregate implementation of Domain Driven Design."""

    def __init__(self):
        """Manage states using events(event sourcing)."""
        super().__init__()
        self.event_handler = EventHandler(self)

    def _trigger_event(self, event):
        """Trigger event to update aggregate state."""
        event.apply_on(self)
        self.event_handler.add_to_history(event)


class Value:
    """Value object implementation of Domain Driven Design."""

    def __init__(self, **kwargs):
        """Inmute object."""
        self.__dict__.update(kwargs)

    def __setattr__(self, name, value):
        """Avoid any change on attributes."""
        raise AttributeError("Value objects can not have new attributes")

    def __eq__(self, other):
        """Return true if value object is equal to other."""
        return (
            self.__class__.__qualname__ == other.__class__.__qualname__
            and self.__dict__ == other.__dict__
        )

    def __neq__(self, other):
        """Return false if value is diferent to other."""
        return not self.__eq__(other)

    def __hash__(self):
        """Return unique hash value of object."""
        return hash(repr(self))

    def __repr__(self):
        """Return readable representation of value."""
        sorted_pars = sorted(self.__dict__.items())
        args_strings = ("{0}={1!r}".format(*item) for item in sorted_pars)
        args_string = ", ".join(args_strings)
        return "{}({})".format(self.__class__.__name__, args_string)

    @property
    def invariants(self):
        """Return inmutable attributes of value."""
        return self.__dict__.copy()

    def as_plain_dict(self):
        plain = self.invariants
        for key in plain.keys():
            if issubclass(type(plain[key]), Value):
                plain[key] = plain[key].as_plain_dict()
        return plain


class Event(Value):
    """Value object implementing a Event ocurrence on aggregate."""

    def __init__(self, **parameters):
        """Change aggregate state."""
        super().__init__(**parameters)

    @property
    def name(self):
        """Return name of event."""
        return self.__class__.__name__

    @property
    def pars(self):
        """Return parameters of event."""
        return self.invariants

    def apply_on(self, aggregate):
        """Apply event over aggregate."""
        raise NotImplementedError()


class _Id(Value):
    def __hash__(self):
        """Return unique hash for id object."""
        return hash(self.__class__.__name__ + self.uuid.hex)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.uuid == other.uuid

    def as_plain_dict(self):
        return {'uuid': self.uuid.hex }


class KeyId(_Id):
    """Id representation by some string keys."""

    def __init__(self, *args, **kwargs):
        """Load id if uuid is done, otherwise creates it."""
        if kwargs.get("uuid"):
            uuid = kwargs["uuid"]
            if type(uuid) is not _uuid.UUID:
                raise ValueError(f"{uuid} is not of UUID class")
        else:
            EntityClass, entity_keys = (args[0], args[1:])
            module = EntityClass.__module__
            class_name = EntityClass.__name__
            valid_keys = [str(key) for key in entity_keys if key is not None]
            keys = ":".join(valid_keys)
            key = f"{module}.{class_name}/{keys}"

            uuid = _uuid.uuid3(_uuid.NAMESPACE_DNS, key)

        super().__init__(uuid=uuid)


class RandomId(_Id):
    """Random id representation."""

    def __init__(self, uuid=None):
        """Create new uuid if uuid is None, restore otherwise."""
        if uuid is None:
            uuid = _uuid.uuid1()

        if type(uuid) is not _uuid.UUID:
            raise ValueError(f"{uuid} is not of UUID class")

        super().__init__(uuid=uuid)
