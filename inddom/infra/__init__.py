import abc


class StoreFactory:
    pass


class ProjectorFactory:
    pass


class Client(abc.ABC):
    @abc.abstractproperty
    def key(self):
        ...

    @abc.abstractproperty
    def responsible_code(self):
        ...

    @abc.abstractproperty
    def location_code(self):
        ...

    @abc.abstractmethod
    def get_response(self, request):
        ...

    @abc.abstractmethod
    def event_is_triggered(self, event_dict):
        ...


class Query(abc.ABC):
    @abc.abstractmethod
    def __call__(self, **kwargs):
        pass
