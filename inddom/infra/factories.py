import importlib

import eventence as ev

from inddom.infra.projections.quality import ControlPlanFinder


class PersistenceFactory:
    def __init__(self, store_engine: ev.DbEngine):
        self._event_factory = ev.Persistence(store_engine)

    def create_store(self):
        return self._event_factory.create_store()

    def create_control_plan_finder(self, projection_engine: ev.DbEngine):
        """Create query for finding control plans by location and product_key."""

        query = ControlPlanFinder(
            self._get_projection_data(
                projection_engine, projection_data_name="ControlPlansData"
            )
        )
        self._event_factory.register_projection(query)
        return query

    def _get_projection_data(self, engine: ev.DbEngine, projection_data_name: str):
        module = importlib.import_module(f"inddom.infra.projections.{engine.name}")
        projection_data_name = f"{engine.name.capitalize()}{projection_data_name}"

        ProjectionData = getattr(module, projection_data_name)
        return ProjectionData(**engine.parameters)
