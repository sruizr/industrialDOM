from eventence.core.commands import EventDataTransfer

from inddom.infra.projections.hhrr import PersonId, PersonsData
from inddom.infra.projections.product import (
    PartModelId,
    PartModelsData,
    RequirementId,
    RequirementsData,
)
from inddom.infra.projections.quality import ControlPlansData
from inddom.quality.plans import ControlPlanId


class _OnmemData:
    def __init__(self):
        self._data = {}
        self._index = -1

    @property
    def last_processed_event_id(self) -> int | None:
        return self._index


class OnmemControlPlansData(_OnmemData, ControlPlansData):
    def insert_control_plan(self, event_dto: EventDataTransfer):
        control_plan_id = ControlPlanId(uuid=event_dto.entity_uuid)
        pars = event_dto.event_pars
        self._data[control_plan_id] = {
            "id": control_plan_id,
            "from_location_code": pars["from_location_code"],
            "to_location_code": pars["to_location_code"],
            "affected": [],
        }
        self._index = event_dto.id

    def update_add_affected(self, event_dto: EventDataTransfer):
        control_plan_id = ControlPlanId(uuid=event_dto.entity_uuid)
        pars = event_dto.event_pars
        self._data[control_plan_id]["affected"].append(pars["affected"])
        self._index = event_dto.id

    def select_control_plan_id(self, location_code, affected_key):
        for cp in self._data.values():
            if (
                location_code == cp["from_location_code"]
                and affected_key in cp["affected"]
            ):
                return cp["id"]


class OnmemRequirementsData(_OnmemData, RequirementsData):
    def insert_requirement(self, event_dto: EventDataTransfer):
        requi_id = event_dto.entity_uuid
        pars = event_dto.event_pars
        char = pars["characteristic"]
        specs = pars["specs"].invariants if pars["specs"] else None
        self._data[requi_id] = {
            "id": requi_id.hex,
            "path": pars["path"],
            "characteristic": "{}@{}".format(char.attr, char.element),
            "eid": pars["eid"],
            "specs": specs,
        }
        self._index = event_dto.id

    def select_requirement(self, requirement_id: RequirementId):
        return self._data.get(requirement_id.uuid, None)


class OnmemPartModelsData(_OnmemData, PartModelsData):
    def insert_part_model(self, event_dto: EventDataTransfer):
        part_model_id = event_dto.entity_uuid
        pars = event_dto.event_pars
        self._data[part_model_id] = {
            "id": part_model_id.hex,
            "part_number": pars["part_number"],
            "description": pars["description"],
            "part_name": pars["part_name"],
            #            'groups': []
        }
        self._index = event_dto.id

    def select_part_model(self, part_number: str):
        part_model_id = PartModelId(part_number)
        return self._data.get(part_model_id.uuid)

    def update_add_group(self, event_dto):
        pass


class OnmemPersonsData(_OnmemData, PersonsData):
    def insert_person(self, event_dto: EventDataTransfer):
        person_id = event_dto.entity_uuid
        pars = event_dto.event_pars
        self._data[person_id] = {
            "id": person_id.hex,
            "full_name": pars["full_name"],
            "username": pars["username"],
        }
        self._index = event_dto.id

    def select_person(self, person_id: PersonId):
        return self._data.get(person_id.uuid)
