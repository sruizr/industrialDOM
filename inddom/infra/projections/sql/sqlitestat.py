from sqlite3 import connect


def create_connection(filename, **kwargs):
    return connect(filename, **kwargs)


class Statements:

    create_requirements = """
    CREATE TABLE "requirements" (
    "id"	TEXT NOT NULL UNIQUE,
    "path"	TEXT,
    "characteristic"	TEXT NOT NULL,
    "eid"	TEXT,
    "specs"	TEXT,
    PRIMARY KEY("id")
    )"""

    insert_requirement = """
    insert into requirements (id, path, characteristic, eid, specs)
    values
    (:id, :path, :characteristic, :eid, :specs)
    """

    select_requirement = """
    select * from requirements where id=?
    """

    update_add_group = """
    update part_models set groups = groups + :group + "/"
    where id=:id
    """

    insert_part_model = """
    insert into part_models (id, part_number, part_name, description)
    values (:id, :part_number, :part_name, :description)"""

    create_part_models = """
    CREATE TABLE "part_models" (
    "id" TEXT NOT NULL,
    "part_number" TEXT,
    "part_name" TEXT,
    "description" TEXT,
    PRIMARY KEY("id"));
    """
    select_part_model = "select * from part_models where id=?"

    create_persons = """
    create table "persons" (
    "id" text not null,
    "full_name" text,
    "username" text,
    primary key("id"))
    """
    insert_person = """
    insert into persons (id, full_name, username)
    values (:id, :full_name, :username)"""

    select_person = "select * from persons where id=?"

    create_control_plans = """
    create table "control_plans" (
    "id" text,
    "from_location_code" text,
    "to_location_code" text,
    "affected" text,
    primary key("id"))
    """

    insert_control_plan = """
    insert into control_plans (id, from_location_code, to_location_code, affected)
    values (:id, :from_location_code, :to_location_code, "|")
    """

    update_add_affected = """
    update control_plans set affected = affected || :affected || "|"
    where id=:id
    """

    select_control_plan_id = """
    select id from control_plans
    where from_location_code =:from_location_code and instr(affected,
    "|" || :affected || "|") > 0
    """

    create_last_events = """
    create table "last_events" (
    "projection" text,
    "last_id" integer,
    primary key("projection"))
    """
    select_last_event = """
    select last_id from last_events
    where projection = ?"""

    insert_last_event = """
    insert into last_events (projection, last_id)
    values (:projection, :last_id)"""

    update_last_event = """
    update last_events set last_id = :last_id where
    projection = :projection"""

    # create_event_schema = [
    #     """CREATE TABLE "aggregates" (
    #     "id" TEXT,
    #     "module" TEXT,
    #     "name" TEXT,
    #     primary key("id"))
    #     """,
    #     """
    #     CREATE TABLE "events" (
    #     "id" INTEGER,
    #     "created_on" timestamp,
    #     "aggregate_id" INTEGER,
    #     "name" TEXT,
    #     "version" INTEGER,
    #     "pars" TEXT,
    #     PRIMARY KEY("id")
    #     foreign key(aggregate_id) references aggregates(id))
    #     """,
    #     """
    #     CREATE INDEX "agregate_name" ON "aggregates" (
    #     "module",
    #     "name")
    #     """,
    #     """
    #     create unique index "events_select" on "events" (
    #     "aggregate_id",
    #     "version")
    #     """,
    #     """
    #     create view aggregate_events
    #     as
    #     select
    #     events.id as id, events.name as name, version, created_on,
    #     aggregates.id as aggregate_id, aggregates.name as aggregate_name,
    #     aggregates.module as aggregate_module, pars
    #     from
    #     events inner join aggregates on events.aggregate_id = aggregates.id
    #     """
    # ]

    # select_event_index = "select max(id) from events"

    # select_aggregate_events = """
    #     select * from aggregate_events where aggregate_id=? order by version asc
    # """
    # insert_event = """
    # insert into events (created_on, aggregate_id, name, version, pars)
    # values (:created_on, :aggregate_id, :name, :version, :pars)
    # """
    # insert_aggregate = """
    # insert into aggregates (id, module, name)
    # values (:aggregate_id, :aggregate_module, :aggregate_name)"""

    # select_all_aggregate_events = """
    # select *
    # from aggregate_events
    # where aggregate_module=? and aggregate_name=? and id > ? order by id"""
