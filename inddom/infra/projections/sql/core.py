import json
import threading
import uuid
from dataclasses import asdict

from eventence.core.commands import EventDataTransfer

from inddom.infra.projections.hhrr import PersonId, PersonsData
from inddom.infra.projections.product import (
    PartModelId,
    PartModelsData,
    RequirementId,
    RequirementsData,
)
from inddom.infra.projections.quality import ControlPlanId, ControlPlansData


class _SqlData:
    def __init__(self, name, db_connection, statements):
        self._conn = db_connection
        self._statements = statements
        self._name = name
        self._lock = threading.Lock()
        if not self._exist_schema():
            self.create_schema()

    @property
    def last_processed_event_id(self):
        cursor = self._conn.cursor()
        try:
            cursor.execute(self._statements.select_last_event, (self._name,))
            row = cursor.fetchone()
            if row:
                return row[0]

        except Exception as e:
            if e.__class__.__name__ == "OperationalError":
                return
            else:
                raise e

    def _set_last_event_id(self, value, cursor):
        cursor.execute(
            self._statements.update_last_event,
            {"projection": self._name, "last_id": value},
        )

    def _create_last_events_schema(self, cursor):
        pass

    def _exist_schema(self):
        cursor = self._conn.cursor()
        cursor.execute(
            f"select name from sqlite_master where type='table' and name='{self._name}'"
        )
        row = cursor.fetchone()
        print(row)
        if row:
            return True

        return False

    def _create_tables(self, cursor):
        pass

    def _try_create_last_event_record(self, cursor):
        try:
            cursor.execute(
                self._statements.insert_last_event,
                {"projection": self._name, "last_id": 0},
            )
        except Exception as e:
            if e.__class__.__name__ == "OperationalError":
                return
            else:
                raise e

    def _try_create_last_events_schema(self, cursor):
        try:
            cursor.execute(self._statements.create_last_events)
        except Exception as e:
            if e.__class__.__name__ == "OperationalError":
                return
            else:
                raise e

    def create_schema(self):
        if self._exist_schema():
            return
        print("Creating schema...")

        with self._lock:
            cursor = self._conn.cursor()
            self._try_create_last_events_schema(cursor)
            self._try_create_last_event_record(cursor)
            try:
                self._create_tables(cursor)
                self._conn.commit()
            except Exception as e:
                self._conn.rollback()
                raise e

    def _execute_event_dto(self, cursor, command, event_dto):
        cursor = self._conn.cursor()
        with self._lock:
            try:
                command(event_dto)
                self._set_last_event_id(event_dto.id, cursor)
                self._conn.commit()
            except Exception as e:
                self._conn.rollback()
                raise e


class SqlControlPlansData(_SqlData, ControlPlansData):
    def __init__(self, connection, statements):
        _SqlData.__init__(self, "control_plans", connection, statements)

    def insert_control_plan(self, event_dto: EventDataTransfer):
        cursor = self._conn.cursor()
        pars = event_dto.event_pars

        def command(event_dto):
            return cursor.execute(
                self._statements.insert_control_plan,
                {
                    "id": event_dto.entity_uuid.hex,
                    "from_location_code": pars["from_location_code"],
                    "to_location_code": pars["to_location_code"],
                },
            )

        self._execute_event_dto(cursor, command, event_dto)

    def update_add_affected(self, event_dto):
        cursor = self._conn.cursor()
        pars = event_dto.event_pars
        command = lambda dto: cursor.execute(
            self._statements.update_add_affected,
            {"id": event_dto.entity_uuid.hex, "affected": pars["affected"]},
        )
        self._execute_event_dto(cursor, command, event_dto)

    def select_control_plan_id(self, location_code, affected_key):
        cursor = self._conn.cursor()
        cursor.execute(
            self._statements.select_control_plan_id,
            {"from_location_code": location_code, "affected": affected_key},
        )

        row = cursor.fetchone()
        if row:
            return ControlPlanId(uuid=uuid.UUID(hex=row[0]))

    def _create_tables(self, cursor):
        cursor.execute(self._statements.create_control_plans)


class SqlRequirementsData(_SqlData, RequirementsData):
    def __init__(self, connection, statements):
        _SqlData.__init__(self, "requirements", connection, statements)

    def insert_requirement(self, event_dto: EventDataTransfer):
        cursor = self._conn.cursor()

        pars = event_dto.event_pars
        char = pars["characteristic"]
        specs = json.dumps(pars["specs"].invariants) if pars["specs"] else None
        command = lambda event_dto: cursor.execute(
            self._statements.insert_requirement,
            {
                "id": event_dto.entity_uuid.hex,
                "path": pars["path"],
                "characteristic": "{}@{}".format(char.attr, char.element),
                "eid": pars["eid"],
                "specs": specs,
            },
        )
        self._execute_event_dto(cursor, command, event_dto)

    def select_requirement(self, requirement_id: RequirementId):
        cursor = self._conn.cursor()
        cursor.execute(self._statements.select_requirement, (requirement_id.uuid.hex,))
        row = cursor.fetchone()
        specs = json.loads(row[4]) if row[4] else {}
        return {
            "id": row[0],
            "path": row[1],
            "characteristic": row[2],
            "eid": row[3],
            "specs": specs,
        }

    def _create_tables(self, cursor):
        cursor.execute(self._statements.create_requirements)


class SqlPartModelsData(_SqlData, PartModelsData):
    def __init__(self, connection, statements):
        _SqlData.__init__(self, "part_models", connection, statements)

    def insert_part_model(self, event_dto: EventDataTransfer):
        cursor = self._conn.cursor()
        pars = event_dto.event_pars
        command = lambda event_dto: cursor.execute(
            self._statements.insert_part_model,
            {
                "id": event_dto.entity_uuid.hex,
                "part_number": pars["part_number"],
                "part_name": pars["part_name"],
                "description": pars["description"],
            },
        )
        self._execute_event_dto(cursor, command, event_dto)

    def select_part_model(self, part_number: str):
        cursor = self._conn.cursor()
        part_model_id = PartModelId(part_number)
        cursor.execute(self._statements.select_part_model, (part_model_id.uuid.hex,))
        row = cursor.fetchone()
        return {
            "id": row[0],
            "part_number": row[1],
            "part_name": row[2],
            "description": row[3],
        }

    def _create_tables(self, cursor):
        cursor.execute(self._statements.create_part_models)

    def update_add_group(self, event_dto):
        pass
        # part_model_id = event_dto['entity']['id']
        # pars = event_dto['pars']
        # self._parts[part_model_id]['groups'].append(pars['group'])
        # self.index = event_dto['id']


class SqlPersonsData(_SqlData, PersonsData):
    def __init__(self, connection, statements):
        _SqlData.__init__(self, "persons", connection, statements)

    def insert_person(self, event_dto: EventDataTransfer):
        cursor = self._conn.cursor()
        pars = event_dto.event_pars
        command = lambda dto: cursor.execute(
            self._statements.insert_person,
            {
                "id": event_dto.entity_uuid.hex,
                "full_name": pars["full_name"],
                "username": pars["username"],
            },
        )

        self._execute_event_dto(cursor, command, event_dto)

    def select_person(self, person_id: PersonId):
        cursor = self._conn.cursor()
        cursor.execute(self._statements.select_person, (person_id.uuid.hex,))
        row = cursor.fetchone()

        if row:
            return {"id": row[0], "full_name": row[1], "username": row[2]}

    def _create_tables(self, curse):
        curse.execute(self._statements.create_persons)
