import abc

from eventence import EventHook, Projection

from inddom.infra import Query
from inddom.product.definitions import PartModelId, RequirementId


class PartModelsData(abc.ABC):
    @abc.abstractmethod
    def insert_part_model(self, event_dto):
        ...

    @abc.abstractproperty
    def last_processed_event_id(self) -> int:
        ...

    @abc.abstractmethod
    def select_part_model(self, part_number: str):
        ...


class PartModelInfoGetter(Projection, Query):
    def __init__(self, projection_data: PartModelsData):
        self._data = projection_data
        self._hooks = [EventHook("PartModel", "Defined", self._data.insert_part_model)]

    @property
    def event_hooks(self):
        return self._hooks

    @property
    def last_event_id(self) -> int:
        return self._data.last_processed_event_id

    def __call__(self, part_number):
        part_model_id = PartModelId(part_number)
        return self._data.select_part_model(part_model_id)


class RequirementsData(abc.ABC):
    @abc.abstractmethod
    def insert_requirement(self, event_dto):
        ...

    @abc.abstractproperty
    def last_processed_event_id(self) -> int:
        ...

    @abc.abstractmethod
    def select_requirement(self, requirement_id: RequirementId):
        ...


class RequirementInfoGetter(Projection, Query):
    def __init__(self, projection_data: RequirementsData):
        self._data = projection_data
        self._hooks = [
            EventHook("Requirement", "Defined", self._data.insert_requirement)
        ]

    @property
    def event_hooks(self):
        return self._hooks

    @property
    def last_event_id(self) -> int:
        return self._data.last_processed_event_id

    def __call__(self, requirement_id):
        return self._data.select_requirement(requirement_id)
