from .sql import core
from .sql.sqlitestat import Statements, create_connection


class SqliteControlPlansData(core.SqlControlPlansData):
    def __init__(self, filename, **kwargs):
        super().__init__(create_connection(filename, **kwargs), Statements)


class SqlitePartModelsData(core.SqlPartModelsData):
    def __init__(self, filename, **kwargs):
        super().__init__(create_connection(filename, **kwargs), Statements)


class SqliteRequirementsData(core.SqlRequirementsData):
    def __init__(self, filename, **kwargs):
        super().__init__(create_connection(filename, **kwargs), Statements)


class SqlitePersonsData(core.SqlPersonsData):
    def __init__(self, filename, **kwargs):
        super().__init__(create_connection(filename, **kwargs), Statements)
