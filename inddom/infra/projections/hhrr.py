import abc

from eventence import EventHook, Projection

from inddom.hhrr.persons import PersonId
from inddom.infra import Query


class PersonsData(abc.ABC):
    @abc.abstractmethod
    def insert_person(self, event_dto):
        ...

    @abc.abstractproperty
    def last_processed_event_id(self) -> int:
        ...

    @abc.abstractmethod
    def select_person(self, person_id: PersonId):
        ...


class PersonInfoGetter(Projection, Query):
    def __init__(self, projection_data: PersonsData):
        self._data = projection_data
        self._hooks = [EventHook("Person", "Hired", self._data.insert_person)]

    @property
    def event_hooks(self):
        return self._hooks

    @property
    def last_event_id(self) -> int:
        return self._data.last_processed_event_id

    def __call__(self, person_code):
        person_id = PersonId(code=person_code)
        return self._data.select_person(person_id)
