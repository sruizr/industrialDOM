import abc

from eventence import EventHook, Projection

from inddom.infra import Query
from inddom.quality.plans import ControlPlanId


class ControlPlansData(abc.ABC):
    @abc.abstractmethod
    def insert_control_plan(self, event_dto):
        ...

    @abc.abstractmethod
    def update_add_affected(self, event_dto):
        ...

    @abc.abstractproperty
    def last_processed_event_id(self) -> int:
        ...

    @abc.abstractmethod
    def select_control_plan_id(self, location, affected_part_key) -> ControlPlanId:
        ...


class ControlPlanFinder(Projection, Query):
    def __init__(self, projection_data: ControlPlansData):
        self._data = projection_data
        self._hooks = [
            EventHook("ControlPlan", "Created", self._data.insert_control_plan),
            EventHook("ControlPlan", "AffectedAdded", self._data.update_add_affected),
        ]

    @property
    def event_hooks(self):
        return self._hooks

    @property
    def last_event_id(self) -> int:
        return self._data.last_processed_event_id

    def __call__(self, location, affected_part_key) -> ControlPlanId:
        return self._data.select_control_plan_id(location, affected_part_key)
