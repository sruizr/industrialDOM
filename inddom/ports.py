import abc


class Store(abc.ABC):
    @abc.abstractmethod
    def load(self, entity_id):
        ...

    @abc.abstractmethod
    def save(self, entity):
        ...
