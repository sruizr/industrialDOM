import uuid

from inddom.product.definitions import RequirementId


class RequirementReading:
    """Supply requirement information to clients in a jsonable dict."""

    def __init__(self, store):
        self._store = store
        self._cache = {}

    def __call__(self, req_uuid):
        """Return requirement information in a jsonable dict."""
        if req_uuid not in self._cache:

            requi = self._store.load(RequirementId(uuid=req_uuid))
            dto = {
                "id": requi.id.uuid,
                "attribute": requi.characteristic.attr,
                "element": requi.characteristic.element,
                "eid": requi.eid,
                "path": requi.path,
            }
            if requi.specs:
                dto["specs"] = requi.specs.invariants
            if requi.requirement_ids:
                dto["requirements"] = []
                for id in requi.requirement_ids:
                    dto["requirements"].append(self(id.uuid))
            self._cache[req_uuid] = dto

        return self._cache[req_uuid]
