class RequirementLoader:
    def __init__(self, event_store):
        self._store = event_store
        self._cache = {}

    def __call__(self, requirement_id):
        if requirement_id in self._cache:
            return self._cache[requirement_id]

        requirement = self._store.load(requirement_id)
        self._load_subrequirements(requirement)
        self._cache[requirement_id] = requirement

        return requirement

    def _load_subrequirements(self, requirement):
        requirement.requirements = []
        for requirement_id in requirement.requirement_ids:
            requi = self._store.load(requirement_id)
            requirement.requirements.append(requi)
            self._load_subrequirements(requi)
