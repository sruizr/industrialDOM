from inddom.ddd import Aggregate, Attribute, Event, KeyId, RandomId, Value


class PartModelId(KeyId):
    def __init__(self, part_number=None, uuid=None):
        super().__init__(PartModel, part_number, uuid=uuid)


class PartModel(Aggregate):
    """Group a part under a standard definition."""

    part_number = Attribute()
    name = Attribute()
    description = Attribute()

    class Defined(Event):
        def apply_on(self, part_model):
            part_model._id = PartModelId(self.part_number)
            part_model._part_number = self.part_number
            part_model._description = self.description
            part_model._name = self.part_name

    class GroupAdded(Event):
        def apply_on(self, part_model):
            part_model._groups.append(self.group)

    def __init__(self):
        super().__init__()
        self._groups = []

    @property
    def groups(self):
        return self._groups.copy()

    def define(self, part_number, part_name, description):
        self._trigger_event(
            self.Defined(
                part_number=part_number, part_name=part_name, description=description
            )
        )

    def add_group(self, group):
        if group not in self._groups:
            self._trigger_event(self.GroupAdded(group=group))


class RequirementId(KeyId):
    def __init__(self, path=None, characteristic=None, eid=None, uuid=None):
        super().__init__(Requirement, path, repr(characteristic), eid, uuid=uuid)


class Requirement(Aggregate):
    path = Attribute()
    characteristic = Attribute()
    eid = Attribute()
    specs = Attribute()

    class Defined(Event):
        def apply_on(self, requi):
            requi._id = RequirementId(self.path, self.characteristic, self.eid)
            requi._characteristic = self.characteristic
            requi._eid = self.eid
            requi._path = self.path
            requi._specs = self.specs

    class RequirementAdded(Event):
        def apply_on(self, requi):
            requi._requirement_ids.append(self.requirement_id)

    class AffectedAdded(Event):
        def apply_on(self, requi):
            requi._affects_to.append(self.affected)

    class SpecificationChanged(Event):
        def apply_on(self, requi):
            requi._specs = self.specs

    def __init__(self):
        super().__init__()
        self._affects_to = []
        self._requirement_ids = []
        self.requirements = []

    @property
    def affects_to(self):
        return self._affects_to.copy()

    @property
    def requirement_ids(self):
        return self._requirement_ids.copy()

    @property
    def key(self):
        key = f"{self.characteristic.attr}@{self.characteristic.element}"
        if self.eid:
            key = f"{key}>{self.eid}"
        return key

    def define(self, path, characteristic, eid, specs=None):
        # if path is None:
        #     raise Exception('path can not be None')

        self._trigger_event(
            self.Defined(path=path, characteristic=characteristic, eid=eid, specs=specs)
        )

    def add_requirement(self, requirement):
        self._trigger_event(self.RequirementAdded(requirement_id=requirement.id))

    def add_affected(self, affected):
        if affected not in self._affects_to:
            self._trigger_event(self.AffectedAdded(affected=affected))

    def change_specs(self, specification, responsible):
        self._trigger_event(
            self.SpecificationChanged(
                specs=specification, responsible_id=responsible.id
            )
        )

    def get_requirement(self, key):
        for requirement in self.requirements:
            if key == requirement.key:
                return requirement

    def load_subrequirements(self, store):
        """Load subrequirements from domain."""
        self.requirements = []
        for requirement_id in self._requirement_ids:
            self.requirements.append(store.load(requirement_id))


class Characteristic(Value):
    def __init__(self, attr, element):
        super().__init__(attr=attr, element=element)


class Specification(Value):
    pass
