"""As a developer, I want all value objects in a place,
so I can get it easily
"""
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from enum import Enum
from typing import Any

from inddom.ddd import Value
from inddom.hhrr.persons import PersonId
from inddom.product.definitions import PartModel


@dataclass
class PartInfo:
    part_number: str
    serial_number: str
    extra_info: dict | None = None


@dataclass(frozen=True)
class PersonDataTransfer:
    code: str
    name: str
    full_name: str


@dataclass(frozen=True)
class RequirementDataTransfer:
    uuid: str
    path: str
    characteristic: dict
    eid: str
    specs: dict
    requirements: list = field(default_factory=list)

    @classmethod
    def from_requirement(cls, requi):
        specs_dto = requi.specs.as_plain_dict() if requi.specs else None
        return cls(
            requi.id.uuid.hex,
            requi.path,
            requi.characteristic.as_plain_dict(),
            requi.eid,
            specs_dto,
            [cls.from_requirement(sub_req) for sub_req in requi.requirements],
        )


class TestResolution(Enum):
    SUCCESS = "success"
    FAILED = "failed"
    CANCELLED = "cancelled"


class OperationResult(Enum):
    OK = "ok"
    NOK = "nok"
    DONE = "done"
    CANCELLED = "cancelled"


@dataclass(frozen=True)
class Request:
    message: str
    sender: str | None = None


@dataclass(frozen=True)
class Response:
    message: str
    request: Request
    sender: str | None = None


@dataclass(frozen=True)
class PartModelDataTransfer:
    part_number: str
    part_name: str
    part_description: str

    @classmethod
    def from_part_model(cls, part_model: PartModel):
        return PartModelDataTransfer(
            part_model.part_number, part_model.name, part_model.description
        )


@dataclass(frozen=True)
class Validation(Value):
    def __init__(
        self,
        entity_id: Any,
        validation_period: int,
        validated_by: PersonId,
        today: datetime | None = None,
    ):
        today = today if today else datetime.today()
        super().__init__(
            issue_date=today,
            caducation_date=today + timedelta(days=validation_period),
            validate_by=validated_by,
        )

    def has_caducated(self, today: datetime | None = None):
        today = today if today else datetime.today()
        return self.caducation_date < today
